<?php

namespace  App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PollRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    switch ($this->getMethod()) {
      case "post":
      case "POST":

        return [
          "title" => "required|string|max:190",
          "options" => "array",
          "excerpt" => "string",
          "status" => "boolean",
          "sticky" => "boolean",
          "end_date" => "string|max:190"
        ];

      case "put":
      case "PUT":
        return [
          "title" => "required|string|max:190",
          "slug" => "string|unique:polls,slug," . $this->id, "|max:190",
          "options" => "array",
          "excerpt" => "string",
          "status" => "boolean",
          "sticky" => "boolean",
          "end_date" => "string|max:190"
        ];
    }
  }
}