<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LinkRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    $rules = [];
    switch ($this->getMethod()) {
        // handle creates
      case "post":
      case "POST":
        $rules["name"] = "required";
        $rules["linkable_type"] = "required";
        if ($this->linkable_type == "Url") {
          $rules["url"] = "required";
        } else {
          $rules["linkable_id"] = "required";
        }
        $rules["menu_id"] = "required";
        $rules["order"] = "required";
        return $rules;

        // Handle updates
      case "put":
      case "PUT":
        $rules["name"] = "required";
        $rules["linkable_type"] = "required";
        if ($this->linkable_type == "url") {
          $rules["url"] = "required";
        } else {
          $rules["linkable_id"] = "required";
        }
        $rules["menu_id"] = "required";
        $rules["order"] = "required";
        return $rules;
    }
  }
}
