<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Repositories\Category\CategoryRepository;
use Cviebrock\EloquentSluggable\Services\SlugService;

class CategoryController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CategoryRepository $category)
    {
        $this->category = $category;
    }

    /**
     * Display a listing of the category.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->category->model
            ->sortable(["order" => "desc"])
            ->where("parent_id", null)
            ->paginate(25);

        $all_categories = $this->category->list(["id", "name"]);

        $breadcrumbs = [["url" => "", "name" => trans("front.categories")]];

        return compact("categories", "all_categories", "breadcrumbs");
    }

    /**
     * Store a newly created category in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $input = $request->all();
        $input["slug"] = SlugService::createSlug(Category::class, "slug", $input["name"]);
        $category = $this->category->create($input);

        return compact("category");
    }

    /**
     * Show the form for editing the specified category.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->category->model->findOrFail($id);

        $thumb = $category->thumb()->first();
        if ($thumb) {
            $category->old_thumb = ["file_name" => $thumb->title, "file_path" => asset("uploads/cat_thumb/lg/" . $thumb->uri)];
        } else {
            $category->old_thumb = false;
        }

        $parente = $category->parente;
        if ($parente) {
            $category->parent = ["id" => $parente->id, "name" => $parente->name];
        } else {
            $category->parent = ["id" => "", "name" => ""];
        }

        $ids = $category->kidsIds();
        $all_categories = $this->category->model->whereNotIn("id", $ids)->select(["id", "name"])->get();

        return compact("category", "all_categories");
    }

    /**
     * Display a listing of the category.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = $this->category->model->where("id", $id)->first();
        $categories = $category->kids()->paginate(25);
        $all_categories = $this->category->list(["id", "name"]);

        $breadcrumbs =  $category->getBreadcrumbs("admin");
        
        array_unshift($breadcrumbs, ["url" => "categories", "name" => trans("front.categories")]);
        
        return compact("categories", "all_categories", "breadcrumbs");
    }

    /**
     * Update the specified category in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
        $category = $this->category->model->findOrFail($id);

        $input = $request->all();

        if ($input["slug"] !== $category->slug) {
            if (is_null($input["slug"])) {
                $input["slug"] = SlugService::createSlug(Category::class, "slug", $input["name"]);
            } else {
                $input["slug"] = SlugService::createSlug(Category::class, "slug", $input["slug"]);
            }
        }

        $category = $this->category->update($id, $input);

        return compact("category");
    }

    /**
     * Remove the specified category from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->category->destroy($id);
        return compact("result");
    }

    /**
     * Change status of the category.
     *
     * @param  int $id
     * @param  int $status
     * @return \Illuminate\Http\Response
     */
    public function changeStatus($id, $status)
    {
        $category = $this->category->changeStatus($id, $status);
        $status = $category->status;
        return compact("status");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function actions(Request $request)
    {
        $input = $request->all();
        $results = [];
        switch ($input["action"]) {
            case "delete":
                foreach ($input["items"] as $id => $val) {
                    if ($val) {
                        $result = $this->category->destroy($id, request("forced"));
                        if ($result) {
                            $results[] = $id;
                        }
                    }
                }
                break;

            case "activate":
                foreach ($input["items"] as $id => $val) {
                    if ($val) {
                        $category = $this->category->changeStatus($id, 1);
                        $status = $category->status;
                        $results[] = ["id" => $id, "status" => $status];
                    }
                }
                break;
            case "deactivate":
                foreach ($input["items"] as $id => $val) {
                    if ($val) {
                        $category = $this->category->changeStatus($id, 0);
                        $status = $category->status;
                        $results[] = ["id" => $id, "status" => $status];
                    }
                }
                break;
            default:
                break;
        }

        return compact("results");
    }
}
