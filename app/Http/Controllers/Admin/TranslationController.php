<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Waavi\Translation\Models\Language;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\TranslationRequest;
use Waavi\Translation\Models\Translation;
use Waavi\Translation\Facades\TranslationCache;
use App\Repositories\Translation\TranslationRepository;

class TranslationController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Translation $translation, TranslationRepository $translationRepo, Language $language)
    {
        $this->translation = $translation;
        $this->translationRepo = $translationRepo;
        $this->language = $language;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $translations = $this->translation;
        if ($request->locale) {
            $translations = $translations->where("locale", $request->locale);
            $language = $this->language->where("locale", $request->locale)->first();
        } else {
            $language = $this->language->where("locale", config("app.locale"))->first();
        }
        if ($request->group) {
            $translations = $translations->where("group", $request->group);
        }
        if ($request->item) {
            $translations = $translations->where("item",  $request->item);
        }
        if ($request->text) {
            $translations = $translations->where("text", "like", "%" . $request->text . "%");
        }
        $translations = $translations->orderBy("id", "asc")->paginate(50);

        foreach ($translations as $translation) {
            
            try {
                $translation->main_text = $this->translation->where("locale", config("app.locale"))->where("item", $translation->item)->where("group", $translation->group)->first()->text;
                
            } catch (\Throwable $th) {
                $translation->main_text = "";
            }
            
        }
        //dd($translations);

        $languages = $this->language->all();

        $groups = $this->translationRepo->getGroupsArray();

        return compact("translations", "language", "languages", "groups");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TranslationRequest $request)
    {
        $input = $request->all();
        $result = $this->translationRepo->create($input);
        flushTrans();

        if (!$result) {
            abort(401);
        } else {
            return ["success" => true];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TranslationRequest $request)
    {
        $input = $request->all();

        $result = $this->translationRepo->update($input);

        flushTrans();

        if (!$result) {
            abort(401);
        } else {
            return ["success" => true];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = $this->translation->findOrFail($id);
        $result = $item->delete();

        flushTrans();

        if (!$result) {
            abort(401);
        } else {
            return ["success" => true];
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fixTranslations()
    {

        $result = $this->translationRepo->fixTranslations();
        $success = true;

        flushTrans();
        return ["success" => $success, "reslut" => $result];
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function flushTrans()
    {
        TranslationCache::flushAll();

        $languages = $this->language->all();

        foreach ($languages as $language) {

            $admin_trans = [];
            foreach ($this->translation->where("locale", $language->locale)->where("group", "admin")->get() as $translation) {
                $admin_trans[$translation->item] = $translation->text;
            }
            $admin = json_encode($admin_trans);

            $front_trans = [];
            foreach ($this->translation->where("locale", $language->locale)->where("group", "front")->get() as $translation) {
                $front_trans[$translation->item] = $translation->text;
            }
            $front = json_encode($front_trans);

            $text_trans = [];
            foreach ($this->translation->where("locale", $language->locale)->where("group", "text")->get() as $translation) {
                $text_trans[$translation->item] = $translation->text;
            }
            $text = json_encode($text_trans);

            Storage::disk("public_html")->put("js/trans/admin_" . $language->locale . ".js", "window.trans = {'admin' : " . $admin . ",'front' : " . $front . ",  'text' : " . $text . "}");
            Storage::disk("public_html")->put("js/trans/text_" . $language->locale . ".js", "window.trans = {'text' : " . $text . "}");
        }
    }
}
