<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\PubRequest;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Repositories\Pub\PubRepository;

class PubController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PubRepository $pub)
    {
        $this->pub = $pub;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $columns = ["statu" => "status"];
        $trashed = request("trashed");
        $pubs = $this->pub->model
            ->sortable(["id" => "desc"])
            ->filterColumns($columns);
        if ($trashed) {
            $pubs = $pubs->onlyTrashed();
        }
        $pubs = $pubs->paginate(24);

        return compact("pubs");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PubRequest $request)
    {
        $input = $request->all();
        $input["start_date"] = Carbon::parse($input["start_date"]);
        $input["end_date"] = Carbon::parse($input["end_date"]);
        $pub = $this->pub->create($input);

        return compact("pub");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pub = $this->pub->model->findOrFail($id);
        $pub->start_date = Carbon::parse($pub->start_date)->format("Y-m-d");
        $pub->end_date = Carbon::parse($pub->end_date)->format("Y-m-d");

        $languages = DB::table("translator_languages")->get();
        $translations = [];
        foreach ($languages as $language) {
            $arr = ["language" => $language->name];
            $name = DB::table("translator_translations")->where("locale", $language->locale)->where("group", "pub")->where("item", "name-" . $id)->first();
            if ($name) {
                $arr["name"] = $name->text;
            } else {
                $arr["name"] = "";
            }
            $translations[$language->locale] = $arr;
        }

        $image = $pub->image()->first();

        if ($image) {
            $pub->old_image = ["file_name" => $image->id, "file_path" => $image->getImage("sm")];
        }

        return compact("pub", "translations");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PubRequest $request, $id)
    {
        $input = $request->all();
        $input["start_date"] = Carbon::parse($input["start_date"]);
        $input["end_date"] = Carbon::parse($input["end_date"]);
        $pub = $this->pub->update($id, $input);
        return compact("pub");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->pub->destroy($id, request("forced"));
        return compact("result");
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $result = $this->pub->model->withTrashed()->find($id)->restore();
        return compact("result");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function changeStatus($id, $status)
    {
        $pub = $this->pub->changeStatus($id, $status);
        $status = $pub->status;

        return compact("status");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function translations(Request $request, $id)
    {
        $input = $request->all();
        foreach ($input as $key => $translation) {
            $name = DB::table("translator_translations")->where("locale", $key)->where("group", "pub")->where("item", "name-" . $id)->first();
            if ($name) {
                DB::table("translator_translations")->where("id", $name->id)->update(array("text" => $translation["name"]));
            } elseif ($translation["name"]) {
                DB::table("translator_translations")->insert(["locale" => $key, "group" => "pub", "item" => "name-" . $id, "text" => $translation["name"]]);
            }
        }
        $result = true;
        return compact("result");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function actions(Request $request)
    {
        $input = $request->all();
        $results = [];
        switch ($input["action"]) {
            case "delete":
                foreach ($input["items"] as $id => $val) {
                    if ($val) {
                        $result = $this->pub->destroy($id, request("forced"));
                        if ($result) {
                            $results[] = $id;
                        }
                    }
                }
                break;
            case "restor":
                foreach ($input["items"] as $id => $val) {
                    if ($val) {
                        $result = $this->pub->model->withTrashed()->find($id)->restore();
                        if ($result) {
                            $results[] = $id;
                        }
                    }
                }
                break;
            case "activate":
                foreach ($input["items"] as $id => $val) {
                    if ($val) {
                        $pub = $this->pub->changeStatus($id, 1);
                        $status = $pub->status;
                        $results[] = ["id" => $id, "status" => $status];
                    }
                }
                break;
            case "deactivate":
                foreach ($input["items"] as $id => $val) {
                    if ($val) {
                        $pub = $this->pub->changeStatus($id, 0);
                        $status = $pub->status;
                        $results[] = ["id" => $id, "status" => $status];
                    }
                }
                break;
            default:
                break;
        }

        return compact("results");
    }
}
