<?php

namespace App\Http\Controllers\Admin;

use App\Services\Upload;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use App\Repositories\User\UserRepository;

class UserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Upload $upload, UserRepository $user)
    {
        $this->user = $user;
        $this->upload = $upload;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = $this->user->model->paginate(20);
        $roles = Role::get();
        return compact("users", "roles");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::get();
        return compact("roles");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->user->model->findOrFail($id);
        if (filter_var($user->profile_photo_path, FILTER_VALIDATE_URL) === FALSE) {
            $file_path = asset("/uploads/avatar/md/" . $user->profile_photo_path);
        } else {
            $file_path = $user->profile_photo_path;
        }
        $user->old_avatar = ["file_name" => $user->profile_photo_path, "file_path" => $file_path];
        return compact("user");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $input = $request->all();
        $user = $this->user->create($input);
        $role = Role::where("name", "user")->first();
        $user->syncRoles($role);
        return compact("user");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $input = $request->all();
        if (isset($input["password"])) {
            $input["password"] = $input["password"];
            unset($input["password_confirmation"]);
        } else {
            unset($input["password"]);
            unset($input["password_confirmation"]);
        }
        $user = $this->user->update($id, $input);
        return compact("user");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->user->destroy($id);
        return compact("result");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changeRole($id, $role)
    {
        $user = $this->user->model->findOrFail($id);
        $role = Role::where("name", $role)->first();
        $user->syncRoles($role);
        $role = $role->name;
        return compact("role");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function uploadAvatar(Request $request)
    {
        $input = $request->all();
        $data["file"] = $request->file("file");
        $file = $this->upload->image($data, "avatar");
        return response()->json(array("success" => true, "avatar" => $file));
    }
}
