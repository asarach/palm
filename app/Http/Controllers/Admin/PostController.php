<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Models\Post;
use Cviebrock\EloquentSluggable\Services\SlugService;
use App\Repositories\Post\PostRepository;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\User\UserRepository;
use Illuminate\Http\Request;

class PostController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PostRepository $post, CategoryRepository $category, UserRepository $user)
    {
        $this->post = $post;
        $this->category = $category;
        $this->user = $user;
    }

    /**
     * Display a listing of the post.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $columns = ['statu' => 'posts.status', 'category' => 'posts.category_id'];
        $trashed = request('trashed');

        $posts = $this->post->model
            ->sortable(['created_at' => 'desc'])
            ->filterColumns($columns);
        if ($trashed) {
            $posts = $posts->onlyTrashed();
        }
        $posts = $posts->with(['author:id,name','category:id,name'])->paginate(24);
        $categories = $this->category->list(['id', 'name']);

        return compact('posts', 'categories');
    }

    /**
     * Show the form for creating a new post.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->category->list(['id', 'name']);
        return compact('categories');
    }

    /**
     * Store a newly created post in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $input = $request->all();
        $input['slug'] = SlugService::createSlug(Post::class, 'slug', $input['title']);
        $post = $this->post->create($input);

        return compact('post');
    }

    /**
     * Show a specified post.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = $this->post->model->findOrFail($id);
        return compact('post');
    }

    /**
     * Show the form for editing the specified post.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = $this->post->model->with(['author:id,name','category:id,name'])->findOrFail($id);
        
        $old_images = [];
        foreach ($post->images as $image) {
            $old_images[] = ["file_name" => $image->uri, "file_id" => $image->id, "file_path" => asset("uploads/post_images/sm/" . $image->uri)];
        }
        $post->old_images = $old_images;
        $post->images_upload = [];
        
        $post->body = html_entity_decode($post->body);
        $categories = $this->category->list(['id', 'name']);
        return compact('post', 'categories');
    }

    /**
     * Update the specified post in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id)
    {
        $post = $this->post->model->findOrFail($id);
        $input = $request->all();

        if ($input['slug'] !== $post->slug) {
            if (is_null($input['slug'])) {
                $input['slug'] = SlugService::createSlug(Post::class, 'slug', $input['title']);
             } else {
                 $input['slug'] = SlugService::createSlug(Post::class, 'slug', $input['slug']);
           }
        }
        
        $post = $this->post->update($id, $input);
        return compact('post');
    }

    /**
     * Remove the specified post from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->post->destroy($id, request('forced'));
        return compact('result');
    }

    /**
     * Restore the specified post to storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $result = $this->post->model->withTrashed()->find($id)->restore();
        return compact('result');
    }

    /**
     * Change status of the post.
     *
     * @param  int $id
     * @param  int $status
     * @return \Illuminate\Http\Response
     */
    public function changeStatus($id, $status)
    {
        $post = $this->post->changeStatus($id, $status);
        $status = $post->status;
        return compact('status');
    }

    /**
     * Change sticky of the post.
     *
     * @param  int $id
     * @param  int $status
     * @return \Illuminate\Http\Response
     */
    public function changeSticky($id, $status)
    {
        $post = $this->post->changeSticky($id, $status);
        $sticky = $post->sticky;
        return compact('sticky');
    }

    /**
     * Change promote of the post.
     *
     * @param  int $id
     * @param  int $status
     * @return \Illuminate\Http\Response
     */
    public function changePromote($id, $status)
    {
        $post = $this->post->changePromote($id, $status);
        $promoted = $post->promoted;
        return compact('promoted');
    }

    /**
     * duplicate the specified post to storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function duplicate($id)
    {
        $post = $this->post->duplicate($id);
        return compact('post');
    }

    /**
     * Restore the specified post to storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function actions(Request $request)
    {
        $input = $request->all();
        $results = [];
        switch ($input['action']) {
            case 'restor':
                foreach ($input['items'] as $id => $val) {
                    if ($val) {
                        $result = $this->post->model->withTrashed()->find($id)->restore();
                        if ($result) {
                            $results[] = $id;
                        }
                    }
                }
                break;

            case 'delete':
                foreach ($input['items'] as $id => $val) {
                    if ($val) {
                        $result = $this->post->destroy($id, request('forced'));
                        if ($result) {
                            $results[] = $id;
                        }
                    }
                }
                break;

            case 'status-active':
                foreach ($input['items'] as $id => $val) {
                    if ($val) {
                        $post = $this->post->changeStatus($id, 1);
                        $status = $post->status;
                        $results[] = ['id' => $id, 'status' => $status];
                    }
                }
                break;
            case 'status-deactive':
                foreach ($input['items'] as $id => $val) {
                    if ($val) {
                        $post = $this->post->changeStatus($id, 0);
                        $status = $post->status;
                        $results[] = ['id' => $id, 'status' => $status];
                    }
                }
                break;
            case 'sticky-yes':
                foreach ($input['items'] as $id => $val) {
                    if ($val) {
                        $post = $this->post->changeSticky($id, 1);
                        $sticky = $post->sticky;
                        $results[] = ['id' => $id, 'sticky' => $sticky];
                    }
                }
                break;
            case 'sticky-no':
                foreach ($input['items'] as $id => $val) {
                    if ($val) {
                        $post = $this->post->changeSticky($id, 0);
                        $sticky = $post->sticky;
                        $results[] = ['id' => $id, 'sticky' => $sticky];
                    }
                }
                break;
            case 'promoted-yes':
                foreach ($input['items'] as $id => $val) {
                    if ($val) {
                        $post = $this->post->changePromote($id, 1);
                        $promoted = $post->promoted;
                        $results[] = ['id' => $id, 'promoted' => $promoted];
                    }
                }
                break;
            case 'promoted-no':
                foreach ($input['items'] as $id => $val) {
                    if ($val) {
                        $post = $this->post->changePromote($id, 0);
                        $promoted = $post->promoted;
                        $results[] = ['id' => $id, 'promoted' => $promoted];
                    }
                }
                break;
            default:
                break;
        }

        return compact('results');

    }
}