<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\LinkRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Repositories\Link\LinkRepository;
use App\Repositories\Menu\MenuRepository;

class MenuController extends Controller
{

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct(MenuRepository $menu, LinkRepository $link)
  {
    $this->menu = $menu;
    $this->link = $link;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $links = collect();
    $level = 0;
    $links = $this->addSublinks($links, $this->link->model, null, $level);
    $menus = $this->menu->model->get();
    $menu_locations = config("asarach.menu_locations");
    $linkable_types = getLinkableTypes();
    return compact("links", "menus", "linkable_types", "menu_locations");
  }

  public function addSublinks($links, $item, $parent, $level)
  {
    $columns = ["menu" => "menu_id"];
    $level = $level + 1;
    $items = $item
      ->sortable(["order" => "asc"])
      ->with("parente:id,name")
      ->with("menu:id,name")
      ->where("parent_id", $parent)
      ->filterColumns($columns)
      ->select("links.*", "name as linkable_item",)
      ->get();
    foreach ($items as $key => $item) {
      $item->level = $level;
      $links->push($item);
      $links = $this->addSublinks($links, $item->kids(), $item->id, $level);
    }

    return $links;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function getItems(Request $request)
  {
    $input = $request->all();
    $items = $this->getItemsByType($input["type"], $input["query"]);
    return compact("items");
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $input = $request->all();
    $menu = $this->menu->create($input);
    Cache::flush();
    return compact("menu");
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function storeLink(LinkRequest $request)
  {
    $input = $request->all();
    $link = $this->link->create($input);
    Cache::flush();
    return compact("link");
  }

  /**
   * Display the specified resource.
   *
   * @param  App\Models\Menu  $menu
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $menu = $this->menu->model->findOrFail($id);
    $menus = $menu->kids()->paginate(20);
    $allMenus = $this->menu->model->get();

    return compact("menu", "menus", "allMenus");
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  App\Models\Menu  $menu
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $menu = $this->menu->model->findOrFail($id);
    return compact("menu");
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  App\Models\Menu  $menu
   * @return \Illuminate\Http\Response
   */
  public function editLink($id)
  {
    $link = $this->link->model->findOrFail($id);
    $links = $this->link->model->whereNotIn("id", [$id])->get();
    $menus = $this->menu->model->get();
    $linkable_types = getLinkableTypes();
    $selected_type = $link->linkable_type;
    $linkable_items = $this->getItemsByType($link->linkable_type, "");
    $selected_item = $linkable_items->where("id", $link->linkable_id)->first();
    return compact("link", "links", "linkable_items", "menus", "linkable_types", "selected_type", "selected_item");
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  App\Models\Menu  $menu
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $input = $request->all();
    $menu = $this->menu->update($id, $input);
    $menus = $this->menu->model->get();
    Cache::flush();
    return compact("menus");
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  App\Models\Menu  $link
   * @return \Illuminate\Http\Response
   */
  public function updateLink(Request $request, $id)
  {
    $input = $request->all();
    $link = $this->link->update($id, $input);
    Cache::flush();
    return compact("link");
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  App\Models\Menu  $menu
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $result = $this->menu->destroy($id);
    $menus = $this->menu->model->get();
    Cache::flush();
    return compact("menus");
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  App\Models\Menu  $link
   * @return \Illuminate\Http\Response
   */
  public function destroyLink($id)
  {
    $result = $this->link->destroy($id);
    $links = $this->link->model->get();
    Cache::flush();

    return compact("links");
  }

  public function getItemsByType($type, $query)
  {
    $col = "title";
    switch ($type) {
      case "App\Models\Category":
        $items = \App\Models\Category::select("id", "name");
        $col = "name";
        break;
      case "App\Models\Page":
        $items = \App\Models\Page::select("id", "name");
        $col = "name";
        break;
      case "App\Models\Post":
        $items = \App\Models\Post::select("id", "title as name", "created_at");
        break;
      default:
        $items = \App\Models\Page::select("id", "name");
        break;
    }
    
    if ($query) {
      $items =  $items->where($col, "like", "%" . $query . "%")->take(100)->get();
    } else {
      $items =  $items->take(100)->get();
    }

    if ($type == "Url") {
      $items = collect();
    }

    return $items;
  }
}
