<?php

namespace App\Http\Controllers\Admin;

use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Requests\PageRequest;
use App\Http\Controllers\Controller;
use Waavi\Translation\Models\Language;
use App\Repositories\Page\PageRepository;
use Cviebrock\EloquentSluggable\Services\SlugService;

class PageController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PageRepository $page, Language $language)
    {
        $this->language = $language;
        $this->page = $page;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $columns = ["statu" => "status", "language" => "language_id"];

        $trashed = request("trashed");

        $pages = $this->page->model;
        if ($trashed) {
            $pages = $pages->onlyTrashed();
        }
        $pages = $pages->orderBy("id", "desc")->filterColumns($columns)->with(["language:id,name"])->paginate(24);

        $languages =  $this->language->select("id", "name")->get();

        return compact("pages", "languages");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = $this->language->get();
        return compact("languages");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageRequest $request)
    {
        $input = $request->all();
        $input["slug"] = SlugService::createSlug(Page::class, "slug", $input["name"]);

        $page = $this->page->create($input);
        return compact("page");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = $this->page->model->with(["language:id,name"])->findOrFail($id);
        $page->content = html_entity_decode($page->content);

        $languages = $this->language->get();

        return compact("page", "languages");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PageRequest $request, $id)
    {
        $page = $this->page->model->findOrFail($id);
        $input = $request->all();
        if ($input["slug"] !== $page->slug) {
            if (is_null($input["slug"])) {
                $input["slug"] = SlugService::createSlug(Page::class, "slug", $input["name"]);
            } else {
                $input["slug"] = SlugService::createSlug(Page::class, "slug", $input["slug"]);
            }
        }

        $page = $this->page->update($id, $input);
        return compact("page");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->page->destroy($id, request("forced"));
        return compact("result");
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $result = $this->page->model->withTrashed()->find($id)->restore();
        return compact("result");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function changeStatus($id, $status)
    {
        $page = $this->page->changeStatus($id, $status);
        $status = $page->status;
        return compact("status");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function actions(Request $request)
    {
        $input = $request->all();
        $results = [];
        switch ($input["action"]) {
            case "delete":
                foreach ($input["items"] as $id => $val) {
                    if ($val) {
                        $result = $this->page->destroy($id, request("forced"));
                        if ($result) {
                            $results[] = $id;
                        }
                    }
                }
                break;
            case "restor":
                foreach ($input["items"] as $id => $val) {
                    if ($val) {
                        $result = $this->page->model->withTrashed()->find($id)->restore();
                        if ($result) {
                            $results[] = $id;
                        }
                    }
                }
                break;
            case "activate":
                foreach ($input["items"] as $id => $val) {
                    if ($val) {
                        $page = $this->page->changeStatus($id, 1);
                        $status = $page->status;
                        $results[] = ["id" => $id, "status" => $status];
                    }
                }
                break;
            case "deactivate":
                foreach ($input["items"] as $id => $val) {
                    if ($val) {
                        $page = $this->page->changeStatus($id, 0);
                        $status = $page->status;
                        $results[] = ["id" => $id, "status" => $status];
                    }
                }
                break;
            default:
                break;
        }

        return compact("results");
    }
}
