<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\Poll;
use App\Models\PollAnswer;
use Illuminate\Http\Request;
use App\Http\Requests\PollRequest;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Waavi\Translation\Models\Language;
use App\Repositories\Poll\PollRepository;
use App\Repositories\User\UserRepository;
use Cviebrock\EloquentSluggable\Services\SlugService;

class PollController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PollRepository $poll, Language $language, UserRepository $user)
    {
        $this->poll = $poll;
        $this->language = $language;
        $this->user = $user;
    }

    /**
     * Display a listing of the poll.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $columns = ["statu" => "status", "language" => "language_id", "sticky" => "sticky"];
        $trashed = request("trashed");

        $polls = $this->poll->model
            ->sortable(["created_at" => "desc"])
            ->filterColumns($columns);
        if ($trashed) {
            $polls = $polls->onlyTrashed();
        }
        $polls = $polls->with(["language:id,name", "user:id,name"])->paginate(24);

        $languages =  $this->language->select("id", "name")->get();

        return compact("polls", "languages");
    }

    /**
     * Show the form for creating a new poll.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = $this->language->get();
        return compact("languages");
    }

    /**
     * Store a newly created poll in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PollRequest $request)
    {
        $input = $request->all();
        $input["slug"] = SlugService::createSlug(Poll::class, "slug", $input["title"]);
        $input["end_date"] = Carbon::parse($input["end_date"]);
        $poll = $this->poll->create($input);
        return compact("poll");
    }

    /**
     * Show a specified poll.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $poll = $this->poll->model->findOrFail($id);
        $results = $this->getResults($poll);
        return compact("poll", "results");
    }

    /**
     * Show the form for editing the specified poll.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $poll = $this->poll->model->with(["language:id,name"])->findOrFail($id);
        $poll->end_date = Carbon::parse($poll->end_date)->toIso8601String();

        $poll->options  = $poll->options()->select(["id", "title"])->get()->toArray();

        $languages = $this->language->get();

        $poll->content = html_entity_decode($poll->content);

        return compact("poll", "languages");
    }

    /**
     * Update the specified poll in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PollRequest $request, $id)
    {
        $poll = $this->poll->model->findOrFail($id);
        $input = $request->all();

        if ($input["slug"] !== $poll->slug) {
            if (is_null($input["slug"])) {
                $input["slug"] = SlugService::createSlug(Poll::class, "slug", $input["title"]);
            } else {
                $input["slug"] = SlugService::createSlug(Poll::class, "slug", $input["slug"]);
            }
        }

        $input["end_date"] = Carbon::parse($input["end_date"]);
        if (!empty($input["created_at"])) {
            $input["created_at"] = Carbon::parse($input["created_at"]);
        } else {
            $input["created_at"] = "";
        }

        $poll = $this->poll->update($id, $input);
        return compact("poll");
    }

    /**
     * Remove the specified poll from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->poll->destroy($id, request("forced"));
        return compact("result");
    }

    /**
     * Restore the specified poll to storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $result = $this->poll->model->withTrashed()->find($id)->restore();
        return compact("result");
    }

    /**
     * Change status of the poll.
     *
     * @param  int $id
     * @param  int $status
     * @return \Illuminate\Http\Response
     */
    public function changeStatus($id, $status)
    {
        $poll = $this->poll->changeStatus($id, $status);
        $status = $poll->status;
        return compact("status");
    }

    /**
     * Change sticky of the poll.
     *
     * @param  int $id
     * @param  int $status
     * @return \Illuminate\Http\Response
     */
    public function changeSticky($id, $status)
    {
        $poll = $this->poll->changeSticky($id, $status);
        $sticky = $poll->sticky;
        return compact("sticky");
    }

    /**
     * Change promote of the poll.
     *
     * @param  int $id
     * @param  int $status
     * @return \Illuminate\Http\Response
     */
    public function changePromote($id, $status)
    {
        $poll = $this->poll->changePromote($id, $status);
        $promoted = $poll->promoted;
        return compact("promoted");
    }


    /**
     * Restore the specified poll to storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function actions(Request $request)
    {
        $input = $request->all();
        $results = [];
        switch ($input["action"]) {
            case "restor":
                foreach ($input["items"] as $id => $val) {
                    if ($val) {
                        $result = $this->poll->model->withTrashed()->find($id)->restore();
                        if ($result) {
                            $results[] = $id;
                        }
                    }
                }
                break;

            case "delete":
                foreach ($input["items"] as $id => $val) {
                    if ($val) {
                        $result = $this->poll->destroy($id, request("forced"));
                        if ($result) {
                            $results[] = $id;
                        }
                    }
                }
                break;

            case "activate":
                foreach ($input["items"] as $id => $val) {
                    if ($val) {
                        $poll = $this->poll->changeStatus($id, 1);
                        $status = $poll->status;
                        $results[] = ["id" => $id, "status" => $status];
                    }
                }
                break;
            case "deactivate":
                foreach ($input["items"] as $id => $val) {
                    if ($val) {
                        $poll = $this->poll->changeStatus($id, 0);
                        $status = $poll->status;
                        $results[] = ["id" => $id, "status" => $status];
                    }
                }
                break;
            case "sticky":
                foreach ($input["items"] as $id => $val) {
                    if ($val) {
                        $poll = $this->poll->changeSticky($id, 1);
                        $sticky = $poll->sticky;
                        $results[] = ["id" => $id, "sticky" => $sticky];
                    }
                }
                break;
            case "desticky":
                foreach ($input["items"] as $id => $val) {
                    if ($val) {
                        $poll = $this->poll->changeSticky($id, 0);
                        $sticky = $poll->sticky;
                        $results[] = ["id" => $id, "sticky" => $sticky];
                    }
                }
                break;
            case "promoted":
                foreach ($input["items"] as $id => $val) {
                    if ($val) {
                        $poll = $this->poll->changePromote($id, 1);
                        $promoted = $poll->promoted;
                        $results[] = ["id" => $id, "promoted" => $promoted];
                    }
                }
                break;
            case "depromoted":
                foreach ($input["items"] as $id => $val) {
                    if ($val) {
                        $poll = $this->poll->changePromote($id, 0);
                        $promoted = $poll->promoted;
                        $results[] = ["id" => $id, "promoted" => $promoted];
                    }
                }
                break;
            default:
                break;
        }

        return compact("results");
    }


    public function getResults($poll)
    {
        $votes = PollAnswer::where("poll_id", $poll->id)
            ->select("option_id", DB::raw("count(*) as option_total"))
            ->groupBy("option_id")
            ->get()
            ->pluck("option_total", "option_id");

        $result = [];

        $result["title"] = $poll->title;
        $result["options"] = [];
        $result["pollq_totalvoters"] = 0;
        foreach ($votes as $key => $vote) {
            $result["pollq_totalvoters"] = $result["pollq_totalvoters"] + $vote;
        }



        foreach ($poll->options as $key => $option) {
            if (isset($votes[$option->id])) {
                $option->votes = $votes[$option->id];
                $option->option_percentage = round(100 * $option->votes / $result["pollq_totalvoters"]);
            } else {
                $option->votes = 0;
                $option->option_percentage = 0;
            }
            $result["options"][] = $option;
        }

        return $result;
    }
}
