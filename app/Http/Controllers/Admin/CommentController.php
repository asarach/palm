<?php

namespace App\Http\Controllers\Admin;

use App\Models\Comment;
use App\Services\Search;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CommentRequest;
use App\Repositories\Comment\CommentRepository;
use Cviebrock\EloquentSluggable\Services\SlugService;


class CommentController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CommentRepository $comment, Search $search)
    {
        $this->comment = $comment;
        $this->search = $search;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $columns = ["statu" => "status"];

        $trashed = request("trashed");
        $comments = $this->comment->model->filterColumns($columns);
        if ($trashed) {
            $comments = $comments->onlyTrashed();
        }

        $q = request("query");
        if ($q) {
            $ids = $this->search->search($q, "comments_index");
            $comments = $comments->whereIn("id", $ids);
        } 

        $comments = $comments->sortable(["created_at" => "desc"])->paginate(24);

        return compact("comments");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = [];
        return compact("languages");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request)
    {
        $input = $request->all();
        $input["slug"] = SlugService::createSlug(Comment::class, "slug", $input["name"]);
        $comment = $this->comment->create($input);
        return compact("comment");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comment = $this->comment->model->findOrFail($id);
        return compact("comment");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CommentRequest $request, $id)
    {
        $comment = $this->comment->model->findOrFail($id);
        $input = $request->all();
        $result = $this->comment->update($id, $input);
        return compact("comment");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->comment->destroy($id, request("forced"));
        return compact("result");
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $result = $this->comment->model->withTrashed()->find($id)->restore();
        return compact("result");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function changeStatus($id, $status)
    {
        $comment = $this->comment->changeStatus($id, $status);
        $status = $comment->status;
        return compact("status");
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function actions(Request $request)
    {
        $input = $request->all();
        $results = [];
        switch ($input["action"]) {
            case "delete":
                foreach ($input["items"] as $id => $val) {
                    if ($val) {
                        $result = $this->comment->destroy($id, request("forced"));
                        if ($result) {
                            $results[] = $id;
                        }
                    }
                }
                break;
            case "restor":
                foreach ($input["items"] as $id => $val) {
                    if ($val) {
                        $result = $this->comment->model->withTrashed()->find($id)->restore();
                        if ($result) {
                            $results[] = $id;
                        }
                    }
                }
                break;
            case "activate":
                foreach ($input["items"] as $id => $val) {
                    if ($val) {
                        $comment = $this->comment->changeStatus($id, 1);
                        $status = $comment->status;
                        $results[] = ["id" => $id, "status" => $status];
                    }
                }
                break;
            case "deactivate":
                foreach ($input["items"] as $id => $val) {
                    if ($val) {
                        $comment = $this->comment->changeStatus($id, 0);
                        $status = $comment->status;
                        $results[] = ["id" => $id, "status" => $status];
                    }
                }
                break;
            default:
                break;
        }

        return compact("results");
    }
}
