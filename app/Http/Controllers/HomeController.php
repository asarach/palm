<?php

namespace App\Http\Controllers;

use App\Repositories\Post\PostRepository;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PostRepository $post)
    {
        $this->post = $post;
    }
    /**
     * Display the list of homes.
     *
     * @param  \App\Home  $home
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimonial_ids = getSubCatsIds(config('asarach.testimonial_cat'));
        $testimonials = $this->post->model
            ->where("status", 1)
            ->whereIn("category_id", $testimonial_ids)
            ->sortable(["created_at" => "desc"])
            ->take(3)
            ->get();

        $project_ids = getSubCatsIds(config('asarach.project_cat'));
        $projects = $this->post->model
            ->where("status", 1)
            ->whereIn("category_id", $project_ids)
            ->sortable(["created_at" => "desc"])
            ->take(5)
            ->get();

        return view("index", compact("testimonials", "projects"));
    }
}
