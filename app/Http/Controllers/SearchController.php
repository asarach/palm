<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Post\PostRepository;

class SearchController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        PostRepository $post
    ) {
        $this->post = $post;
    }
    /**
     * Display the list of homes.
     *
     * @param  \App\Home  $home
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $input = $request->all();

        $posts = $this->post->search([
            "q" => request('q'),
            "status" => 1,
            "index" => "posts_index",
            "page_name" => "page",
            "per_page" => 15,
            "status" => 1,
        ]);

        return view("search", compact("posts"));
    }
}
