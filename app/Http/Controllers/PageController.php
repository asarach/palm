<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Http\Requests\MessageRequest;
use App\Repositories\Post\PostRepository;
use App\Repositories\Page\PageRepository;
use App\Repositories\Message\MessageRepository;

class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PageRepository $page, MessageRepository $contact, PostRepository $post)
    {
        $this->page = $page;
        $this->post = $post;
        $this->contact = $contact;
    }

    public function about()
    {
        $page = $this->page->model->where("name", "about")->where("status", 1)->first();
        return view("page.about", compact("page"));
    }

    public function faq()
    {
        $page = $this->page->model->where("name", "faq")->where("status", 1)->first();
        return view("page.faq", compact("page"));
    }

   

    public function services()
    {
        $page = $this->page->model->where("name", "services")->where("status", 1)->first();
        return view("page.services", compact("page"));
    }

    public function sitemap()
    {
        $posts = $this->post->model->where("status", 1)->get();
        return view("page.sitemap", compact("posts"));
    }

    public function contact()
    {
        $page = $this->page->model->where("name", "contact")->where("status", 1)->first();
        return view("page.contact", compact("page"));
    }

    public function storeMessage(MessageRequest $request)
    {
        $input = $request->all();
        $this->contact->create($input);
        session()->flash("notification", ["message" => trans("text.message-sent"), "type" => "success", "title" => trans("text.success")]);
        return redirect("/");
    }
}
