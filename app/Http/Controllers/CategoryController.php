<?php

namespace App\Http\Controllers;

use App\Repositories\Category\CategoryRepository;

class CategoryController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CategoryRepository $category)
    {
        $this->category = $category;
    }
    /**
     * Display the specified category.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $loacle = session("current_locale");

        $category = $this->category->model
            ->where("slug", $slug)
            ->where("status", 1)
            ->firstOrFail();

        views($category)->record();

        $posts = $category->posts->model
            ->where("status", 1)
            ->where("language_id", $loacle)
            ->orderBy("created_at", "desc")
            ->paginate(25);

        return view("category.show", compact("category", "posts"));
    }
}
