<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CommentRequest;
use App\Repositories\Comment\CommentRepository;

class CommentController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CommentRepository $comment)
    {
        $this->comment = $comment;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index($type, $id)
    {
        $model = $this->comment->getModel($id, $type);
        $comments = $model->comments()->where("status", 1)->where("parent_id", null)->paginate(4);

        return ["comments" => $comments];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request)
    {
        $user = Auth::user();
        $input = $request->all();

        if (!isset($input["parent"])) {
            $input["parent"] = null;
        }
        /*
        if ($user) {
            $input["name"] = $user->name;
            $input["email"] =$user->email;
        }
        */
        $commentable = $this->comment->getModel($input["post"], $input["type"]);
        $comment = $this->comment->create($input, $commentable);

        return ["success" => true, "comment" => $comment];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(CommentRequest $request, $id)
    {
        $input = $request->all();

        $comment = $this->comment->update($id, $input);

        return ["success" => true, "comment" => $comment];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->comment->destroy($id);
        if (!$result) {
            abort(401);
        } else {
            return ["success" => true];
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function like($id)
    {
        $comment = Comment::findorfail($id);

        $liked = is_ip_action("1", $id);

        if ($liked) {
            unset_ip_action("1", $id);
            $comment->like--;
            $comment->save();
        } else {
            add_ip_action("1", $id);
            $comment->like++;
            $comment->save();
        }

        $disliked = is_ip_action("2", $id);
        if ($disliked) {
            unset_ip_action("2", $id);
            $comment->dislike--;
            $comment->save();
        }

        return ["like" => $comment->like, "dislike" => $comment->dislike];
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function dislike($id)
    {
        $comment = Comment::findorfail($id);

        $disliked = is_ip_action("2", $id);

        if ($disliked) {
            unset_ip_action("2", $id);
            $comment->dislike--;
            $comment->save();
        } else {
            add_ip_action("2", $id);
            $comment->dislike++;
            $comment->save();
        }

        $liked = is_ip_action("1", $id);

        if ($liked) {
            unset_ip_action("1", $id);
            $comment->like--;
            $comment->save();
        }

        return ["like" => $comment->like, "dislike" => $comment->dislike];
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function signal($id)
    {
        $comment = Comment::findorfail($id);

        $signaled = is_ip_action("3", $id);

        if (!$signaled) {
            add_ip_action("3", $id);
            $comment->signal++;
            $comment->save();
            return ["success" => true];
        } else {
            abort(401, "Unauthorized.");
        }
    }
}
