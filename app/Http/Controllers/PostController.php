<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repositories\Post\PostRepository;
use App\Repositories\Category\CategoryRepository;

class PostController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PostRepository $post, CategoryRepository $category)
    {
        $this->post = $post;
        $this->category = $category;
    }
    /**
     * Display the list of posts.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog_ids = getSubCatsIds(config('asarach.blog_cat'));
        $posts = $this->post->model
            ->where("status", 1)
            ->whereIn("category_id", $blog_ids)
            ->sortable(["created_at" => "desc"])
            ->paginate(9);

        return view("post.index", compact("posts"));
    }
    /**
     * Display the specified post.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = $this->post->model
            ->where("slug", $slug)
            ->where("status", 1)
            ->first();

        views($post)->record();


        return view("post.show", compact("post"));
    }
    /**
     * Display the list of posts.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function portfolioIndex()
    {
        $portfolio_ids = getSubCatsIds(config('asarach.project_cat'));

        $cats = $this->category->model
            ->where("id", '!=', config('asarach.project_cat'))
            ->whereIn("id", $portfolio_ids)
            ->get();


        $posts = $this->post->model
            ->where("status", 1)
            ->whereIn("category_id", $portfolio_ids)
            ->sortable(["created_at" => "desc"])
            ->get();

        return view("portfolio.index", compact("posts", "cats"));
    }
    /**
     * Display the specified post.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function portfolioShow($slug)
    {
        $post = $this->post->model
            ->where("slug", $slug)
            ->where("status", 1)
            ->first();

        views($post)->record();

        return view("portfolio.show", compact("post"));
    }
}
