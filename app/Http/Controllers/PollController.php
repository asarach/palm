<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Poll\PollRepository;
use App\Models\PollAnswer;
use DB;

class PollController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PollRepository $poll)
    {
        $this->poll = $poll;
    }
    /**
     * Display the list of polls.
     *
     * @param  \App\Models\Article  $poll
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $loacle = session("current_locale");
        $poll = $this->poll->model
            ->where("status", 1)
            ->where("sticky", 1)
            ->where("language_id", $loacle)
            ->where("end_date", ">",  now())
            ->with(["options"])
            ->sortable(["created_at" => "desc"])
            ->first();

        if ($poll) {
            $result = $this->getResults($poll);
            $result["user_voted"] = !$this->canVote($request->ip(), $poll->id);
        } else {
            $result =  [];
        }

        $status = "poll";

        return response()->json(compact("poll", "result", "status"));
    }
    /**
     * Display the specified poll.
     *
     * @param  \App\Models\Article  $poll
     * @return \Illuminate\Http\Response
     */
    public function vote(Request $request)
    {
        $input = $request->all();
        $loacle = session("current_locale");
        $ip = $request->ip();
        $poll = $this->poll->model
            ->where("status", 1)
            ->where("sticky", 1)
            ->where("language_id", $loacle)
            //->where("id", $input["poll_id"])
            ->with(["options"])
            ->first();

        $result = $this->getResults($poll);

        if ($this->canVote($ip, $input["poll_id"])) {
            foreach ($input["votes"] as $vote) {
                $answer  = PollAnswer::create([
                    "ip" => $ip,
                    "option_id" => $vote,
                    "poll_id" => $input["poll_id"]
                ]);
            }
            $result = $this->getResults($poll);
            $result["user_voted"] = !$this->canVote($ip, $poll->id);
            $error = "";
            $status = "result";
        } else {
            $result = [];
            $error = trans("front.you-cant-vote");
            $status = "error";
        }

        return response()->json(compact("poll", "result", "status", "error"));
    }
    public function canVote($ip, $id)
    {
        $voted = PollAnswer::where("poll_id", $id)
            ->where("ip", $ip)
            ->first();
        if ($voted) {
            return false;
        } else {
            return true;
        }
    }
    public function getResults($poll)
    {
        $votes = PollAnswer::where("poll_id", $poll->id)
            ->select("option_id", DB::raw("count(*) as option_total"))
            ->groupBy("option_id")
            ->get()
            ->pluck("option_total", "option_id");

        $result = [];

        $result["title"] = $poll->title;
        $result["options"] = [];
        $result["pollq_totalvoters"] = 0;
        foreach ($votes as $key => $vote) {
            $result["pollq_totalvoters"] = $result["pollq_totalvoters"] + $vote;
        }



        foreach ($poll->options as $key => $option) {
            if (isset($votes[$option->id])) {
                $option->votes = $votes[$option->id];
                $option->option_percentage = round(100 * $option->votes / $result["pollq_totalvoters"]);
            } else {
                $option->votes = 0;
                $option->option_percentage = 0;
            }
            $result["options"][] = $option;
        }

        return $result;
    }
}
