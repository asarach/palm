<?php

namespace UniGen\Repositories\Language;
    
interface LanguageRepository
{

    /**
     * Create a model.
     *
     * @param array   $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create($data);

    /**
     * Update a model.
     *
     * @param int   $LanguageId
     * @param array   $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($LanguageId, $data);

    /**
     * Destroy a model.
     *
     * @param int   $LanguageId
     * @param boolean   $forced
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function destroy($LanguageId, $forced = false);
}