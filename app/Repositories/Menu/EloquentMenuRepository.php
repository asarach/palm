<?php

namespace App\Repositories\Menu;

use App\Models\Menu;
use App\Repositories\EloquentRepository;

class EloquentMenuRepository extends EloquentRepository implements MenuRepository
{
    public $model;

    public function __construct(Menu $model)
    {
        $this->model = $model;
    }

    public function create(array $data)
    {
        $item = new $this->model;
        $item->name = $data["name"];
        $item->location = $data["location"];
        $item->save();
        return $item;
    }

    public function update($id, array $data)
    {
        $item = $this->model->findOrFail($id);
        $item->name = $data["name"];
        $item->location = $data["location"];
        $item->save();
        return $item;
    }

    public function destroy($id)
    {
        $item = $this->model->findOrFail($id);
        foreach ($item->links as $link) {
            $link->menu()->dissociate();
            $link->save();
        }
        return $item->delete();
    }
}
