<?php

namespace App\Repositories\Pub;

use App\Repositories\EloquentRepository;
use App\Models\Pub;

class EloquentPubRepository extends EloquentRepository implements PubRepository
{
    public $model;

    public function __construct(Pub $model)
    {
        $this->model = $model;
    }

    public function create(array $data)
    {
        $item = new $this->model;

        $item->name = $data["name"];
        $item->order_num = $data["order_num"];
        $item->url = $data["url"];
        $item->start_date = $data["start_date"];
        $item->end_date = $data["end_date"];
        $item->status = $data["status"];

        $result = $item->save();

        if (!empty($data["image"])) {
            $item->image()->attach($data["image"]);
        }

        if ($result) {
            return $item;
        } else {
            return $result;
        }
    }

    public function update($id, array $data)
    {
        $item = $this->model->findOrFail($id);

        $item->name = $data["name"];
        $item->order_num = $data["order_num"];
        $item->url = $data["url"];
        $item->start_date = $data["start_date"];
        $item->end_date = $data["end_date"];
        $item->status = $data["status"];

        $result = $item->save();

        if (!empty($data["image"])) {
            $item->image()->detach();
            $item->image()->attach($data["image"]);
        }

        if ($result) {
            return $item;
        } else {
            return $result;
        }
    }

    public function destroy($id, $forced)
    {
        $item = $this->model->withTrashed()->findOrFail($id);
        if ($forced) {
            return $item->forceDelete();
        } else {
            return $item->delete();
        }
    }
}
