<?php

namespace App\Repositories\Comment;

use App\Models\Card;
use App\Models\Comment;
use Illuminate\Support\Facades\Auth;
use App\Repositories\EloquentRepository;

class EloquentCommentRepository extends EloquentRepository implements CommentRepository
{
    public $model;

    public function __construct(
        Comment $model,
        Card $card
    ) {
        $this->model = $model;
        $this->card = $card;
    }

    public function create(array $data, $commentable)
    {
        $item = new $this->model;

        $item->comment = seo_nofollow($data["content"]);
        $item->email = $data["email"];
        $item->name = $data["name"];
        $item->parent_id = $data["parent"];
        $item->status = 0;

        if (Auth::check()) {
            $item->user()->associate(Auth::user()->id);
        }
        if (!is_null($data["parent"])) {
            $item->parente()->associate($data["parent"]);
        }

        $item->save();

        $comment = $commentable->comments()->save($item);

        return $comment;
    }

    public function update($id, array $data)
    {
        $item = $this->model->findOrFail($id);
        $item->comment = seo_nofollow($data["content"]);
        $item->save();
        return $item;
    }


    public function destroy($id, $forced)
    {
        $item = $this->model->withTrashed()->findOrFail($id);
        if ($forced) {
            return $item->forceDelete();
        } else {
            return $item->delete();
        }
    }


    public function getModel($id, $type)
    {
        switch ($type) {
            case "card":
                $model = $this->card->find($id);
                break;
            default:
                $model = null;
                break;
        }

        return $model;
    }
}
