<?php

namespace App\Repositories\Page;

use App\Models\Page;
use Waavi\Translation\Models\Language;
use App\Repositories\EloquentRepository;

class EloquentPageRepository extends EloquentRepository implements PageRepository
{

    public $model;

    public function __construct(Page $model, Language $language)
    {
        $this->model = $model;
        $this->language = $language;
    }

    public function create(array $data)
    {
        $item = new $this->model;

        $item->name = $data["name"];
        $item->title = $data["title"];
        $item->slug = $data["slug"];
        $item->content = e($data["content"]);
        $item->description = $data["description"];
        $item->keywords = $data["keywords"];
        $item->status = $data["status"];

        if (!empty($data["language"]) and isset($data["language"]["id"])) {
            $item->language()->associate($data["language"]["id"]);
        }

        $item->save();

        return $item;
    }

    public function update($id, array $data)
    {
        $item = $this->model->findOrFail($id);

        $item->name = $data["name"];
        $item->title = $data["title"];
        $item->slug = $data["slug"];
        $item->content = e($data["content"]);
        $item->description = $data["description"];
        $item->keywords = $data["keywords"];
        $item->status = $data["status"];

        if (!empty($data["language"]) and isset($data["language"]["id"])) {
            $item->language()->associate($data["language"]["id"]);
        } else {
            $item->language()->dissociate();
        }

        $item->save();

        return $item;
    }


    public function destroy($id, $forced)
    {
        $item = $this->model->withTrashed()->findOrFail($id);
        if ($forced) {
            return $item->forceDelete();
        } else {
            return $item->delete();
        }
    }
}
