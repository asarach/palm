<?php

namespace  App\Repositories\Category;

use App\Repositories\EloquentRepository;
use App\Models\Category;

class EloquentCategoryRepository extends EloquentRepository implements CategoryRepository
{
    public $model;

    /**
     * Create a new Repository instance.
     *
     * @return void
     */
    public function __construct(Category $category)
    {
        $this->model = $category;
    }
    /**
     * Create a model.
     *
     * @param array   $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create($data)
    {
        $item = new $this->model;

        $item->name = $data["name"];
        $item->slug = $data["slug"];
        $item->order = $data["order"];
        $item->status = $data["status"];
        $item->description = $data["description"];
        $item->keywords = $data["keywords"];

        if (!is_null($data["parent_id"])) {
            $item->parente()->associate($data["parent_id"]);
        }

        $result = $item->save();

        $item->thumb()->attach($data["thumb"]);


        if ($result) {
            return $item;
        } else {
            return $result;
        }
    }

    /**
     * Update a model.
     *
     * @param int   $categoryId
     * @param array   $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($categoryId, $data)
    {
        $item = $this->model->findOrFail($categoryId);

        $item->name = $data["name"];
        $item->slug = $data["slug"];
        $item->order = $data["order"];
        $item->status = $data["status"];
        $item->description = $data["description"];
        $item->keywords = $data["keywords"];

        if (!is_null($data["parent_id"])) {
            $item->parente()->associate($data["parent_id"]);
        } else {
            $item->parente()->dissociate();
        }

        $result = $item->save();

        if ($data["thumb"]) {
            $item->thumb()->detach();
            $item->thumb()->attach($data["thumb"]);
        } else {
            $item->thumb()->detach();
        }


        if ($result) {
            return $item;
        } else {
            return $result;
        }
    }

    /**
     * Destroy a model.
     *
     * @param int   $categoryId
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function destroy($categoryId)
    {
        $item = $this->model->findOrFail($categoryId);
        return $item->delete();
    }
}
