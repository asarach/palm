<?php

namespace App\Repositories\Category;

interface CategoryRepository
{

    /**
     * Create a model.
     *
     * @param array   $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create($data);

    /**
     * Update a model.
     *
     * @param int   $categoryId
     * @param array   $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($categoryId, $data);

    /**
     * Destroy a model.
     *
     * @param int   $categoryId
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function destroy($categoryId);
}
