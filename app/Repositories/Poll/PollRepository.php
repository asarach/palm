<?php

namespace App\Repositories\Poll;

interface PollRepository
{

    /**
     * Create a model.
     *
     * @param array   $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create($data);

    /**
     * Update a model.
     *
     * @param int   $pollId
     * @param array   $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($pollId, $data);

    /**
     * Destroy a model.
     *
     * @param int   $pollId
     * @param boolean   $forced
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function destroy($pollId, $forced = false);
}
