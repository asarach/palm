<?php

namespace  App\Repositories\Poll;

use App\Models\Poll;
use App\Services\Search;
use App\Models\PollOption;
use Illuminate\Support\Facades\Auth;
use Waavi\Translation\Models\Language;
use App\Repositories\EloquentRepository;

class EloquentPollRepository extends EloquentRepository implements PollRepository
{
    public $model;


    /**
     * Create a new Repository instance.
     *
     * @return void
     */
    public function __construct(Poll $poll, PollOption $option, Language $language, Search $search)
    {
        $this->model = $poll;
        $this->language = $language;
        $this->option = $option;
        $this->search = $search;
    }
    /**
     * Create a model.
     *
     * @param array   $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create($data)
    {
        $item = new $this->model;

        $item->title = $data["title"];
        $item->slug = $data["slug"];
        $item->content = e($data["content"]);
        $item->excerpt = $data["excerpt"];
        $item->status = $data["status"];
        $item->sticky = $data["sticky"];
        $item->end_date = $data["end_date"];

        $item->user()->associate(Auth::user()->id);


        if (!empty($data["language"]) and isset($data["language"]["id"])) {
            $item->language()->associate($data["language"]["id"]);
        }

        $result = $item->save();

        $this->updateOptions($item, $data["options"]);

        if ($result) {
            return $item;
        } else {
            return $result;
        }
    }

    /**
     * Update a model.
     *
     * @param int   $pollId
     * @param array   $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($pollId, $data)
    {
        $item = $this->model->findOrFail($pollId);

        $item->title = $data["title"];
        $item->slug = $data["slug"];
        $item->content = e($data["content"]);
        $item->excerpt = $data["excerpt"];
        $item->status = $data["status"];
        $item->sticky = $data["sticky"];
        $item->end_date = $data["end_date"];

        if (!empty($data["language"]) and isset($data["language"]["id"])) {
            $item->language()->associate($data["language"]["id"]);
        } else {
            $item->language()->dissociate();
        }

        $result = $item->save();

        $this->updateOptions($item, $data["options"]);

        if ($result) {
            return $item;
        } else {
            return $result;
        }
    }

    /**
     * Destroy a model.
     *
     * @param int   $pollId
     * @param boolean   $forced
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function destroy($pollId, $forced = false)
    {
        $item = $this->model->withTrashed()->findOrFail($pollId);
        if ($forced) {
            return $item->forceDelete();
        } else {
            return $item->delete();
        }
    }



    /**
     * Update a model.
     *
     * @param int   $pollId
     * @param array   $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function updateOptions($poll, $options)
    {
        $ids = [];

        foreach ($options as  $item) {
            $option = $poll->options()->find($item["id"]);
            if ($option) {
                $option->title = $item["title"];
                $option->save();
            } else {
                $option = new $this->option;
                $option->title = $item["title"];
                $option->poll_id = $poll->id;
                $option->save();
            }

            $ids[] =  $option->id;
        }
        $poll->options()->whereNotIn("id", $ids)->delete();

        return $poll->options;
    }
}
