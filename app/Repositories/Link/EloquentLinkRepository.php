<?php

namespace App\Repositories\Link;

use App\Models\Link;
use Waavi\Translation\Models\Language;
use App\Repositories\EloquentRepository;
use Waavi\Translation\Models\Translation;

class EloquentLinkRepository extends EloquentRepository implements LinkRepository
{

    public $model;

    public function __construct(Link $model, Translation $translation, Language $language)
    {
        $this->model = $model;
        $this->translation = $translation;
        $this->language = $language;
    }

    public function create(array $data)
    {
        $item = new $this->model;

        $item->linkable_type = $data["linkable_type"];
        if ($data["linkable_type"] == "Url") {
            $item->url = $data["url"];
        } else {
            $item->linkable_id = $data["linkable_id"];
        }
        $item->order = $data["order"];
        $item->name = $data["name"];

        if (!is_null($data["parent_id"])) {
            $item->parente()->associate($data["parent_id"]);
        }
        if (!is_null($data["menu_id"])) {
            $item->menu()->associate($data["menu_id"]);
        }

        $item->save();

        $this->setTranslation($item);

        return $item;
    }

    public function update($id, array $data)
    {
        $item = $this->model->findOrFail($id);
        $item->linkable_type = $data["linkable_type"];
        if ($data["linkable_type"] == "Url") {
            $item->url = $data["url"];
        } else {
            $item->linkable_id = $data["linkable_id"];
        }
        $item->order = $data["order"];
        $item->name = $data["name"];

        if (!is_null($data["parent_id"])) {
            $item->parente()->associate($data["parent_id"]);
        } else {
            $item->parente()->dissociate();
        }

        if (!is_null($data["menu_id"])) {
            $item->menu()->associate($data["menu_id"]);
        } else {
            $item->menu()->dissociate();
        }

        $result = $item->save();

        $this->setTranslation($item);

        return $result;
    }

    public function destroy($id)
    {
        $item = $this->model->findOrFail($id);

        foreach ($item->kids as $kid) {
            $kid->parente()->dissociate();
            $kid->save();
        }

        return $item->delete();
    }

    public function setTranslation($item)
    {
        $exist = $this->translation->where("group", "menu")->where("item", "link-" . $item->id)->first();
        if ($exist) {
            $this->translation->where("group", "menu")->where("item", "link-" . $item->id)->update([
                "text" => $item->name
            ]);
        } else {
            $languages = $this->language->all();

            foreach ($languages as $key => $language) {
                $this->translation->create([
                    "group" => "menu",
                    "item" => "link-" . $item->id,
                    "locale" => $language->locale,
                    "text" => $item->name,
                ]);
            }
        }
    }
}
