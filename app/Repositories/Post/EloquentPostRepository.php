<?php

namespace  App\Repositories\Post;

use App\Repositories\EloquentRepository;
use App\Models\Post;
use App\Models\Category;
use App\Services\Search;
use Illuminate\Support\Facades\Auth;

class EloquentPostRepository extends EloquentRepository implements PostRepository
{
    public $model;
    
    
    /**
     * Create a new Repository instance.
     *
     * @return void
     */
    public function __construct(Post $post, Category $category, Search $search)
    {
        $this->model = $post;
        $this->category = $category;
        $this->search = $search;
        
    }
    /**
     * Create a model.
     *
     * @param array   $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create($data)
    {
        $item = new $this->model;

        $item->title = $data["title"];
        $item->slug = $data["slug"];
        $item->body = $data["body"];
        $item->excerpt = $data["excerpt"];
        $item->keywords = $data["keywords"];
        $item->status = $data["status"];
        $item->comment_status = $data["comment_status"];
        $item->client = $data["client"];
        $item->date = $data["date"];
        $item->skills = $data["skills"];
        $item->url = $data["url"];
        
        if (!empty($data["category"]) and isset($data["category"]["id"])) {
            $item->category()->associate($data["category"]["id"]);
        }
        $item->author()->associate(Auth::user()->id);

        
        $result = $item->save();

        foreach ($data["images_upload"] as $media ) {
            $this->attachMedia($item->id, "App\Models\Post", $media["val"]);
        }
        
        if ($result) {
            return $item;
        } else {
            return $result;
        }
    }
    
    /**
     * Update a model.
     *
     * @param int   $postId
     * @param array   $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($postId, $data)
    {
        $item = $this->model->findOrFail($postId);

        $item->title = $data["title"];
        $item->slug = $data["slug"];
        $item->body = $data["body"];
        $item->excerpt = $data["excerpt"];
        $item->keywords = $data["keywords"];
        $item->status = $data["status"];
        $item->comment_status = $data["comment_status"];
        $item->client = $data["client"];
        $item->date = $data["date"];
        $item->skills = $data["skills"];
        $item->url = $data["url"];
        
        if (!empty($data["category"]) and isset($data["category"]["id"])) {
            $item->category()->associate($data["category"]["id"]);
        } else {
            $item->category()->dissociate();
        }
        
        $result = $item->save();

        $this->detachMedia("post_images", $item->id, "App\Models\Post");
        foreach ($data["images_upload"] as $media ) {
            $this->attachMedia($item->id, "App\Models\Post", $media["val"]);
        }

        
        if ($result) {
            return $item;
        } else {
            return $result;
        }
    }
    
    /**
     * Destroy a model.
     *
     * @param int   $postId
     * @param boolean   $forced
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function destroy($postId, $forced = false)
    {
        $item = $this->model->withTrashed()->findOrFail($postId);
        if ($forced) {
            return $item->forceDelete();
        } else {
            return $item->delete();
        }
    }
    
    
}