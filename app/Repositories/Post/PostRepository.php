<?php

namespace App\Repositories\Post;
    
interface PostRepository
{

    /**
     * Create a model.
     *
     * @param array   $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create($data);

    /**
     * Update a model.
     *
     * @param int   $postId
     * @param array   $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($postId, $data);

    /**
     * Destroy a model.
     *
     * @param int   $postId
     * @param boolean   $forced
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function destroy($postId, $forced = false);
}