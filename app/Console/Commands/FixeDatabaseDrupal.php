<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DirectoryIterator;
use DB;
use Stichoza\GoogleTranslate\GoogleTranslate;
use Carbon\Carbon;
use App\Services\Upload;
use Cviebrock\EloquentSluggable\Services\SlugService;
use App\Models\Article;

class FixeDatabaseDrupal extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Upload $upload) {
        $this->upload = $upload;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
                DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        //$this->fixCategory();
        //$this->fixalbum();
        //$this->fixArticle();
        //$this->fixAdresse();
        //$this->fixAssociaction();
        //$this->fixEvenement();
        //$this->fixProjet();
        $this->fixPublication();
        //$this->fixVideo();
        //$this->fixPoll();
        

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    public function fixUser() {
        DB::connection('mysql')->table('users')->delete();
        $items = DB::connection('old_database')->table('users')->get();
        $bar = $this->output->createProgressBar(count($items));
        $bar->start();
        foreach ($items as $item) {
            DB::connection('mysql')->table('users')->insert([
                'id' => $item->uid,
                'name' => $item->name,
                'email' => $item->mail,
                'password' => $item->pass,
                'role' => 'admin',
                'created_at' => Carbon::createFromTimestamp($item->created)->toDateTimeString(),
                'updated_at' => Carbon::createFromTimestamp($item->created)->toDateTimeString(),
            ]);
            $bar->advance();
        }
        $bar->finish();
        $this->info('users table');
    }

    public function fixCategory() {
        DB::connection('mysql')->table('categories')->delete();
        $items = DB::connection('old_database')
        ->table('taxonomy_term_data')
        ->leftJoin('taxonomy_term_hierarchy', 'taxonomy_term_data.tid', '=', 'taxonomy_term_hierarchy.tid')
        ->select('taxonomy_term_data.*', 'taxonomy_term_hierarchy.parent as parent_id')
        ->get();

        $bar = $this->output->createProgressBar(count($items));
        $bar->start();

        foreach ($items as $item) {
            $url_alias = DB::connection('old_database')->table('url_alias')->where('source', 'taxonomy/term/' . $item->tid)->first();
            if ($url_alias) {
                $slug = str_replace('category/', '', $url_alias->alias);
            } else {
                $slug = '';
            }

            DB::connection('mysql')->table('categories')->insert([
                'id' => $item->tid,
                'name' => $item->name,
                'slug' => $slug,
                'order' => $item->weight,
                'status' => 1,
                'description' => $item->description,
                'keywords' => '',
                'parent_id' => $item->parent_id,
            ]);
            $bar->advance();
        }
        $bar->finish();
        $this->info('categories table');
    }

    public function fixArticle() {
        DB::connection('mysql')->table('articles')->delete();
        $items = DB::connection('old_database')->table('node')->where('type', 'article')
                ->get();
        $slug_count = 1;

        $bar = $this->output->createProgressBar(count($items));
        $bar->start();

        foreach ($items as $item) {
            $url_alias = DB::connection('old_database')->table('url_alias')->where('source', 'node/' . $item->nid)->first()->alias;
            $data_body = DB::connection('old_database')->table('field_data_body')->where('entity_id', $item->nid)->first();
            $taxonomy = DB::connection('old_database')->table('taxonomy_index')->where('nid', $item->nid)->first();

            if ($data_body) {
                if ($data_body->deleted) {
                    $delete = Carbon::createFromTimestamp($data_body->deleted)->toDateTimeString();
                } else {
                    $delete = null;
                }
                $body_value = $data_body->body_value;
                $body_summary = $data_body->body_summary;
            }else{
                $delete = null;
                $body_value = '';
                $body_summary = '';
            }
            $slug = str_replace('content/', '', $url_alias);
            $slug_exist = DB::connection('mysql')->table('articles')->where('slug', $slug)->first();
            if ($slug_exist) {
                $slug =  $slug . '-' .  $slug_count;
                $slug_count++;
            }


            DB::connection('mysql')->table('articles')->insert([
                'id' => $item->nid,
                'title' => $item->title,
                'slug' => $slug,
                'body' => e($body_value),
                'excerpt' => $body_summary,
                'keywords' => '',
                'status' => $item->status,
                'comment_status' => $item->comment,
                'promoted' => $item->promote,
                'sticky' => $item->sticky,
                'user_id' => 1,
                'category_id' => $taxonomy->tid,
                'language_id' => 1,
                //'total_views' => $item->totalcount,
                //'day_views' => $item->daycount,
                //'last_viewed_at' => Carbon::createFromTimestamp($item->timestamp)->toDateTimeString(),
                'created_at' => Carbon::createFromTimestamp($item->created)->toDateTimeString(),
                'updated_at' => Carbon::createFromTimestamp($item->changed)->toDateTimeString(),
                'deleted_at' => $delete,
            ]);

            $files = DB::connection('old_database')->table('file_usage')
                    ->where('file_usage.id', $item->nid)
                    ->get();

            foreach ($files as $file) {
                $media = DB::connection('old_database')->table('file_managed')
                    ->where('fid', $file->fid)
                    ->first();
                $path =  storage_path() . str_replace('public://', '/files/', $media->uri);

                try {
                    switch ($media->filemime) {
                        case 'image/png':
                            $media_name = $this->upload->importPostImage($path, 'article_thumb');
                            $type = 'article_thumb';
                            $is_media = true;
                            break;
                        case 'image/jpg':
                            $media_name = $this->upload->importPostImage($path, 'article_thumb');
                            $type = 'article_thumb';
                            $is_media = true;
                            break;
                        case 'image/jpeg':
                            $media_name = $this->upload->importPostImage($path, 'article_thumb');
                            $type = 'article_thumb';
                            $is_media = true;
                            break;
                        case 'image/gif':
                            $media_name = $this->upload->importPostImage($path, 'article_thumb');
                            $type = 'article_thumb';
                            $is_media = true;
                            break;
                        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                            $media_name = $this->upload->importPostFile($path, 'article_file');
                            $type = 'article_file';
                            $is_media = true;
                            break;
                        case 'application/pdf':
                            $media_name = $this->upload->importPostFile($path, 'article_file');
                            $type = 'article_file';
                            $is_media = true;
                            break;
                        case 'application/octet-stream':
                            $media_name = $this->upload->importPostFile($path, 'article_file');
                            $type = 'article_file';
                            $is_media = true;
                            break;
                        
                        default:
                            $is_media = false;
                            break;
                    }


                    if ($is_media) {
                        DB::connection('mysql')->table('medias')->insert([
                            'id' => $media->fid,
                            'title' => $media->filename,
                            'type' => $type,
                            'uri' => $media_name,
                            'status' => $media->status,
                            'created_at' => Carbon::createFromTimestamp($media->timestamp)->toDateTimeString(),
                            'updated_at' => Carbon::createFromTimestamp($media->timestamp)->toDateTimeString(),
                        ]);
                        DB::connection('mysql')->table('mediable')->insert([
                            'media_id' => $media->fid,
                            'mediable_id' => $item->nid,
                            'mediable_type' => 'App\Models\Article',
                        ]);
                    }
                    
                } catch (\Exception $e) {
                    //dd($e);
                    $this->error('Media error'. $media->fid);
                }
            }
            $bar->advance();
        }
        $bar->finish();
        $this->info('articles table');
    }
    public function fixAdresse() {
        DB::connection('mysql')->table('adresses')->delete();
        $items = DB::connection('old_database')->table('node')->where('type', 'adresse')
                ->get();
                $slug_count = 1;
        //dd($items->count());

        $bar = $this->output->createProgressBar(count($items));
        $bar->start();

        foreach ($items as $item) {
            $url_alias = DB::connection('old_database')->table('url_alias')->where('source', 'node/' . $item->nid)->first()->alias;
            $data_body = DB::connection('old_database')->table('field_data_body')->where('entity_id', $item->nid)->first();
            $taxonomy = DB::connection('old_database')->table('taxonomy_index')->where('nid', $item->nid)->first();

            if ($data_body) {
                if ($data_body->deleted) {
                    $delete = Carbon::createFromTimestamp($data_body->deleted)->toDateTimeString();
                } else {
                    $delete = null;
                }
                $body_value = $data_body->body_value;
                $body_summary = $data_body->body_summary;
            }else{
                $delete = null;
                $body_value = '';
                $body_summary = '';
            }
            $slug = str_replace('content/', '', $url_alias);
            $slug_exist = DB::connection('mysql')->table('articles')->where('slug', $slug)->first();
            if ($slug_exist) {
                $slug =  $slug . '-' .  $slug_count;
                $slug_count++;
            }


            DB::connection('mysql')->table('articles')->insert([
                'id' => $item->nid,
                'title' => $item->title,
                'slug' => $slug,
                'body' => e($body_value),
                'excerpt' => $body_summary,
                'keywords' => '',
                'status' => $item->status,
                'comment_status' => $item->comment,
                'promoted' => $item->promote,
                'sticky' => $item->sticky,
                'user_id' => 1,
                'category_id' => $taxonomy->tid,
                'language_id' => 1,
                //'total_views' => $item->totalcount,
                //'day_views' => $item->daycount,
                //'last_viewed_at' => Carbon::createFromTimestamp($item->timestamp)->toDateTimeString(),
                'created_at' => Carbon::createFromTimestamp($item->created)->toDateTimeString(),
                'updated_at' => Carbon::createFromTimestamp($item->changed)->toDateTimeString(),
                'deleted_at' => $delete,
            ]);

            $files = DB::connection('old_database')->table('file_usage')
                    ->where('file_usage.id', $item->nid)
                    ->get();

            foreach ($files as $file) {
                $media = DB::connection('old_database')->table('file_managed')
                    ->where('fid', $file->fid)
                    ->first();
                $path =  storage_path() . str_replace('public://', '/files/', $media->uri);

                try {
                    switch ($media->filemime) {
                        case 'image/png':
                            $media_name = $this->upload->importPostImage($path, 'article_thumb');
                            $type = 'article_thumb';
                            $is_media = true;
                            break;
                        case 'image/jpg':
                            $media_name = $this->upload->importPostImage($path, 'article_thumb');
                            $type = 'article_thumb';
                            $is_media = true;
                            break;
                        case 'image/jpeg':
                            $media_name = $this->upload->importPostImage($path, 'article_thumb');
                            $type = 'article_thumb';
                            $is_media = true;
                            break;
                        case 'image/gif':
                            $media_name = $this->upload->importPostImage($path, 'article_thumb');
                            $type = 'article_thumb';
                            $is_media = true;
                            break;
                        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                            $media_name = $this->upload->importPostFile($path, 'article_file');
                            $type = 'article_file';
                            $is_media = true;
                            break;
                        case 'application/pdf':
                            $media_name = $this->upload->importPostFile($path, 'article_file');
                            $type = 'article_file';
                            $is_media = true;
                            break;
                        case 'application/octet-stream':
                            $media_name = $this->upload->importPostFile($path, 'article_file');
                            $type = 'article_file';
                            $is_media = true;
                            break;
                        
                        default:
                            $is_media = false;
                            break;
                    }


                    if ($is_media) {
                        DB::connection('mysql')->table('medias')->insert([
                            'id' => $media->fid,
                            'title' => $media->filename,
                            'type' => $type,
                            'uri' => $media_name,
                            'status' => $media->status,
                            'created_at' => Carbon::createFromTimestamp($media->timestamp)->toDateTimeString(),
                            'updated_at' => Carbon::createFromTimestamp($media->timestamp)->toDateTimeString(),
                        ]);
                        DB::connection('mysql')->table('mediable')->insert([
                            'media_id' => $media->fid,
                            'mediable_id' => $item->nid,
                            'mediable_type' => 'App\Models\Article',
                        ]);
                    }
                    
                } catch (\Exception $e) {
                    $this->error('Media error'. $media->fid);
                }
            }
            $bar->advance();
        }
        $bar->finish();
        $this->info('articles table');
    }
    public function fixAssociaction() {
        DB::connection('mysql')->table('associactions')->delete();
        $items = DB::connection('old_database')->table('node')->where('type', 'associaction')
                ->get();
        $slug_count = 1;
        //dd($items->count());

        $bar = $this->output->createProgressBar(count($items));
        $bar->start();

        foreach ($items as $item) {
            $url_alias = DB::connection('old_database')->table('url_alias')->where('source', 'node/' . $item->nid)->first()->alias;
            $data_body = DB::connection('old_database')->table('field_data_body')->where('entity_id', $item->nid)->first();

            if ($data_body) {
                if ($data_body->deleted) {
                    $delete = Carbon::createFromTimestamp($data_body->deleted)->toDateTimeString();
                } else {
                    $delete = null;
                }
                $body_value = $data_body->body_value;
                $body_summary = $data_body->body_summary;
            }else{
                $delete = null;
                $body_value = '';
                $body_summary = '';
            }
            $slug = str_replace('content/', '', $url_alias);
            $slug_exist = DB::connection('mysql')->table('associactions')->where('slug', $slug)->first();
            if ($slug_exist) {
                $slug =  $slug . '-' .  $slug_count;
                $slug_count++;
            }


            DB::connection('mysql')->table('associactions')->insert([
                'id' => $item->nid,
                'title' => $item->title,
                'slug' => $slug,
                'body' => e($body_value),
                'excerpt' => $body_summary,
                'keywords' => '',
                'status' => $item->status,
                'comment_status' => $item->comment,
                'promoted' => $item->promote,
                'sticky' => $item->sticky,
                'user_id' => 1,
                'language_id' => 1,
                'created_at' => Carbon::createFromTimestamp($item->created)->toDateTimeString(),
                'updated_at' => Carbon::createFromTimestamp($item->changed)->toDateTimeString(),
                'deleted_at' => $delete,
            ]);

            $files = DB::connection('old_database')->table('file_usage')
                    ->where('file_usage.id', $item->nid)
                    ->get();

            foreach ($files as $file) {
                $media = DB::connection('old_database')->table('file_managed')
                    ->where('fid', $file->fid)
                    ->first();
                $path =  storage_path() . str_replace('public://', '/files/', $media->uri);

                try {
                    switch ($media->filemime) {
                        case 'image/png':
                            $media_name = $this->upload->importPostImage($path, 'associaction_thumb');
                            $type = 'associaction_thumb';
                            $is_media = true;
                            break;
                        case 'image/jpg':
                            $media_name = $this->upload->importPostImage($path, 'associaction_thumb');
                            $type = 'associaction_thumb';
                            $is_media = true;
                            break;
                        case 'image/jpeg':
                            $media_name = $this->upload->importPostImage($path, 'associaction_thumb');
                            $type = 'associaction_thumb';
                            $is_media = true;
                            break;
                        case 'image/gif':
                            $media_name = $this->upload->importPostImage($path, 'associaction_thumb');
                            $type = 'associaction_thumb';
                            $is_media = true;
                            break;
                        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                            $media_name = $this->upload->importPostFile($path, 'associaction_file');
                            $type = 'associaction_file';
                            $is_media = true;
                            break;
                        case 'application/pdf':
                            $media_name = $this->upload->importPostFile($path, 'associaction_file');
                            $type = 'associaction_file';
                            $is_media = true;
                            break;
                        case 'application/octet-stream':
                            $media_name = $this->upload->importPostFile($path, 'associaction_file');
                            $type = 'associaction_file';
                            $is_media = true;
                            break;
                        
                        default:
                            $is_media = false;
                            break;
                    }


                    if ($is_media) {
                        DB::connection('mysql')->table('medias')->insert([
                            'id' => $media->fid,
                            'title' => $media->filename,
                            'type' => $type,
                            'uri' => $media_name,
                            'status' => $media->status,
                            'created_at' => Carbon::createFromTimestamp($media->timestamp)->toDateTimeString(),
                            'updated_at' => Carbon::createFromTimestamp($media->timestamp)->toDateTimeString(),
                        ]);
                        DB::connection('mysql')->table('mediable')->insert([
                            'media_id' => $media->fid,
                            'mediable_id' => $item->nid,
                            'mediable_type' => 'App\Associaction',
                        ]);
                    }
                    
                } catch (\Exception $e) {
                    //($e);
                    $this->error('Media error'. $media->fid);
                }
            }
            $bar->advance();
        }
        $bar->finish();
        $this->info('associactions table');
    }


    public function fixEvenement() {
        DB::connection('mysql')->table('evenements')->delete();
        $items = DB::connection('old_database')->table('node')->where('type', 'evenement')
                ->get();
        $slug_count = 1;

        $bar = $this->output->createProgressBar(count($items));
        $bar->start();

        foreach ($items as $item) {
            $url_alias = DB::connection('old_database')->table('url_alias')->where('source', 'node/' . $item->nid)->first()->alias;
            $data_body = DB::connection('old_database')->table('field_data_body')->where('entity_id', $item->nid)->first();
            $evenemen_date = DB::connection('old_database')->table('field_data_field_evenemen_date')->where('entity_id', $item->nid)->first();

            if ($data_body) {
                if ($data_body->deleted) {
                    $delete = Carbon::createFromTimestamp($data_body->deleted)->toDateTimeString();
                } else {
                    $delete = null;
                }
                $body_value = $data_body->body_value;
                $body_summary = $data_body->body_summary;
            }else{
                $delete = null;
                $body_value = '';
                $body_summary = '';
            }
            $slug = str_replace('content/', '', $url_alias);
            $slug_exist = DB::connection('mysql')->table('evenements')->where('slug', $slug)->first();
            if ($slug_exist) {
                $slug =  $slug . '-' .  $slug_count;
                $slug_count++;
            }


            DB::connection('mysql')->table('evenements')->insert([
                'id' => $item->nid,
                'title' => $item->title,
                'slug' => $slug,
                'body' => e($body_value),
                'excerpt' => $body_summary,
                'keywords' => '',
                'status' => $item->status,
                'comment_status' => $item->comment,
                'promoted' => $item->promote,
                'sticky' => $item->sticky,
                'language_id' => 1,
                'start_date' => $evenemen_date->field_evenemen_date_value,
                'end_date' => $evenemen_date->field_evenemen_date_value2,
                'place' => '',
                'guests' => '',
                'access' => '',
                'created_at' => Carbon::createFromTimestamp($item->created)->toDateTimeString(),
                'updated_at' => Carbon::createFromTimestamp($item->changed)->toDateTimeString(),
                'deleted_at' => $delete,
            ]);

            $files = DB::connection('old_database')->table('file_usage')
                    ->where('file_usage.id', $item->nid)
                    ->get();

            foreach ($files as $file) {
                $media = DB::connection('old_database')->table('file_managed')
                    ->where('fid', $file->fid)
                    ->first();
                $path =  storage_path() . str_replace('public://', '/files/', $media->uri);

                try {
                    switch ($media->filemime) {
                        case 'image/png':
                            $media_name = $this->upload->importPostImage($path, 'evenement_image');
                            $type = 'evenement_image';
                            $is_media = true;
                            break;
                        case 'image/jpg':
                            $media_name = $this->upload->importPostImage($path, 'evenement_image');
                            $type = 'evenement_image';
                            $is_media = true;
                            break;
                        case 'image/jpeg':
                            $media_name = $this->upload->importPostImage($path, 'evenement_image');
                            $type = 'evenement_image';
                            $is_media = true;
                            break;
                        case 'image/gif':
                            $media_name = $this->upload->importPostImage($path, 'evenement_image');
                            $type = 'evenement_image';
                            $is_media = true;
                            break;
                        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                            $media_name = $this->upload->importPostFile($path, 'evenement_document');
                            $type = 'evenement_document';
                            $is_media = true;
                            break;
                        case 'application/pdf':
                            $media_name = $this->upload->importPostFile($path, 'evenement_document');
                            $type = 'evenement_document';
                            $is_media = true;
                            break;
                        case 'application/octet-stream':
                            $media_name = $this->upload->importPostFile($path, 'evenement_document');
                            $type = 'evenement_document';
                            $is_media = true;
                            break;
                        
                        default:
                            $is_media = false;
                            break;
                    }


                    if ($is_media) {
                        DB::connection('mysql')->table('medias')->insert([
                            'id' => $media->fid,
                            'title' => $media->filename,
                            'type' => $type,
                            'uri' => $media_name,
                            'status' => $media->status,
                            'created_at' => Carbon::createFromTimestamp($media->timestamp)->toDateTimeString(),
                            'updated_at' => Carbon::createFromTimestamp($media->timestamp)->toDateTimeString(),
                        ]);
                        DB::connection('mysql')->table('mediable')->insert([
                            'media_id' => $media->fid,
                            'mediable_id' => $item->nid,
                            'mediable_type' => 'App\Evenement',
                        ]);
                    }
                    
                } catch (\Exception $e) {
                    //dd($e);
                    $this->error('Media error'. $media->fid);
                }
            }
            $bar->advance();
        }
        $bar->finish();
        $this->info('evenements table');
    }

    public function fixProjet() {
        DB::connection('mysql')->table('projets')->delete();
        $items = DB::connection('old_database')->table('node')->where('type', 'projet')
                ->get();
        $slug_count = 1;

        $bar = $this->output->createProgressBar(count($items));
        $bar->start();

        foreach ($items as $item) {
            $url_alias = DB::connection('old_database')->table('url_alias')->where('source', 'node/' . $item->nid)->first()->alias;
            $data_body = DB::connection('old_database')->table('field_data_body')->where('entity_id', $item->nid)->first();
            $taxonomy = DB::connection('old_database')->table('taxonomy_index')->where('nid', $item->nid)->first();

            if ($taxonomy) {
                $category_id = $taxonomy->tid;
            }else{
                $category_id = null;
            }

            if ($data_body) {
                if ($data_body->deleted) {
                    $delete = Carbon::createFromTimestamp($data_body->deleted)->toDateTimeString();
                } else {
                    $delete = null;
                }
                $body_value = $data_body->body_value;
                $body_summary = $data_body->body_summary;
            }else{
                $delete = null;
                $body_value = '';
                $body_summary = '';
            }
            $slug = str_replace('content/', '', $url_alias);
            $slug_exist = DB::connection('mysql')->table('projets')->where('slug', $slug)->first();
            if ($slug_exist) {
                $slug =  $slug . '-' .  $slug_count;
                $slug_count++;
            }


            DB::connection('mysql')->table('projets')->insert([
                'id' => $item->nid,
                'title' => $item->title,
                'slug' => $slug,
                'body' =>e(e($body_value)),
                'excerpt' => $body_summary,
                'keywords' => '',
                'status' => $item->status,
                'comment_status' => $item->comment,
                'promoted' => $item->promote,
                'sticky' => $item->sticky,
                'user_id' => 1,
                'category_id' => $category_id,
                'language_id' => 1,
                'created_at' => Carbon::createFromTimestamp($item->created)->toDateTimeString(),
                'updated_at' => Carbon::createFromTimestamp($item->changed)->toDateTimeString(),
                'deleted_at' => $delete,
            ]);

            $files = DB::connection('old_database')->table('file_usage')
                    ->where('file_usage.id', $item->nid)
                    ->get();

            foreach ($files as $file) {
                $media = DB::connection('old_database')->table('file_managed')
                    ->where('fid', $file->fid)
                    ->first();
                $path =  storage_path() . str_replace('public://', '/files/', $media->uri);

                try {
                    switch ($media->filemime) {
                        case 'image/png':
                            $media_name = $this->upload->importPostImage($path, 'projet_thumb');
                            $type = 'projet_thumb';
                            $is_media = true;
                            break;
                        case 'image/jpg':
                            $media_name = $this->upload->importPostImage($path, 'projet_thumb');
                            $type = 'projet_thumb';
                            $is_media = true;
                            break;
                        case 'image/jpeg':
                            $media_name = $this->upload->importPostImage($path, 'projet_thumb');
                            $type = 'projet_thumb';
                            $is_media = true;
                            break;
                        case 'image/gif':
                            $media_name = $this->upload->importPostImage($path, 'projet_thumb');
                            $type = 'projet_thumb';
                            $is_media = true;
                            break;
                        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                            $media_name = $this->upload->importPostFile($path, 'projet_file');
                            $type = 'projet_file';
                            $is_media = true;
                            break;
                        case 'application/pdf':
                            $media_name = $this->upload->importPostFile($path, 'projet_file');
                            $type = 'projet_file';
                            $is_media = true;
                            break;
                        case 'application/octet-stream':
                            $media_name = $this->upload->importPostFile($path, 'projet_file');
                            $type = 'projet_file';
                            $is_media = true;
                            break;
                        
                        default:
                            $is_media = false;
                            break;
                    }


                    if ($is_media) {
                        DB::connection('mysql')->table('medias')->insert([
                            'id' => $media->fid,
                            'title' => $media->filename,
                            'type' => $type,
                            'uri' => $media_name,
                            'status' => $media->status,
                            'created_at' => Carbon::createFromTimestamp($media->timestamp)->toDateTimeString(),
                            'updated_at' => Carbon::createFromTimestamp($media->timestamp)->toDateTimeString(),
                        ]);
                        DB::connection('mysql')->table('mediable')->insert([
                            'media_id' => $media->fid,
                            'mediable_id' => $item->nid,
                            'mediable_type' => 'App\Projet',
                        ]);
                    }
                    
                } catch (\Exception $e) {
                    //dd($e);
                    $this->error('Media error'. $media->fid);
                }
            }
            $bar->advance();
        }
        $bar->finish();
        $this->info('projets table');
    }
    public function fixPublication() {
        //DB::connection('mysql')->table('publications')->delete();
        $items = DB::connection('old_database')->table('node')->where('nid', '3242')
                ->get();
        $slug_count = 1;

        $bar = $this->output->createProgressBar(count($items));
        $bar->start();

        foreach ($items as $item) {
            $url_alias = DB::connection('old_database')->table('url_alias')->where('source', 'node/' . $item->nid)->first()->alias;
            $data_body = DB::connection('old_database')->table('field_data_body')->where('entity_id', $item->nid)->first();
            $taxonomy = DB::connection('old_database')->table('taxonomy_index')->where('nid', $item->nid)->first();

            if ($taxonomy) {
                $category_id = $taxonomy->tid;
            }else{
                $category_id = null;
            }

            if ($data_body) {
                if ($data_body->deleted) {
                    $delete = Carbon::createFromTimestamp($data_body->deleted)->toDateTimeString();
                } else {
                    $delete = null;
                }
                $body_value = $data_body->body_value;
                $body_summary = $data_body->body_summary;
            }else{
                $delete = null;
                $body_value = '';
                $body_summary = '';
            }
            $slug = str_replace('content/', '', $url_alias);
            $slug_exist = DB::connection('mysql')->table('publications')->where('slug', $slug)->first();
            if ($slug_exist) {
                $slug =  $slug . '-' .  $slug_count;
                $slug_count++;
            }
            /*

            DB::connection('mysql')->table('publications')->insert([
                'id' => $item->nid,
                'title' => $item->title,
                'slug' => $slug,
                'body' => e($body_value),
                'excerpt' => $body_summary,
                'keywords' => '',
                'status' => $item->status,
                'comment_status' => $item->comment,
                'promoted' => $item->promote,
                'sticky' => $item->sticky,
                'user_id' => 1,
                'category_id' => $category_id,
                'language_id' => 1,
                'created_at' => Carbon::createFromTimestamp($item->created)->toDateTimeString(),
                'updated_at' => Carbon::createFromTimestamp($item->changed)->toDateTimeString(),
                'deleted_at' => $delete,
            ]);
            */

            $files = DB::connection('old_database')->table('file_usage')
                    ->where('file_usage.id', $item->nid)
                    ->get();
                    dd($files);

            foreach ($files as $file) {
                $media = DB::connection('old_database')->table('file_managed')
                    ->where('fid', $file->fid)
                    ->first();
                $path =  storage_path() . str_replace('public://', '/files/', $media->uri);

                try {
                    switch ($media->filemime) {
                        case 'image/png':
                            $media_name = $this->upload->importPostImage($path, 'publication_thumb');
                            $type = 'publication_thumb';
                            $is_media = true;
                            break;
                        case 'image/jpg':
                            $media_name = $this->upload->importPostImage($path, 'publication_thumb');
                            $type = 'publication_thumb';
                            $is_media = true;
                            break;
                        case 'image/jpeg':
                            $media_name = $this->upload->importPostImage($path, 'publication_thumb');
                            $type = 'publication_thumb';
                            $is_media = true;
                            break;
                        case 'image/gif':
                            $media_name = $this->upload->importPostImage($path, 'publication_thumb');
                            $type = 'publication_thumb';
                            $is_media = true;
                            break;
                        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                            $media_name = $this->upload->importPostFile($path, 'publication_file');
                            $type = 'publication_file';
                            $is_media = true;
                            break;
                        case 'application/pdf':
                            $media_name = $this->upload->importPostFile($path, 'publication_file');
                            $type = 'publication_file';
                            $is_media = true;
                            break;
                        case 'application/octet-stream':
                            $media_name = $this->upload->importPostFile($path, 'publication_file');
                            $type = 'publication_file';
                            $is_media = true;
                            break;
                        
                        default:
                            $is_media = false;
                            break;
                    }


                    if ($is_media) {
                        DB::connection('mysql')->table('medias')->insert([
                            'id' => $media->fid,
                            'title' => $media->filename,
                            'type' => $type,
                            'uri' => $media_name,
                            'status' => $media->status,
                            'created_at' => Carbon::createFromTimestamp($media->timestamp)->toDateTimeString(),
                            'updated_at' => Carbon::createFromTimestamp($media->timestamp)->toDateTimeString(),
                        ]);
                        DB::connection('mysql')->table('mediable')->insert([
                            'media_id' => $media->fid,
                            'mediable_id' => $item->nid,
                            'mediable_type' => 'App\Models\Publication',
                        ]);
                    }
                    
                } catch (\Exception $e) {
                    //dd($e);
                    $this->error('Media error'. $media->fid);
                }
            }
            $bar->advance();
        }
        $bar->finish();
        $this->info('publications table');
    }
    

    public function fixVideo() {
        DB::connection('mysql')->table('videos')->delete();
        $items = DB::connection('old_database')->table('node')->where('type', 'videos_gallery')
                ->get();
        $slug_count = 1;

        $bar = $this->output->createProgressBar(count($items));
        $bar->start();

        foreach ($items as $item) {
            $url_alias = DB::connection('old_database')->table('url_alias')->where('source', 'node/' . $item->nid)->first()->alias;
            $data_body = DB::connection('old_database')->table('field_data_body')->where('entity_id', $item->nid)->first();
            $field_video = DB::connection('old_database')->table('field_data_field_video')->where('entity_id', $item->nid)->first();
            $taxonomy = DB::connection('old_database')->table('taxonomy_index')->where('nid', $item->nid)->first();

            if ($taxonomy) {
                $category_id = $taxonomy->tid;
            }else{
                $category_id = null;
            }
            if ($field_video) {
                $video_link = $field_video->field_video_input;
            }else{
                $video_link = null;
            }

            if ($data_body) {
                if ($data_body->deleted) {
                    $delete = Carbon::createFromTimestamp($data_body->deleted)->toDateTimeString();
                } else {
                    $delete = null;
                }
                $body_value = $data_body->body_value;
                $body_summary = $data_body->body_summary;
            }else{
                $delete = null;
                $body_value = '';
                $body_summary = '';
            }
            $slug = str_replace('content/', '', $url_alias);
            $slug_exist = DB::connection('mysql')->table('videos')->where('slug', $slug)->first();
            if ($slug_exist) {
                $slug =  $slug . '-' .  $slug_count;
                $slug_count++;
            }


            DB::connection('mysql')->table('videos')->insert([
                'id' => $item->nid,
                'title' => $item->title,
                'slug' => $slug,
                'body' => e($body_value),
                'excerpt' => $body_summary,
                'keywords' => '',
                'status' => $item->status,
                'comment_status' => $item->comment,
                'promoted' => $item->promote,
                'sticky' => $item->sticky,
                'user_id' => 1,
                'category_id' => $category_id,
                'video_link' => $video_link,
                'language_id' => 1,
                'created_at' => Carbon::createFromTimestamp($item->created)->toDateTimeString(),
                'updated_at' => Carbon::createFromTimestamp($item->changed)->toDateTimeString(),
                'deleted_at' => $delete,
            ]);

            $files = DB::connection('old_database')->table('file_usage')
                    ->where('file_usage.id', $item->nid)
                    ->get();

            foreach ($files as $file) {
                $media = DB::connection('old_database')->table('file_managed')
                    ->where('fid', $file->fid)
                    ->first();
                $path =  storage_path() . str_replace('public://', '/files/', $media->uri);

                try {
                    switch ($media->filemime) {
                        case 'image/png':
                            $media_name = $this->upload->importPostImage($path, 'video_thumb');
                            $type = 'video_thumb';
                            $is_media = true;
                            break;
                        case 'image/jpg':
                            $media_name = $this->upload->importPostImage($path, 'video_thumb');
                            $type = 'video_thumb';
                            $is_media = true;
                            break;
                        case 'image/jpeg':
                            $media_name = $this->upload->importPostImage($path, 'video_thumb');
                            $type = 'video_thumb';
                            $is_media = true;
                            break;
                        case 'image/gif':
                            $media_name = $this->upload->importPostImage($path, 'video_thumb');
                            $type = 'video_thumb';
                            $is_media = true;
                            break;
                        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                            $media_name = $this->upload->importPostFile($path, 'video_file');
                            $type = 'video_file';
                            $is_media = true;
                            break;
                        case 'application/pdf':
                            $media_name = $this->upload->importPostFile($path, 'video_file');
                            $type = 'video_file';
                            $is_media = true;
                            break;
                        case 'application/octet-stream':
                            $media_name = $this->upload->importPostFile($path, 'video_file');
                            $type = 'video_file';
                            $is_media = true;
                            break;
                        
                        default:
                            $is_media = false;
                            break;
                    }


                    if ($is_media) {
                        DB::connection('mysql')->table('medias')->insert([
                            'id' => $media->fid,
                            'title' => $media->filename,
                            'type' => $type,
                            'uri' => $media_name,
                            'status' => $media->status,
                            'created_at' => Carbon::createFromTimestamp($media->timestamp)->toDateTimeString(),
                            'updated_at' => Carbon::createFromTimestamp($media->timestamp)->toDateTimeString(),
                        ]);
                        DB::connection('mysql')->table('mediable')->insert([
                            'media_id' => $media->fid,
                            'mediable_id' => $item->nid,
                            'mediable_type' => 'App\Video',
                        ]);
                    }
                    
                } catch (\Exception $e) {
                    //dd($e);
                    $this->error('Media error'. $media->fid);
                }
            }
            $bar->advance();
        }
        $bar->finish();
        $this->info('videos table');
    }
    

    public function fixPoll() {
        DB::connection('mysql')->table('polls')->delete();
        $items = DB::connection('old_database')->table('node')->where('type', 'poll')
                ->leftJoin('field_data_body', 'node.nid', '=', 'field_data_body.entity_id')
                ->select('node.*', 'field_data_body.bundle', 'field_data_body.deleted', 'field_data_body.delta', 'field_data_body.body_value', 'field_data_body.body_summary', 'field_data_body.body_format')
                ->get();
                $slug_count = 1;
        //dd($items->count());
        $bar = $this->output->createProgressBar(count($items));
        $bar->start();

        foreach ($items as $item) {
            $url_alias = DB::connection('old_database')->table('url_alias')->where('source', 'node/' . $item->nid)->first()->alias;

            if ($item->deleted) {
                $delete = Carbon::createFromTimestamp($item->deleted)->toDateTimeString();
            } else {
                $delete = null;
            }
            $slug = str_replace('content/', '', $url_alias);
            $slug_exist = DB::connection('mysql')->table('polls')->where('slug', $slug)->first();
            if ($slug_exist) {
                $slug =  $slug . '-' .  $slug_count;
                $slug_count++;
            }


            DB::connection('mysql')->table('polls')->insert([
                'id' => $item->nid,
                'title' => $item->title,
                'slug' => $slug,
                'body' => $item->body_value,
                'excerpt' => $item->body_summary,
                'keywords' => '',
                'status' => $item->status,
                'comment_status' => $item->comment,
                'promoted' => $item->promote,
                'sticky' => $item->sticky,
                'user_id' => $item->uid,
                //'total_views' => $item->totalcount,
                //'day_views' => $item->daycount,
                //'last_viewed_at' => Carbon::createFromTimestamp($item->timestamp)->toDateTimeString(),
                'created_at' => Carbon::createFromTimestamp($item->created)->toDateTimeString(),
                'updated_at' => Carbon::createFromTimestamp($item->changed)->toDateTimeString(),
                'deleted_at' => $delete,
            ]);
            $medias = DB::connection('old_database')->table('file_managed')
                    ->join('file_usage', 'file_managed.fid', '=', 'file_usage.fid')
                    ->where('file_usage.id', $item->nid)
                    ->get();
            foreach ($medias as $media) {
                $path =  storage_path() . str_replace('public://', '/files/', $media->uri);

                try {
                    $media_name = $this->upload->importPostImage($path, 'polls');
                    DB::connection('mysql')->table('medias')->insert([
                        'id' => $media->fid,
                        'title' => $media->filename,
                        'uri' => $media_name,
                        'status' => $media->status,
                        'created_at' => Carbon::createFromTimestamp($media->timestamp)->toDateTimeString(),
                        'updated_at' => Carbon::createFromTimestamp($media->timestamp)->toDateTimeString(),
                    ]);
                    DB::connection('mysql')->table('mediable')->insert([
                        'media_id' => $media->fid,
                        'mediable_id' => $item->nid,
                        'mediable_type' => 'App\Models\Article',
                    ]);
                } catch (\Exception $e) {

                }
            }
            $bar->advance();
        }
        $bar->finish();
        $this->info('polls table');
    }


    public function fixalbum() {
        DB::connection('mysql')->table('albums')->delete();
        DB::connection('mysql')->table('medias')->delete();
        DB::connection('mysql')->table('mediable')->delete();
        $items = DB::connection('old_database')->table('node')->where('type', 'image_gallery')
                ->get();
        $slug_count = 1;

        $bar = $this->output->createProgressBar(count($items));
        $bar->start();

        foreach ($items as $item) {
            $url_alias = DB::connection('old_database')->table('url_alias')->where('source', 'node/' . $item->nid)->first()->alias;
            $data_body = DB::connection('old_database')->table('field_data_body')->where('entity_id', $item->nid)->first();
            $taxonomy = DB::connection('old_database')->table('taxonomy_index')->where('nid', $item->nid)->first();

            if ($taxonomy) {
                $category_id = $taxonomy->tid;
            }else{
                $category_id = null;
            }

            if ($data_body) {
                if ($data_body->deleted) {
                    $delete = Carbon::createFromTimestamp($data_body->deleted)->toDateTimeString();
                } else {
                    $delete = null;
                }
                $body_value = $data_body->body_value;
                $body_summary = $data_body->body_summary;
            }else{
                $delete = null;
                $body_value = '';
                $body_summary = '';
            }
            $slug = str_replace('content/', '', $url_alias);
            $slug_exist = DB::connection('mysql')->table('albums')->where('slug', $slug)->first();
            if ($slug_exist) {
                $slug =  $slug . '-' .  $slug_count;
                $slug_count++;
            }


            DB::connection('mysql')->table('albums')->insert([
                'id' => $item->nid,
                'title' => $item->title,
                'slug' => $slug,
                'body' => e($body_value),
                'excerpt' => $body_summary,
                'keywords' => '',
                'status' => $item->status,
                'comment_status' => $item->comment,
                'promoted' => $item->promote,
                'sticky' => $item->sticky,
                'user_id' => 1,
                'language_id' => 1,
                'created_at' => Carbon::createFromTimestamp($item->created)->toDateTimeString(),
                'updated_at' => Carbon::createFromTimestamp($item->changed)->toDateTimeString(),
                'deleted_at' => $delete,
            ]);

            $files = DB::connection('old_database')->table('file_usage')
                    ->where('file_usage.id', $item->nid)
                    ->get();

            foreach ($files as $file) {
                $media = DB::connection('old_database')->table('file_managed')
                    ->where('fid', $file->fid)
                    ->first();
                $path =  storage_path() . str_replace('public://', '/files/', $media->uri);

                try {
                    switch ($media->filemime) {
                        case 'image/png':
                            $media_name = $this->upload->importPostImage($path, 'photos');
                            $type = 'photos';
                            $is_media = true;
                            break;
                        case 'image/jpg':
                            $media_name = $this->upload->importPostImage($path, 'photos');
                            $type = 'photos';
                            $is_media = true;
                            break;
                        case 'image/jpeg':
                            $media_name = $this->upload->importPostImage($path, 'photos');
                            $type = 'photos';
                            $is_media = true;
                            break;
                        case 'image/gif':
                            $media_name = $this->upload->importPostImage($path, 'photos');
                            $type = 'photos';
                            $is_media = true;
                            break;
                        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                            $media_name = $this->upload->importPostFile($path, 'album_file');
                            $type = 'album_file';
                            $is_media = true;
                            break;
                        case 'application/pdf':
                            $media_name = $this->upload->importPostFile($path, 'album_file');
                            $type = 'album_file';
                            $is_media = true;
                            break;
                        case 'application/octet-stream':
                            $media_name = $this->upload->importPostFile($path, 'album_file');
                            $type = 'album_file';
                            $is_media = true;
                            break;
                        
                        default:
                            $is_media = false;
                            break;
                    }


                    if ($is_media) {
                        DB::connection('mysql')->table('medias')->insert([
                            'id' => $media->fid,
                            'title' => $media->filename,
                            'type' => $type,
                            'uri' => $media_name,
                            'status' => $media->status,
                            'created_at' => Carbon::createFromTimestamp($media->timestamp)->toDateTimeString(),
                            'updated_at' => Carbon::createFromTimestamp($media->timestamp)->toDateTimeString(),
                        ]);
                        DB::connection('mysql')->table('mediable')->insert([
                            'media_id' => $media->fid,
                            'mediable_id' => $item->nid,
                            'mediable_type' => 'App\Models\Album',
                        ]);
                    }
                    
                } catch (\Exception $e) {
                    //dd($e);
                    $this->error('Media error'. $media->fid);
                }
            }
            $bar->advance();
        }
        $bar->finish();
        $this->info('albums table');
    }

}
