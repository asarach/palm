<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Cache;
use App\Unigen\CommonModels;
use App\Unigen\Controllers;
use App\Unigen\AdminControllers;
use App\Unigen\Requests;
use App\Unigen\RepositoriesService;
use App\Unigen\Repositories;
use App\Unigen\Models;
use App\Unigen\Migrations;
use App\Unigen\Factories;
use App\Unigen\Seed;
use App\Unigen\Views;
use App\Unigen\Components;
use App\Unigen\AdminComponents;
use App\Unigen\Routes;
use App\Unigen\Tests;
use App\Unigen\Configs;

class UniGen extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'unigen:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generat full app';
    protected $common_models = [];
    protected $models = [];
    protected $snippets = [];
    protected $appName = 'App';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->setModels();
        $this->setSnippets();

        $commonModels = new CommonModels($this->snippets, $this->models, $this->common_models, $this->appName);
        $commonModels->generate();

        $AdminControllers = new AdminControllers($this->snippets, $this->models, $this->common_models, $this->appName); //Card
        $AdminControllers->generate(); //done

        $Requests = new Requests($this->snippets, $this->models, $this->common_models, $this->appName);
        $Requests->generate(); //done 

        $RepositoriesService = new RepositoriesService($this->snippets, $this->models, $this->common_models, $this->appName);
        $RepositoriesService->generate(); //done 

        $Repositories = new Repositories($this->snippets, $this->models, $this->common_models, $this->appName);
        $Repositories->generate(); //done 

        $Models = new Models($this->snippets, $this->models, $this->common_models, $this->appName);
        $Models->generate(); //done 

        $AdminComponents = new AdminComponents($this->snippets, $this->models, $this->common_models, $this->appName);
        $AdminComponents->generate(); //notyet 

        $Migrations = new Migrations($this->snippets, $this->models, $this->common_models, $this->appName);
        $Migrations->generate(); //notyet

        $Factories = new Factories($this->snippets, $this->models, $this->common_models, $this->appName);
        $Factories->generate(); //notyet

        $Seed = new Seed($this->snippets, $this->models, $this->common_models, $this->appName);
        $Seed->generate(); //notyet

        $controllers = new Controllers($this->snippets, $this->models, $this->common_models, $this->appName);
        $controllers->generate(); //done

        $Components = new Components($this->snippets, $this->models, $this->common_models, $this->appName);
        $Components->generate(); //notyet

        $Views = new Views($this->snippets, $this->models, $this->common_models, $this->appName);
        $Views->generate(); //notyet        

        $Routes = new Routes($this->snippets, $this->models, $this->common_models, $this->appName);
        $Routes->generate(); //notyet

        $Tests = new Tests($this->snippets, $this->models, $this->common_models, $this->appName);
        $Tests->generate(); //notyet

        $Configs = new Configs($this->snippets, $this->models, $this->common_models, $this->appName);
        $Configs->generate(); //notyet
    }

    public function setModels()
    {
        $unigen = Cache::get('unigen');

        foreach ($unigen['modules'] as $key => $module) {
            $this->models[$module['name']] = $module;
        }

        $common_models = require storage_path('unigen/src/common_map.php');

        foreach ($common_models as $key => $common_model) {
            if ($unigen['commun'][$key]) {
                $this->models[$key] = $common_model;
            }
        }
    }

    public function setSnippets()
    {
        $this->snippets['controller'] = require storage_path('unigen/src/snippets/controller.php');
        $this->snippets['admin_controller'] = require storage_path('unigen/src/snippets/admin_controller.php');
        $this->snippets['request'] = require storage_path('unigen/src/snippets/request.php');
        $this->snippets['repositories'] = require storage_path('unigen/src/snippets/repositories.php');
        $this->snippets['configs'] = require storage_path('unigen/src/snippets/configs.php');
        $this->snippets['models'] = require storage_path('unigen/src/snippets/models.php');
        $this->snippets['migrations'] = require storage_path('unigen/src/snippets/migrations.php');
        $this->snippets['factories'] = require storage_path('unigen/src/snippets/factories.php');
        $this->snippets['seeders'] = require storage_path('unigen/src/snippets/seeders.php');
        $this->snippets['views'] = require storage_path('unigen/src/views/map.php');
        $this->snippets['components'] = require storage_path('unigen/src/components/components.php');
        $this->snippets['admin_components'] = require storage_path('unigen/src/components/admin_components.php');
        $this->snippets['common_models'] = require storage_path('unigen/src/common/main.php');
        $this->snippets['routes'] = require storage_path('unigen/src/snippets/routes.php');
        $this->snippets['tests'] = require storage_path('unigen/src/snippets/tests.php');
    }
}
