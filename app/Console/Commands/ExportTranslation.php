<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DirectoryIterator;
use DB;
use Stichoza\GoogleTranslate\GoogleTranslate;
use Spatie\Permission\Models\Role;
use Carbon\Carbon;

class ExportTranslation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translator:export';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $groups = ['admin', 'auth', 'front', 'messages', 'pagination', 'passwords', 'text', 'validation', 'email'];
        $translations = DB::table('translator_translations')->whereIn('group', $groups)->get();
        $bar = $this->output->createProgressBar(count($translations));
        $bar->start();

        foreach ($translations as $translation) {
            $current_text = str_replace('-', ' ', $translation->item);
            if ($translation->text != $current_text) {
                $exist = DB::table('translator_translations_archive')
                    ->where('group', $translation->group)
                    ->where('item', $translation->item)
                    ->where('locale', $translation->locale)
                    ->first();
                if (!$exist) {
                    $translations = DB::table('translator_translations_archive')
                    ->insert([
                        'locale' => $translation->locale,
                        'namespace' => $translation->namespace,
                        'group' => $translation->group,
                        'item' => $translation->item,
                        'text' => $translation->text,
                        'unstable' => $translation->unstable,
                        'locked' => $translation->locked,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ]);

                   //$this->info($translation->item . " => Archived");
                }
            }
            $bar->advance();
        }
        $bar->finish();
        
    }
}
