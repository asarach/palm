<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DirectoryIterator;
use DB;
use Stichoza\GoogleTranslate\GoogleTranslate;
use Spatie\Permission\Models\Role;
use Carbon\Carbon;

class ImportTranslation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translator:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        

        $translations = DB::table('translator_translations')->get();
        foreach ($translations as $translation) {
            $current_text = str_replace('-', ' ', $translation->item);
            if ($translation->text == $current_text) {
                $text = DB::table('translator_translations_archive')
                    ->where('group', $translation->group)
                    ->where('item', $translation->item)
                    ->where('locale', $translation->locale)
                    ->first();
                if ($text) {
                    $translations = DB::table('translator_translations')
                        ->where('group', $translation->group)
                        ->where('item', $translation->item)
                        ->where('locale', $translation->locale)
                        ->update(['text' => $text->text]);
                   $this->info($translation->item . " => Imported");
                }else{
                    try {
                        $tr = new GoogleTranslate();
                        $tr->setSource('en');
                        $tr->setTarget('ar');
                        $translated = ucfirst($tr->translate($translation->text)) ;
                      } catch (\Exception $e) {
                        $translated = ucfirst($translation->text)  ;
                      }
                    //   dd($translated);

                      $translations = DB::table('translator_translations')
                        ->where('group', $translation->group)
                        ->where('item', $translation->item)
                        ->where('locale', $translation->locale)
                        ->update(['text' => $translated, 'updated_at'=> Carbon::now()]);

                    $this->info($translation->item . " => Google Translated");
                }
            }
        }
        
    }
}
