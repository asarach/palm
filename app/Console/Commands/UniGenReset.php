<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use DB;
use Cache;

class UniGenReset extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'unigen:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generat full app';
    protected $common_models = [];
    protected $models = [];
    protected $snippets = [];
    protected $appName = 'App';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->setModels();
        $this->setSnippets();

        $this->putCommonModels(); //Done
        $this->putControllers(); //Done
        $this->putAdminControllers(); //Done
        $this->putRequests(); //Done
        $this->putRepositoriesService(); //Done
        $this->putRepositories();
        $this->putModels(); //Done
        $this->putMigrations(); //Done
        $this->putFactories(); //no
        $this->putSeeders(); //no
        $this->putViews(); //no
        $this->putComponents();
        $this->putAdminComponents();
        $this->putRoutes(); //Done
        $this->putTests(); //no
        Storage::disk('unigen')->deleteDirectory('build');
    }

    public function putCommonModels()
    {
        foreach ($this->models as $common_model) {
            if ($common_model['type'] == 'common') {
                # code...
                if ($common_model['admin_controller']) {
                    $filename = 'app/Http/Controllers/Admin/' . $common_model['name_m'] . 'Controller.php';
                    $this->deleteFile($filename);
                }

                if ($common_model['controller']) {
                    $filename = 'app/Http/Controllers/' . $common_model['name_m'] . 'Controller.php';
                    $this->deleteFile($filename);
                }

                if ($common_model['repository']) {
                    $implements_filename = 'app/Repositories/' . $common_model['name_m'] . '/Eloquent' . $common_model['name_m'] . 'Repository.php';
                    $this->deleteFile($implements_filename);

                    $filename = 'app/Repositories/' . $common_model['name_m'] . '/' . $common_model['name_m'] . 'Repository.php';
                    $this->deleteFile($filename);

                    $implements_filename = 'app/Repositories/' . $common_model['name_m'];
                    $this->deleteDirectory($implements_filename);
                }

                if ($common_model['model'] and $common_model['name_m'] != 'User') {
                    $filename = 'app/Models/' . $common_model['name_m'] . '.php';
                    $this->deleteFile($filename);
                    foreach ($common_model['aux_model'] as $aux_model) {
                        $filename = 'app/Models/' . $aux_model['name_m'] . '.php';
                        $this->deleteFile($filename);
                    }
                }

                if ($common_model['request']) {
                    $filename = 'app/Http/Requests/' . $common_model['name_m'] . 'Request.php';
                    $this->deleteFile($filename);
                }

                if ($common_model['migration']) {
                    $filename = 'database/migrations/' . $common_model['migration_name'] . '.php';
                    $this->deleteFile($filename);
                }

                if ($common_model['factory']) {
                    $filename = 'database/factories/' . $common_model['name_m'] . 'Factory.php';
                    $this->deleteFile($filename);
                }

                if ($common_model['seed']) {
                    $filename = 'database/seeds/' . $common_model['name_pm'] . 'TableSeeder.php';
                    $this->deleteFile($filename);
                }

                if ($common_model['admin_components']) {
                    foreach ($this->snippets['common_models'][$common_model['name']]['admin_components'] as $name => $snippet) {
                        $filename = 'resources/js/admin/components/' . $common_model['name'] . '/' . $name . '.vue';
                        $this->deleteFile($filename);
                    }
                    $implements_filename = 'resources/js/admin/components/' . $common_model['name'];
                    $this->deleteDirectory($implements_filename);
                }

                if ($common_model['components']) {
                    foreach ($this->snippets['common_models'][$common_model['name']]['components'] as $name => $snippet) {
                        $filename = 'resources/js/components/' . $common_model['name'] . '/' . $name . '.vue';
                        $this->deleteFile($filename);
                    }
                    $implements_filename = 'resources/js/components/' . $common_model['name'];
                    $this->deleteDirectory($implements_filename);
                }

                if ($common_model['tests']) {
                    $filename = 'tests/Unit/' . $common_model['name_m'] . 'Test.php';
                    $this->deleteFile($filename);
                }
            }
        }
    }
    public function putControllers()
    {
        $models = $this->models;

        foreach ($models as $model) {
            if ($model['type'] == 'simple') {
                $filename = 'app/Http/Controllers/' . $model['name_m'] . 'Controller.php';
                $this->deleteFile($filename);
            }
        }
    }

    public function putAdminControllers()
    {
        foreach ($this->models as $model) {
            if ($model['type'] == 'simple') {
                $filename = 'app/Http/Controllers/Admin/' . $model['name_m'] . 'Controller.php';
                $this->deleteFile($filename);
            }
        }
    }

    public function putRequests()
    {
        foreach ($this->models as $model) {
            if ($model['type'] == 'simple') {
                $filename = 'app/Http/Requests/' . $model['name_m'] . 'Request.php';
                $this->deleteFile($filename);
            }
        }
    }

    public function putRepositoriesService()
    {
        $singletons = '';

        $content = $this->replace($this->snippets['repositories']['service'], [$this->appName, $singletons]);

        $filename = 'build/app/Providers/RepositoriesServiceProvider.php';
        Storage::disk('unigen')->put($filename, $content);
        $this->info($filename . ' Regenerated');
    }


    public function putRepositories()
    {
        foreach ($this->models as $model) {
            if ($model['type'] == 'simple') {
                $filename = 'app/Repositories/' . $model['name_m'] . '/' . $model['name_m'] . 'Repository.php';
                $this->deleteFile($filename);


                $filename_eloquent = 'app/Repositories/' . $model['name_m'] . '/Eloquent' . $model['name_m'] . 'Repository.php';
                $this->deleteFile($filename_eloquent);


                $implements_filename = 'app/Repositories/' . $model['name_m'];
                $this->deleteDirectory($implements_filename);
            }
        }
    }

    public function putModels()
    {
        foreach ($this->models as $model) {
            if ($model['type'] == 'simple') {
                $filename = 'app/Models/' . $model['name_m'] . '.php';
                $this->deleteFile($filename);
            }
        }
    }

    public function putMigrations()
    {
        $order = 0;
        foreach ($this->models as $model) {
            if ($model['type'] == 'simple') {
                $filename = 'database/migrations/2020_03_01_1600' . $order . '_create_' . $model['name_p'] . '_table.php';
                $this->deleteFile($filename);

                $order++;
            }
        }
    }

    public function putFactories()
    {
        foreach ($this->models as $model) {
            if ($model['type'] == 'simple') {
                $filename = 'database/factories/' . $model['name_m'] . 'Factory.php';
                $this->deleteFile($filename);
            }
        }
    }

    public function putSeeders()
    {
        foreach ($this->models as $model) {
            $filename = 'database/seeders/' . $model['name_m'] . 'Seeder.php';
            $this->deleteFile($filename);
        }
    }

    public function putViews()
    {
        foreach ($this->models as $model) {
            if ($model['type'] == 'simple') {
                $filename = 'resources/views/' . $model['name'] ;
                $this->deleteDirectory($filename);
            }
        }
    }

    public function putComponents()
    {
        foreach ($this->models as $model) {
            if ($model['type'] == 'simple') {
                $filename = 'resources/js/components/' . $model['name'] . '/Index.vue';
                $this->deleteFile($filename);
                $filename = 'resources/js/components/' . $model['name'] . '/Edit.vue';
                $this->deleteFile($filename);
                $filename = 'resources/js/components/' . $model['name'] . '/Form.vue';
                $this->deleteFile($filename);
                $filename = 'resources/js/components/' . $model['name'] . '/Create.vue';
                $this->deleteFile($filename);

                $implements_filename = 'resources/js/components/' . $model['name'];
                $this->deleteDirectory($implements_filename);
            }
        }
    }

    public function putAdminComponents()
    {
        $filename = 'resources/js/admin/admin.js';
        $this->deleteFile($filename);


        $filename = 'resources/js/admin/routes.js';
        $this->deleteFile($filename);

        foreach ($this->models as $model) {
            if ($model['type'] == 'simple') {
                $filename = 'resources/js/admin/components/' . $model['name'] . '/Index.vue';
                $this->deleteFile($filename);
                $filename = 'resources/js/admin/components/' . $model['name'] . '/Edit.vue';
                $this->deleteFile($filename);
                $filename = 'resources/js/admin/components/' . $model['name'] . '/Form.vue';
                $this->deleteFile($filename);
                $filename = 'resources/js/admin/components/' . $model['name'] . '/Create.vue';
                $this->deleteFile($filename);

                $implements_filename = 'resources/js/admin/components/' . $model['name'];
                $this->deleteDirectory($implements_filename);
            }
        }
    }

    public function putRoutes()
    {
        $routes_admin = "\r\n";
        $routes_web = "\r\n";

        $content_admin = $this->replace($this->snippets['routes']['admin'], [$routes_admin]);
        $content_web = $this->replace($this->snippets['routes']['main'], [$routes_web]);

        $filename_admin = 'build/routes/admin.php';
        $filename_web = 'build/routes/web.php';

        Storage::disk('unigen')->put($filename_web, $content_web);
        $this->info($filename_web . ' Generated');

        Storage::disk('unigen')->put($filename_admin, $content_admin);
        $this->info($filename_admin . ' Generated');
    }

    public function putTests()
    {
        foreach ($this->models as $model) {
            $filename = 'tests/Feature/Admin' . $model['name_m'] . 'Test.php';
            $this->deleteFile($filename);

            $filename = 'tests/Feature/' . $model['name_m'] . 'Test.php';
            $this->deleteFile($filename);
        }
    }



    public function generate_admin_component_index($model, $component)
    {
        $filename = 'resources/js/admin/components/' . $model['name'] . '/' . $component['name_m'] . '.vue';
        $this->deleteFile($filename);
    }

    public function generate_component_form($model, $component)
    {
        $filename = 'resources/js/admin/components/' . $model['name'] . '/' . $component['name_m'] . '.vue';
        $this->deleteFile($filename);
    }

    public function generate_component_create($model, $component)
    {
        $filename = 'resources/js/admin/components/' . $model['name'] . '/' . $component['name_m'] . '.vue';
        $this->deleteFile($filename);
    }

    public function generate_component_edit($model, $component)
    {
        $filename = 'resources/js/admin/components/' . $model['name'] . '/' . $component['name_m'] . '.vue';
        $this->deleteFile($filename);
    }

    public function replace($snippet, $vars = [])
    {
        foreach ($vars as $key => $value) {
            if ($key < 10) {
                $key = '0' . $key;
            }
            $snippet = str_replace('%var' . $key, $value, $snippet);
        }
        return $snippet;
    }

    public function setModels()
    {
        $unigen = Cache::get('unigen');

        foreach ($unigen['modules'] as $key => $module) {
            $this->models[$module['name']] = $module;
        }

        $common_models = require storage_path('unigen/src/common_map.php');

        foreach ($common_models as $key => $common_model) {
            if ($unigen['commun'][$key]) {
                $this->models[$key] = $common_model;
            }
        }
    }

    public function setSnippets()
    {
        $this->snippets['controller'] = require storage_path('unigen/src/snippets/controller.php');
        $this->snippets['admin_controller'] = require storage_path('unigen/src/snippets/admin_controller.php');
        $this->snippets['request'] = require storage_path('unigen/src/snippets/request.php');
        $this->snippets['repositories'] = require storage_path('unigen/src/snippets/repositories.php');
        $this->snippets['models'] = require storage_path('unigen/src/snippets/models.php');
        $this->snippets['migrations'] = require storage_path('unigen/src/snippets/migrations.php');
        $this->snippets['factories'] = require storage_path('unigen/src/snippets/factories.php');
        $this->snippets['views'] = require storage_path('unigen/src/views/map.php');
        //$this->snippets['components'] = require storage_path('unigen/src/components/components.php');
        $this->snippets['admin_components'] = require storage_path('unigen/src/components/admin_components.php');
        $this->snippets['common_models'] = require storage_path('unigen/src/common/main.php');
        $this->snippets['routes'] = require storage_path('unigen/src/snippets/routes.php');
        $this->snippets['tests'] = require storage_path('unigen/src/snippets/tests.php');
    }

    public function deleteFile($filename)
    {
        try {
            Storage::disk('app')->delete($filename);
            $this->info($filename . '  Deleted');
        } catch (\Throwable $th) {
            $this->error($filename . ' Not Deleted');
        }
    }
    public function deleteDirectory($filename)
    {
        try {
            Storage::disk('app')->deleteDirectory($filename);
            $this->info($filename . '  Deleted');
        } catch (\Throwable $th) {
            $this->error($filename . ' Not Deleted');
        }
    }
}
