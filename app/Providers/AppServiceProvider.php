<?php

namespace App\Providers;

use Carbon\Carbon;
use LaravelLocalization;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        try {
            $lang = LaravelLocalization::getCurrentLocale();
            $regional = DB::table('translator_languages')->where('locale', $lang)->first()->regional;

        } catch (\Exception $e) {
              $regional = 'ar_AE';
        }
        setlocale(LC_ALL, $regional . '.utf8'); 
        Paginator::useBootstrap();


        $this->app->bind('path.public', function() {
            return base_path().'/local';
          });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
