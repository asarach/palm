<?php

use App\Models\Category;
use App\Source;
use Carbon\Carbon;
use App\Models\Setting;
use Jenssegers\Agent\Agent;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Waavi\Translation\Models\Language;
use Illuminate\Support\Facades\Storage;
use Waavi\Translation\Models\Translation;
use Waavi\Translation\Facades\TranslationCache;


function getAssetTimestamp()
{
    $setting = env('APP_ASSET_TIMESTAMP', false);
    $current_time = Carbon::now()->timestamp;

    if ($setting) {
        $html = '?' . $current_time;
    } else {
        $html = '';
    }
    return $html;
}


function createNavigationArray($annonces)
{
    $old = session('annonces_navigation');
    $page = request()->page;
    if (!is_null($old) and !is_null($page) and intval($page) > 1) {
        $result = $annonces->pluck('slug')->toArray();
        if (isset($result[0]) and !in_array($result[0], $old)) {
            $slugs = array_merge($old, $result);
        } else {
            $slugs = $old;
        }
    } else {
        $slugs = $annonces->pluck('slug')->toArray();
    }
    session(['annonces_navigation' => $slugs]);
    //dd($slugs);
}

function seo_nofollow($text)
{
    $text = preg_replace_callback('~<(a\s[^>]+)>~isU', "asa_cb2", $text);
    return e($text);
}
function getSubCatsIds($id)
{
    return Cache::rememberForever('sub_cats_ids_' . $id, function () use ($id) {
        return Category::find($id)->kidsIds();
    });
}

function asa_cb2($match)
{
    list($original, $tag) = $match;   // regex match groups

    $my_folder =  "/hostgator";       // re-add quirky config here
    $blog_url = config('app.url');

    if (strpos($tag, "nofollow")) {
        return $original;
    } elseif (strpos($tag, $blog_url) && (!$my_folder || !strpos($tag, $my_folder))) {
        return $original;
    } else {
        return "<$tag rel='nofollow'>";
    }
}

function flushTrans()
{
    Waavi\Translation\Facades\TranslationCache::flushAll();

    $languages = Waavi\Translation\Models\Language::all();

    foreach ($languages as $language) {
        $admin_trans = [];
        foreach (Waavi\Translation\Models\Translation::where('locale', $language->locale)->where('group', 'admin')->get() as $translation) {
            $admin_trans[$translation->item] = $translation->text;
        }
        $admin = json_encode($admin_trans);

        $front_trans = [];
        foreach (Waavi\Translation\Models\Translation::where('locale', $language->locale)->where('group', 'front')->get() as $translation) {
            $front_trans[$translation->item] = $translation->text;
        }
        $front = json_encode($front_trans);

        $text_trans = [];
        foreach (Waavi\Translation\Models\Translation::where('locale', $language->locale)->where('group', 'text')->get() as $translation) {
            $text_trans[$translation->item] = $translation->text;
        }
        $text = json_encode($text_trans);

        Storage::disk('public_html')->put('js/trans/admin_' . $language->locale . '.js', 'window.trans = {"admin" : ' . $admin . ',"front" : ' . $front . ',  "text" : ' . $text . '}');
        Storage::disk('public_html')->put('js/trans/text_' . $language->locale . '.js', 'window.trans = {"text" : ' . $text . '}');
    }
}

function setActive(string $path, string $class_name = "is-active")
{
    if (request()->routeIs($path)) {
        return $class_name;
    } else {
        return '';
    }
}
function isActiveClass($a, $b, $c)
{
    if ($a == $b) {
        return $c;
    }
}

function clearArticleCache(array $cahces)
{
    foreach ($cahces as $cache) {
        Cache::forget($cache);
    }
}

function getRecommendations($is_api = 0)
{
    $recommendations  = App\Models\Article::where('status', 1)->take(6)->get();
    if ($is_api) {
        return compact('recommendations');
    } else {
        return  view('article.recommendations', compact('recommendations'))->render();
    }
}

function getThumbnail($images, $type, $size = 'md')
{
    if ($size) {
        $base = 'uploads/' . $type . '/' . $size . '/';
    } else {
        $base = 'uploads/' . $type . '/';
    }

    if (isset($images[0]) and $images[0]->uri and filter_var($images[0]->uri, FILTER_VALIDATE_URL)) {
        $image = $images[0]->uri;               // 5 days ago
    } elseif (isset($images[0]) and $images[0]->uri) {
        $image = asset($base . $images[0]->uri);
    } else {
        $image = asset($base . 'default.jpg');
    }

    return $image;
}

function formatDateB($date)
{
    $dt = Carbon::parse($date);
    if ($dt) {
        $date = $dt->formatLocalized('%d %B %Y - %H:%M');               // 5 days ago
    } else {
        $date = '';
    }
    return $date;
}

function get_view($view, $params)
{
    if (request()->is('api/*') or request()->is('api')) {
        return $params;
    } else {
        return view($view, $params);
    }
}
function getPageViews()
{
    return config('drtopic.page_views');
}
function getLinkableTypes()
{
    $array = [
        'App\Models\Category'  => trans('text.category'),
        'App\Models\Page'      => trans('text.page'),
        'Url'              => trans('text.url'),
        'Divider'          => trans('text.divider'),
    ];
    return $array;
}


function is_mobile()
{
    $agent = new Agent();

    if ($agent->isMobile() or $agent->isPhone() or $agent->device() == 'iPhone') {
        return true;
    } else {
        return false;
    }
}

function getActiveIcon($status)
{
    if ($status == 1) {
        return '<i class="fa fa-chevron-down text-success"></i>';
    } else {
        return '<i class="fa fa-ban text-danger"></i>';
    }
}
function RateStars($rate)
{
    for ($i = 0; $i < 5; $i++) {
        if ($rate > $i) {
            echo '<span class="fa fa-star checked"></span>';
        } else {
            echo '<span class="fa fa-star"></span>';
        }
    }
}

function getActiveBtn($status, $id)
{
    if ($status == 1) {
        $icon = ' fa-chevron-down ';
        $text_class = ' text-success ';
        $btn_class = ' btn_deactivate ';
    } else {
        $icon = ' fa-ban ';
        $text_class = ' text-danger ';
        $btn_class = ' btn_activate ';
    }
    $html = '<a href="#" class="' . $btn_class . '" data-id="' . $id . '"><i class="fa' . $icon . $text_class . '"></i></a>';

    return $html;
}
function getReviewBtn($status, $id)
{
    if ($status == 1) {
        $icon = ' fa-chevron-down ';
        $text_class = ' text-success ';
        $btn_class = ' btn_dereview ';
    } else {
        $icon = ' fa-ban ';
        $text_class = ' text-danger ';
        $btn_class = ' btn_review ';
    }
    $html = '<a href="#" class="' . $btn_class . '" data-id="' . $id . '"><i class="fa' . $icon . $text_class . '"></i></a>';

    return $html;
}

function getStickyBtn($status, $id)
{
    if ($status == 1) {
        $icon = ' fa-chevron-down ';
        $text_class = ' text-success ';
        $btn_class = ' btn_desticky ';
    } else {
        $icon = ' fa-ban ';
        $text_class = ' text-danger ';
        $btn_class = ' btn_sticky ';
    }
    $html = '<a href="#" class="' . $btn_class . '" data-id="' . $id . '"><i class="fa' . $icon . $text_class . '"></i></a>';

    return $html;
}
function getPromoteBtn($status, $id)
{
    if ($status == 1) {
        $icon = ' fa-chevron-down ';
        $text_class = ' text-success ';
        $btn_class = ' btn_depromote ';
    } else {
        $icon = ' fa-ban ';
        $text_class = ' text-danger ';
        $btn_class = ' btn_promote ';
    }
    $html = '<a href="#" class="' . $btn_class . '" data-id="' . $id . '"><i class="fa' . $icon . $text_class . '"></i></a>';

    return $html;
}

function getUserRoles()
{
    return config('drtopic.user_roles');
}
function getUrlBase()
{
    $path = parse_url(Config::get('app.url'));
    if (array_key_exists('path', $path)) {
        return  $path['path'] . '/';
    } else {
        return  '/';
    }
}
function settings($key, $default = null)
{
    $result = Cache::rememberForever('settings', function () {
        $settings = Setting::all();
        $result = [];
        foreach ($settings as $setting) {
            $result[$setting->key] = $setting->value;
        }
        return $result;
    });
    if (array_key_exists($key, $result)) {
        return $result[$key];
    } elseif (!is_null($default)) {
        return $default;
    } else {
        return '';
    }
}
function UseVue()
{
    return true;
}
function make_slug($string, $separator = '-')
{
    $text = html_entity_decode($string, ENT_QUOTES, 'UTF-8');
    $text = preg_replace('~[^\pL\d]+~u', $separator, $text);
    $text = trim($text, $separator);
    return $text;
}
function formatBytes($bytes, $precision = 2)
{
    $units = array('B', 'KB', 'MB', 'GB', 'TB');

    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);
    $bytes /= (1 << (10 * $pow));

    return round($bytes, $precision) . ' ' . $units[$pow];
}
function testDumpError($response)
{
    try {
        $res = $response->decodeResponseJson();
        $error = [
            'message' => $res['message'],
            'exception' => $res['exception'],
            'file' => $res['file'],
            'line' => $res['line'],
        ];

        dd($error);
    } catch (\Throwable $th) {
        //throw $th;
    }
}
