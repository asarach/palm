<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PollAnswer extends Model
{
    use HasFactory;

    protected $fillable = ["ip", "option_id", "poll_id"];

    /**
     * Get the categories of the  article.
     */
    public function  option()
    {
        return $this->belongsTo("App\Models\PollOption");
    }
    /**
     * Get the categories of the  article.
     */
    public function  poll()
    {
        return $this->belongsTo("App\Models\Poll");
    }
}
