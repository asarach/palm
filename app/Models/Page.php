<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Heroicpixels\Filterable\FilterableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Page extends Model
{
    use FilterableTrait, SoftDeletes, Sluggable, HasFactory;

    protected $dates = ["deleted_at"];

    public function sluggable(): array
    {
        return [
            "slug" => [
                "source" => "name"
            ]
        ];
    }

    public function toArray()
    {
        $array = parent::ToArray();
        $array["created_at"] = $this->getDate();
        return $array;
    }

    public function getDate()
    {
        if ($this->created_at) {
            return $this->created_at->format("d-m-y");
        } else {
            return "";
        }
    }

    /**
     * Get the language of the  article.
     */
    public function  language()
    {
        return $this->belongsTo("Waavi\Translation\Models\Language");
    }
}
