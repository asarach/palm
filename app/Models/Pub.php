<?php

namespace App\Models;

use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;
use Heroicpixels\Filterable\FilterableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pub extends Model
{
    use FilterableTrait, SoftDeletes, Sortable, HasFactory;

    protected $fillable = ["name", "order_num", "url", "start_date", "end_date", "status"];

    public $sortable = ["id", "name", "order_num"];

    protected $dates = ["deleted_at"];

    public  $timestamps  = false;

    /**
     * The users that maked to the mushaf.
     */
    public function image()
    {
        return $this->morphToMany("App\Models\Media", "mediable", "mediable")->where("type", "pub_image");
    }
}
