<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PollOption extends Model
{
    use HasFactory;

    /**
     * Get the articles of the  category.
     */
    public function answers()
    {
        return $this->hasMany("App\Models\PollAnswer");
    }
    /**
     * Get the categories of the  article.
     */
    public function  poll()
    {
        return $this->belongsTo("App\Models\Poll");
    }
}
