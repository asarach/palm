<?php

namespace App\Models;

use Laravel\Scout\Searchable;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Heroicpixels\Filterable\FilterableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use CyrildeWit\EloquentViewable\InteractsWithViews;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use CyrildeWit\EloquentViewable\Contracts\Viewable as ViewableContract;


class Poll extends Model implements ViewableContract
{
    use FilterableTrait, Searchable, InteractsWithViews, SoftDeletes, Sortable, Sluggable, HasFactory;

    protected $fillable = ["title", "slug", "content", "excerpt", "status", "sticky", "end_date"];

    protected $removeViewsOnDelete = true;

    protected $dates = ["deleted_at"];

    public $sortable = ["id", "title", "created_at",];

    public function searchableAs()
    {
        return "polls_index";
    }

    public function toSearchableArray()
    {
        return [
            "id" => $this->id,
            "title" => $this->title,
            "excerpt" => $this->excerpt,
        ];
    }

    public function sluggable(): array
    {
        return [
            "slug" => [
                "source" => "title"
            ]
        ];
    }

    public function toArray()
    {
        $array = parent::ToArray();
        return $array;
    }

    /**
     * Get the language of the  poll.
     */
    public function  language()
    {
        return $this->belongsTo("Waavi\Translation\Models\Language");
    }
    /**
     * Get the author of the  poll.
     */
    public function  user()
    {
        return $this->belongsTo("App\Models\User");
    }

    /**
     * Get the articles of the  category.
     */
    public function options()
    {
        return $this->hasMany("App\Models\PollOption");
    }
    /**
     * Get the articles of the  category.
     */
    public function answers()
    {
        return $this->hasMany("App\Models\PollAnswer");
    }
}
