<?php

namespace App\Models;

use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;
use Heroicpixels\Filterable\FilterableTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Message extends Model
{
    use FilterableTrait, Sortable, HasFactory;

    public $sortable = ["id", "created_at"];

    public function toArray()
    {
        $array = parent::ToArray();

        return $array;
    }
}
