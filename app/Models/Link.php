<?php

namespace App\Models;

use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;
use Heroicpixels\Filterable\FilterableTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Link extends Model
{
  use FilterableTrait, Sortable, HasFactory;


  /**
   * Get the language of the annonce.
   */
  public function menu()
  {
    return $this->belongsTo("App\Models\Menu");
  }

  /**
   * Get the user of the comment.
   */
  public function linkable()
  {
    return $this->morphTo();
  }

  /**
   * Get the language of the annonce.
   */
  public function parente()
  {
    return $this->belongsTo("App\Models\Link", "parent_id");
  }

  /**
   * Get the kids for the annonce.
   */
  public function kids()
  {
    return $this->hasMany("App\Models\Link", "parent_id");
  }
  public function getLink()
  {
    switch ($this->linkable_type) {
      case "App\Models\Category":
        $cat = $this->linkable;
        return route("category.show", $cat->slug);
        break;

      case "App\Models\Page":
        $page = $this->linkable;
        return route("page.show", $page->name);
        break;

      case "Url":
        return $this->url;
        break;

      default:
        return  $this->linkable->getLink();
        break;
    }
  }
  public function getIcon($size)
  {
    if ($this->linkable_type == "App\Models\Category" && $this->linkable->image) {
      return '<img src="' . asset('uploads/cat_thumb/' . $size . '/' . $this->linkable->image->file_name) . '" alt="' . $this->linkable->image->alt . '">';
    } else {
      return "";
    }
  }
  public function renderSubLink($kid)
  {
    $link = '<div class="sub-menu">';
    foreach ($kid->kids as $kid) {
      $link .= '<a class="menu-dropdown-item" href="' . $kid->getLink() . '">';
      $link .= trans("menu.link-" . $kid->id);
      $link .= '</a>';
      if ($kid->kids()->exists()) {
        $link .= $kid->renderSubLink($kid);
      }
    }
    $link .= '</div>';
    return $link;
  }
  public function renderLink($level)
  {
    $link = "";
    if ($this->kids()->exists()) {
      $link .= '<li class="nav-item  list-inline-item  menu-dropdown">';
      $link .= '<a class="nav-link menu-dropdown-toggle" href="#" id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false">';
      $link .= '<span data-hover="' . trans('menu.link-' . $this->id) . '">' . trans('menu.link-' . $this->id) . '</span>';
      $link .= '</a>';
      $link .= '<div class="menu-dropdown-menu" aria-labelledby="navbarDropdown">';
      foreach ($this->kids as $kid) {
        $link .= '<a class="menu-dropdown-item" href="' . $kid->getLink() . '">';
        $link .= trans('menu.link-' . $kid->id);
        $link .= '</a>';
        if ($kid->kids()->exists()) {
          $link .= $kid->renderSubLink($kid);
        }
      }
      $link .= '</div>';
      $link .= '</li>';
    } else {
      $link .= '<li class="nav-item list-inline-item  menu-dropdown ' . setActive($this->getLink(), 'active') . '" >';
      $link .= '<a class="nav-link menu-dropdown-toggle" href="' . $this->getLink() . '">';
      $link .= '<span data-hover="' . trans('menu.link-' . $this->id) . '">' . trans('menu.link-' . $this->id) . '</span>';
      $link .= '</a>';
      $link .= '</li>';
    }


    return $link;
  }

  public function renderLinkMobile($level)
  {
    $link = "";
    if ($this->kids()->exists()) {
      $link .= '<div class="side-card">';
      $link .= '<div class="card-header" id="heading-' . $this->id . '">';
      $link .= '<button class="btn btn-link" data-toggle="collapse" data-target="#collapse-' . $this->id . '" aria-expanded="true" aria-controls="collapse-' . $this->id . '">';
      $link .=  trans('menu.link-' . $this->id);
      $link .= '</button>';
      $link .= '</div>';
      $link .= '<div id="collapse-' . $this->id . '" class="collapse" aria-labelledby="heading-' . $this->id . '" data-parent="#accordion">';
      $link .= '<div class="card-body">';
      foreach ($this->kids as $kid) {
        $link .= '<a class="menu-dropdown-item" href="' . $kid->getLink() . '">';
        $link .= trans('menu.link-' . $kid->id);
        $link .= '</a>';
        if ($kid->kids()->exists()) {
          $link .= $kid->renderSubLink($kid);
        }
      }
      $link .= '</div>';
      $link .= '</div>';
      $link .= '</div>';
    } else {
      $link .= '<li class="nav-item list-inline-item  menu-dropdown ' . setActive($this->getLink(), 'active') . '" >';
      $link .= '<a class="nav-link menu-dropdown-toggle" href="' . $this->getLink() . '">';
      $link .= '<span data-hover="' . trans('menu.link-' . $this->id) . '">' . trans('menu.link-' . $this->id) . '</span>';
      $link .= '</a>';
      $link .= '</li>';
    }

    return $link;
  }
}
