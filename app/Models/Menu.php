<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Menu extends Model
{
  use HasFactory;
  /**
   * Get all annonces of this category.
   */
  public function links()
  {
    return $this->hasMany("App\Models\Link");
  }
}
