<?php

namespace App\Models;

use Laravel\Scout\Searchable;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;
use Heroicpixels\Filterable\FilterableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Comment extends Model
{
    use FilterableTrait, Searchable, Sortable, SoftDeletes, HasFactory;

    protected $fillable = ["comment", "status"];

    public $sortable = ["id", "created_at", "signal"];

    protected $dates = ["deleted_at"];

    public function searchableAs()
    {
        return "comments_index";
    }

    public function toSearchableArray()
    {
        return [
            "id" => $this->id,
            "comment" => $this->v
        ];
    }

    public function toArray()
    {
        if (strpos(url()->current(), "admin") !== false) {
            $author = $this->getAuthor();
            $post = $this->getPost();
            $array = parent::ToArray();
            $array["author"] = $author["name"];
            $array["post_title"] = $post["title"];
            $array["post_link"] = $post["link"];
            $array["date"] = $this->getLastUpdate();
        } else {
            $author = $this->getAuthor();
            $array = [
                "id" => $this->id,
                "user_id" => $this->user_id,
                "parent_id" => $this->parent_id,
                "like" => $this->like,
                "dislike" => $this->dislike,
                "count" => $author["count"],
                "name" => $author["name"],
                "avatar" => $author["avatar"],
                "last_update" => $this->getLastUpdate(),
                "content" => $this->getContent(),
                "replies" => $this->getReplys(),
            ];
        }

        return $array;
    }


    /**
     * Get the commentable of the comment.
     */
    public function commentable()
    {
        return $this->morphTo();
    }

    /**
     * Get the parente of the comment.
     */
    public function parente()
    {
        return $this->belongsTo("App\Models\Comment", "parent_id");
    }

    /**
     * Get the kids for the comment.
     */
    public function kids()
    {
        return $this->hasMany("App\Models\Comment", "parent_id");
    }

    /**
     * Get the user of the comment.
     */
    public function user()
    {
        return $this->belongsTo("App\Models\User");
    }

    public function getAuthor()
    {
        $author = [];
        if ($this->user) {
            $author["count"] = $this->getCount();
            $author["name"] = $this->user->name;
            $author["avatar"] = $this->user->getAvatar();
        } else {
            $author["count"] = $this->getCount();
            $author["name"] = $this->name;
            $author["avatar"] = asset("img/user-default.png");
        }
        return $author;
    }

    public function getPost()
    {
        $post = [];
        if ($this->commentable) {
            $post["title"] = $this->commentable->title;
            $post["link"] = $this->commentable->getLink();
        } else {
            $post["title"] = "";
            $post["link"] = "";
        }
        return $post;
    }

    public function getCount()
    {
        if ($this->user) {
            $count = $this->where("email", $this->user->email)->count();
        } else {
            $count = $this->where("email", $this->email)->count();
        }
        return $count;
    }

    public function getContent()
    {
        return html_entity_decode($this->comment);
    }

    public function getLastUpdate()
    {
        return $this->created_at->diffForHumans();
    }

    public function getReplys()
    {
        $result = [];
        $replies = $this->where("status", 1)->where("parent_id", $this->id)->get();
        foreach ($replies as $reply) {
            $author = $reply->getAuthor();
            $array = [
                "id" => $reply->id,
                "user_id" => $reply->user_id,
                "parent_id" => $reply->parent_id,
                "like" => $reply->like,
                "dislike" => $reply->dislike,
                "count" => $author["count"],
                "name" => $author["name"],
                "avatar" => $author["avatar"],
                "last_update" => $reply->getLastUpdate(),
                "content" => $reply->getContent(),
                "replies" => $reply->getReplys(),
            ];
            $result[] = $array;
        }
        return $result;
    }
}
