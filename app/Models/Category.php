<?php

namespace App\Models;

use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use Sortable, Sluggable, HasFactory;

    protected $fillable = ["name", "slug", "order", "status", "description", "keywords"];

    public $timestamps = false;

    public $sortable = ["order", "name"];

    public function sluggable(): array
    {
        return [
            "slug" => [
                "source" => "name"
            ]
        ];
    }

    public function toArray()
    {
        $array = parent::ToArray();
        return $array;
    }

    /**
     * Get category thum.
     */
    public function thumb()
    {
        return $this->morphToMany("App\Models\Media", "mediable", "mediable")->where("type", "cat_thumb");
    }

    /**
     * Get the cards of the  category.
     */
    public function cards()
    {
        return $this->hasMany("App\Card");
    }

    /**
     * Get the parente of the  category.
     */
    public function parente()
    {
        return $this->belongsTo("App\Models\Category", "parent_id");
    }

    /**
     * Get the kids of the  category.
     */
    public function kids()
    {
        return $this->hasMany("App\Models\Category", "parent_id");
    }

    public function kidsIds()
    {
        $ids = [];
        $ids[] = $this->id;
        foreach ($this->kids as $kid) {
            $ids =  array_merge($ids, $kid->kidsIds());
        }

        return $ids;
    }

    public function getBreadcrumbs($type)
    {
        $breadcrumbs = [];
        $cat = $this;
        $breadcrumbs[] = ["url" => "", "name" => $cat->name];
        while ($cat) {
            if ($cat->parente) {
                $cat = $cat->parente;
                if ($type == "admin") {
                    $url = "category/" . $cat->id;
                } else {
                    $url = $cat->slug;
                }

                $breadcrumbs[] = ["url" => $url, "name" => $cat->name];
            } else {
                $cat = false;
            }
        }

        return array_reverse($breadcrumbs);
    }
}
