<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Media extends Model
{
    use HasFactory;

    protected $table = "medias";

    public function toArray()
    {
        $array = parent::ToArray();
        $array["src"] = $this->getImage();
        return $array;
    }

    /**
     * Get all of the articles that are assigned this media.
     */
    public function articles()
    {
        return $this->morphedByMany("App\Models\Article", "mediable", "mediable");
    }

    /**
     * Get all of the categories that are assigned this media.
     */
    public function categories()
    {
        return $this->morphedByMany("App\Models\Category", "mediable", "mediable");
    }

    /**
     * Get all of the pubs that are assigned this media.
     */
    public function pubs()
    {
        return $this->morphedByMany("App\Models\Pub", "mediable", "mediable");
    }

    /**
     * The users that maked to the image.
     */
    public function getDocUrl()
    {
        return asset("uploads/" . $this->type  . "/" . $this->uri);
    }
    public function getDocName()
    {
        $arr = explode("/", $this->uri);
        if (isset($arr[2])) {
            return $arr[2];
        } else {
            return $this->uri;
        }
    }
    public function getImage($size = "md")
    {
        if ($size) {
            $base = "uploads/" . $this->type . "/" . $size . "/";
        } else {
            $base = "uploads/" . $this->type  . "/";
        }
        if ($this->uri and filter_var($this->uri, FILTER_VALIDATE_URL)) {
            $image = $this->uri;
        } elseif ($this->uri) {
            $image = asset($base . $this->uri);
        } else {
            $image = asset($base . "default.jpg");
        }
        return $image;
    }
}
