<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Heroicpixels\Filterable\FilterableTrait;
use Laravel\Scout\Searchable;
use CyrildeWit\EloquentViewable\Contracts\Viewable as ViewableContract;
use CyrildeWit\EloquentViewable\InteractsWithViews;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Post extends Model implements ViewableContract
{
    use FilterableTrait, Searchable, InteractsWithViews, SoftDeletes, Sortable, Sluggable, HasFactory;

    protected $fillable = ["title", "slug", "body", "excerpt", "keywords", "status", "comment_status", "client", "date", "skills", "url"];

    protected $removeViewsOnDelete = true;

    protected $dates = ["deleted_at"];

    public $sortable = ["id", "title", "created_at"];

    public function searchableAs()
    {
        return "posts_index";
    }

    public function toSearchableArray()
    {
        return [
            "id" => $this->id,
            "title" => $this->title,
            "excerpt" => $this->excerpt,
        ];
    }

    public function sluggable(): array
    {
        return [
            "slug" => [
                "source" => "title"
            ]
        ];
    }
    /**
     * Get the images  of the  post.
     */
    public function images()
    {
        return $this->morphToMany("App\Models\Media", "mediable", "mediable")->where("type", "post_images");
    }

    /**
     * Get the category of the  post.
     */
    public function category()
    {
        return $this->belongsTo("App\Models\Category");
    }

    /**
     * Get the author of the  post.
     */
    public function author()
    {
        return $this->belongsTo("App\Models\User");
    }

    public function getLink()
    {
        $project_ids = getSubCatsIds(config('asarach.project_cat'));

        if (in_array($this->category_id, $project_ids)) {
            return route("portfolio.show", $this->slug);
        } else {
            return route("blog.show", $this->slug);
        }
    }

    public function authorName()
    {
        $name = '';
        if ($this->author) {
            $name = $this->author->name;
        }

        return $name;
    }

    public function getCategoryName()
    {
        $name = '';
        if ($this->category) {
            $name = $this->category->name;
        }

        return $name;
    }
    public function getSkills()
    {
        return explode(',', $this->skills);
    }
}
