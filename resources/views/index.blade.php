@extends('layouts.app')
@section('meta')
<title>{{trans('text.home-page-title')}} - {{ settings('app_name', 'News') }}</title>
<meta name="keywords" content="{{ settings('main_keywords', 'News') }}">
<meta name="description" content="{{ settings('main_description', 'News') }}">
@endsection
@section('content')
<div class="owl-carousel-wrapper" style="height: 100vh;">
    <div class="owl-carousel dots-inside dots-horizontal-center show-dots-hover show-dots-xs dots-light nav-inside nav-inside-plus nav-dark nav-md nav-font-size-md show-nav-hover mb-0"
        data-plugin-options="{'responsive': {'0': {'items': 1, 'dots': true, 'nav': false}, '479': {'items': 1}, '768': {'items': 1}, '979': {'items': 1}, '1199': {'items': 1}}, 'loop': false, 'autoHeight': false, 'margin': 0, 'dots': false, 'dotsVerticalOffset': '-35px', 'nav': true, 'animateIn': 'fadeIn', 'animateOut': 'fadeOut', 'mouseDrag': false, 'touchDrag': false, 'pullDrag': false, 'autoplay': true, 'autoplayTimeout': 9000, 'autoplayHoverPause': true, 'rewind': true}">

        <!-- Carousel Slide 1 -->
        <div class="position-relative overlay overlay-show overlay-op-9 pt-5"
            style="background-image: url({{ asset('img/slide-1.jpg') }}); background-size: cover; background-position: center; height: 100vh;">
            <div class="container position-relative z-index-3 h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-lg-7 text-center">
                        <div class="d-flex flex-column align-items-center justify-content-center h-100">
                            <h3 class="position-relative text-color-light text-5 line-height-5 font-weight-medium ls-0 px-4 mb-1 appear-animation"
                                data-appear-animation="fadeInDownShorterPlus"
                                data-plugin-options="{'minWindowWidth': 0}">
                                <span class="position-absolute right-100pct top-50pct transform3dy-n50 opacity-7">
                                    <img src="{{ asset('img/slide-title-border-light.png') }}"
                                        class="w-auto appear-animation" data-appear-animation="fadeInRightShorter"
                                        data-appear-animation-delay="250" data-plugin-options="{'minWindowWidth': 0}"
                                        alt="{{ trans('text.home-slider-1-title-1') }}" />
                                </span>
                                {{ trans('text.home-slider-1-title-1') }}

                                <span class="position-absolute left-100pct top-50pct transform3dy-n50 opacity-7">
                                    <img src="{{ asset('img/slide-title-border-light.png') }}"
                                        class="w-auto appear-animation" data-appear-animation="fadeInLeftShorter"
                                        data-appear-animation-delay="250" data-plugin-options="{'minWindowWidth': 0}"
                                        alt="{{ trans('text.home-slider-1-title-1') }}" />
                                </span>
                            </h3>
                            <h1 class="text-color-light font-weight-extra-bold text-10 text-sm-12-13 line-height-1 line-height-sm-3 mb-2 appear-animation"
                                data-appear-animation="blurIn" data-appear-animation-delay="500"
                                data-plugin-options="{'minWindowWidth': 0}">{{ trans('text.home-slider-1-title-2') }}
                            </h1>
                            <p class="text-4-5 text-color-light font-weight-light opacity-7 text-center mb-4"
                                data-plugin-animated-letters
                                data-plugin-options="{'startDelay': 1000, 'minWindowWidth': 0, 'animationSpeed': 30}">
                                {{ trans('text.home-slider-1-text-1') }}
                            </p>
                            <div class="appear-animation" data-appear-animation="fadeInUpShorter"
                                data-appear-animation-delay="2300">
                                <div class="d-flex align-items-center mt-2">
                                    <a href="{{ route('services') }}"
                                        class="btn btn-light btn-modern text-color-primary font-weight-bold text-2 py-3 btn-px-4">
                                        {{ trans('text.learn-more') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Carousel Slide 2 -->
        <div class="position-relative overlay overlay-show overlay-op-9 pt-5"
            style="background-image: url({{ asset('img/slide-2.jpg') }}); background-size: cover; background-position: center; height: 100vh;">
            <div class="container position-relative z-index-3 h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-lg-7 text-center">
                        <div class="d-flex flex-column align-items-center justify-content-center h-100">
                            <h3 class="position-relative text-color-light text-5 line-height-5 font-weight-medium ls-0 px-4 mb-2 appear-animation"
                                data-appear-animation="fadeInDownShorterPlus"
                                data-plugin-options="{'minWindowWidth': 0}">
                                <span class="position-absolute right-100pct top-50pct transform3dy-n50 opacity-7">
                                    <img src="{{ asset('img/slide-title-border-light.png') }}"
                                        class="w-auto appear-animation" data-appear-animation="fadeInRightShorter"
                                        data-appear-animation-delay="250" data-plugin-options="{'minWindowWidth': 0}"
                                        alt="{{ trans('text.home-slider-2-title-1') }}" />
                                </span>
                                {{ trans('text.home-slider-2-title-1') }}

                                <span class="position-absolute left-100pct top-50pct transform3dy-n50 opacity-7">
                                    <img src="{{ asset('img/slide-title-border-light.png') }}"
                                        class="w-auto appear-animation" data-appear-animation="fadeInLeftShorter"
                                        data-appear-animation-delay="250" data-plugin-options="{'minWindowWidth': 0}"
                                        alt="{{ trans('text.home-slider-2-title-1') }}" />
                                </span>
                            </h3>
                            <h1 class="text-color-light font-weight-extra-bold text-10 text-md-12-13 line-height-1 line-height-sm-3 mb-2 appear-animation"
                                data-appear-animation="blurIn" data-appear-animation-delay="500"
                                data-plugin-options="{'minWindowWidth': 0}">{{ trans('text.home-slider-2-title-2') }}
                            </h1>
                            <p class="text-4-5 text-color-light font-weight-light opacity-7 text-center mb-5"
                                data-plugin-animated-letters
                                data-plugin-options="{'startDelay': 1000, 'minWindowWidth': 0, 'animationSpeed': 30}">
                                {{ trans('text.home-slider-2-text-1') }}
                            </p>
                            <a href="{{ route('services') }}"
                                class="btn btn-primary btn-modern font-weight-bold text-2 py-3 btn-px-4 appear-animation"
                                data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1800"
                                data-plugin-options="{'minWindowWidth': 0}">{{ trans('text.learn-more') }}
                                </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="container home-services py-4">
    <div class="row pt-4 mt-5">
        <div class="col">
            <div class="row pt-2 clearfix">
                <div class="col-lg-6">
                    <div class="feature-box feature-box-style-2 reverse appear-animation"
                        data-appear-animation="fadeInRightShorter">
                        <div class="feature-box-icon">
                            <i class="fab fa-pied-piper text-color-primary"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="mb-2">{{ trans('text.home-service-title-1') }}</h4>
                            <p class="mb-4">{{ trans('text.home-service-text-1') }}.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="feature-box feature-box-style-2 appear-animation"
                        data-appear-animation="fadeInLeftShorter">
                        <div class="feature-box-icon">
                            <i class="fas fa-columns icons text-color-primary"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="mb-2">{{ trans('text.home-service-title-2') }}</h4>
                            <p class="mb-4">{{ trans('text.home-service-text-2') }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="feature-box feature-box-style-2 reverse appear-animation"
                        data-appear-animation="fadeInRightShorter" data-appear-animation-delay="200">
                        <div class="feature-box-icon">
                            <i class="fas fa-mobile-alt text-color-primary"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="mb-2">{{ trans('text.home-service-title-3') }}</h4>
                            <p class="mb-4">{{ trans('text.home-service-text-3') }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="feature-box feature-box-style-2 appear-animation"
                        data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="200">
                        <div class="feature-box-icon">
                            <i class="icon-note icons text-color-primary"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="mb-2">{{ trans('text.home-service-title-4') }}</h4>
                            <p class="mb-4">{{ trans('text.home-service-text-4') }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="feature-box feature-box-style-2 reverse appear-animation"
                        data-appear-animation="fadeInRightShorter" data-appear-animation-delay="400">
                        <div class="feature-box-icon">
                            <i class="icon-bubble icons text-color-primary"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="mb-2">{{ trans('text.home-service-title-5') }}</h4>
                            <p class="mb-4">{{ trans('text.home-service-text-5') }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="feature-box feature-box-style-2 appear-animation"
                        data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="400">
                        <div class="feature-box-icon">
                            <i class="fas fa-server text-color-primary"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="mb-2">{{ trans('text.home-service-title-6') }}</h4>
                            <p class="mb-4">{{ trans('text.home-service-text-6') }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="feature-box feature-box-style-2 reverse appear-animation"
                        data-appear-animation="fadeInRightShorter" data-appear-animation-delay="400">
                        <div class="feature-box-icon">
                            <i class="fas fa-share-alt-square text-color-primary"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="mb-2">{{ trans('text.home-service-title-7') }}</h4>
                            <p class="mb-4">{{ trans('text.home-service-text-7') }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="feature-box feature-box-style-2 appear-animation"
                        data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="400">
                        <div class="feature-box-icon">
                            <i class="icon-film icons text-color-primary"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="mb-2">{{ trans('text.home-service-title-8') }}</h4>
                            <p class="mb-4">{{ trans('text.home-service-text-8') }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row pb-5 mb-5 mt-3">
        <div class="col text-center">
            <a href="{{ route('services') }}" class="btn btn-primary btn-px-5 py-3 font-weight-semibold text-2 appear-animation"
                data-appear-animation="fadeInUpShorter"
                data-appear-animation-delay="300">{{ trans('text.learn-more') }}</a>
        </div>
    </div>
</div>

<section class="section home-who section-secondary border-0 py-0 m-0 appear-animation" data-appear-animation="fadeIn">
    <div class="container">
        <div class="row align-items-center justify-content-center justify-content-lg-between pb-5 pb-lg-0">
            <div class="col-lg-5 order-2 order-lg-1 pt-4 pt-lg-0 pb-5 pb-lg-0 mt-5 mt-lg-0 appear-animation"
                data-appear-animation="fadeInRightShorter" data-appear-animation-delay="200">
                <h2 class="font-weight-bold text-color-light text-7 mb-2">{{ trans('text.home-about-title') }}</h2>
                <p class="lead font-weight-light text-color-light text-4">{{ trans('text.home-about-sub-title') }}</p>
                <p class="font-weight-light text-color-light text-2 mb-4 opacity-7">{{ trans('text.home-about-text') }}
                </p>
                <a href="{{ route('about') }}" class="btn btn-dark-scale-2 btn-px-5 btn-py-2 text-2">{{ trans('text.learn-more') }}</a>
            </div>
            <div class="col-9 offset-lg-1 col-lg-5 order-1 order-lg-2 scale-2">
                <img class="img-fluid box-shadow-3 my-2 border-radius" src="{{ asset('img/image-1.jpg') }}"
                    alt="">
            </div>
        </div>
    </div>
</section>

<section class="section section-height-4 bg-color-grey-scale-1 border-0 m-0 pb-5">
    <div class="container">
        <div class="row justify-content-center my-4">
            <div class="col appear-animation" data-appear-animation="fadeInUpShorter">
                <div class="owl-carousel owl-theme nav-bottom rounded-nav"
                    data-plugin-options="{'items': 1, 'loop': true, 'autoHeight': true}">
                    @foreach ($testimonials as $testimonial)
                    <div>
                        <div class="col">
                            <div
                                class="testimonial testimonial-style-2 testimonial-with-quotes testimonial-quotes-dark mb-0">
                                <div class="testimonial-author">
                                    <img src="{{ getThumbnail($testimonial->images, 'post_images', 'ts') }}"
                                        class="img-fluid rounded-circle" alt="{{ $testimonial->title }}">
                                </div>
                                <blockquote>
                                    <p class="text-color-dark text-5 line-height-5">
                                        {!! $testimonial->body !!}
                                    </p>
                                </blockquote>
                                <div class="testimonial-author">
                                    <p class="opacity-10">
                                        <strong class="font-weight-extra-bold text-2">
                                            {{ $testimonial->authorName() }}
                                        </strong>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section home-projects bg-color-light border-0 pb-0 m-0">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center appear-animation" data-appear-animation="fadeInUpShorter">
                <h2 class="font-weight-normal text-6 pb-2 mb-4">
                    <strong class="font-weight-extra-bold">{{ trans('text.home-projects-title') }}</strong>
                </h2>
            </div>
        </div>
    </div>
    <div class="image-gallery sort-destination full-width mb-0">
        @php
        $delaies = ['700', '500', '300', '500', '700'];
        $animations = ['fadeInLeftShorter', 'fadeInLeftShorter', 'fadeIn', 'fadeInRightShorter', 'fadeInRightShorter'];
        @endphp
        @foreach ($projects as $key => $project)
        <div class="isotope-item">
            <div class="image-gallery-item mb-0 appear-animation" data-appear-animation="{{ $animations[$key] }}"
                data-appear-animation-delay="{{ $delaies[$key] }}">
                <a href="{{ route('portfolio.show', ['slug' => $project->slug ]) }}">
                    <span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
                        <span class="thumb-info-wrapper">
                            <img src="{{ getThumbnail($project->images, 'post_images', 'hm') }}" class="img-fluid"
                                alt="{{  $project->title }}">
                            <span class="thumb-info-title">
                                <span class="thumb-info-inner">{{  $project->title }}</span>
                                <span class="thumb-info-type">{{  $project->getCategoryName() }}</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon"><i class="fas fa-plus"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>
        @endforeach
    </div>
</section>

@endsection