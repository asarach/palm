<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>{{trans('text.home-page-title')}}</title>

    <meta name="keywords" content="{{ settings('main_keywords', 'News') }}" />
    <meta name="description" content="{{ settings('main_description', 'News') }}">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon" />
    <link rel="apple-touch-icon" href="{{ asset('img/apple-touch-icon.png') }}">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

    <!-- Web Fonts  -->
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light%7CPlayfair+Display:400&display=swap"
        rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/animate/animate.compat.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/simple-line-icons/css/simple-line-icons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/owl.carousel/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/owl.carousel/assets/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/magnific-popup/magnific-popup.min.css') }}">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <!-- Head Libs -->
    <script src="{{ asset('vendor/modernizr/modernizr.min.js') }}"></script>

</head>

<body class="loading-overlay-showing" data-plugin-page-transition data-loading-overlay
    data-plugin-options="{'hideDelay': 500}">
    <div class="loading-overlay">
        <div class="bounce-loader">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div class="body">
        <header id="header"
            class="header-transparent header-transparent-dark-bottom-border header-transparent-dark-bottom-border-1 header-effect-shrink"
            data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}">
            <div class="header-body border-top-0 bg-dark box-shadow-none">
                <div class="header-top header-top-borders">
                    <div class="container h-100">
                        <div class="header-row h-100">
                            <div class="header-column justify-content-start">
                                <div class="header-row">
                                    <nav class="header-nav-top">
                                        <ul class="nav nav-pills">
                                           
                                            <li class="nav-item nav-item-borders py-2">
                                                <a href="tel:123-456-7890">
                                                    <i class="fab fa-whatsapp text-4 text-color-primary"
                                                        style="top: 0;">
                                                    </i>
                                                    <a  dir="ltr"
                                                    class="p-0"
                                                href="tel:{{ trans('text.footer-phone') }}">{{ trans('text.header-top-phone') }}</a>
                                                </a>
                                            </li>
                                            <li class="nav-item nav-item-borders py-2 d-none d-md-inline-flex">
                                                <a href="mailto:mail@domain.com">
                                                    <i class="far fa-envelope text-4 text-color-primary"
                                                        style="top: 1px;">
                                                    </i>
                                                    {{ trans('text.header-top-email') }}
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-container container">
                    <div class="header-row">
                        <div class="header-column">
                            <div class="header-row">
                                <div class="header-logo">
                                    <a href="{{ route('index') }}">
                                        <img alt="Porto" width="82" height="40" src="{{ asset('img/logo-c.png') }}">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="header-column justify-content-end">
                            <div class="header-row">
                                <div
                                    class="header-nav header-nav-links header-nav-dropdowns-dark header-nav-light-text order-2 order-lg-1">
                                    <div
                                        class="header-nav-main header-nav-main-mobile-dark header-nav-main-square header-nav-main-dropdown-no-borders header-nav-main-effect-2 header-nav-main-sub-effect-1">
                                        <nav class="collapse">
                                            <ul class="nav nav-pills" id="mainNav">
                                                <li class="nav-item">
                                                    <a class="nav-link {{ setActive( 'index', 'active') }}" href="{{ route('index') }}">
                                                        {{ trans('text.header-nav-home') }}
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link {{ setActive( 'about', 'active') }}" href="{{ route('about') }}">
                                                        {{ trans('text.header-nav-about') }}
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link {{ setActive( 'services', 'active') }}" href="{{ route('services') }}">
                                                        {{ trans('text.header-nav-services') }}
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link {{ setActive( 'portfolio.*', 'active') }}" href="{{ route('portfolio') }}">
                                                        {{ trans('text.header-nav-portfolio') }}
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link {{ setActive( 'blog.*', 'active') }}" href="{{ route('blog') }}">
                                                        {{ trans('text.header-nav-blog') }}
                                                    </a>
                                                </li>
                                            </ul>

                                        </nav>
                                    </div>
                                    <button class="btn header-btn-collapse-nav" data-toggle="collapse"
                                        data-target=".header-nav-main nav">
                                        <i class="fas fa-bars"></i>
                                    </button>
                                </div>
                                <div
                                    class="header-nav-features header-nav-features-light header-nav-features-no-border header-nav-features-lg-show-border order-1 order-lg-2">
                                    <div class="header-nav-feature header-nav-features-search d-inline-flex">
                                        <a href="#" class="header-nav-features-toggle" data-focus="headerSearch">
                                            <i class="fas fa-search header-nav-top-icon"></i>
                                        </a>
                                        <div class="header-nav-features-dropdown header-nav-features-dropdown-mobile-fixed"
                                            id="headerTopSearchDropdown">
                                            <form role="search" action="{{ route('search') }}" method="get">
                                                <div class="simple-search input-group">
                                                    <input class="form-control text-1" id="headerSearch" name="q"
                                                        type="search" value="" placeholder="{{ trans('text.search') }}">
                                                    <span class="input-group-append">
                                                        <button class="btn" type="submit">
                                                            <i class="fa fa-search"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <div role="main" class="main">
            @yield('content')
        </div>

        <footer id="footer">
            <div class="container">
                <div class="row py-5 my-4">
                    <div class="col-md-6 mb-4 mb-lg-0">
                        <a href="{{ route('index') }}" class="logo pr-0 pl-lg-3">
                            <img alt="Porto Website Template" src="{{ asset('img/logo-w.png') }}"
                                class="opacity-7 bottom-4" height="33">
                        </a>
                        <p class="mt-2 mb-2">{{ trans('text.footer-about') }}</p>
                        <p class="mb-0">
                            <a href="{{ route('about') }}" class="btn-flat btn-xs text-color-light">
                                <strong class="text-2">{{ trans('text.view-more') }}</strong>
                                <i class="fas fa-angle-left p-relative top-1 pl-2"></i>
                            </a>
                        </p>
                    </div>
                    <div class="col-md-6">
                        <h5 class="text-3 mb-3">{{ trans('text.contact-us') }}</h5>
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="list list-icons list-icons-lg">
                                
                                    <li class="mb-1">
                                        <i class="fab fa-whatsapp text-color-primary"></i>
                                        <p class="m-0">
                                            <a  dir="ltr"
                                                href="tel:{{ trans('text.footer-phone') }}">{{ trans('text.footer-phone') }}</a>
                                        </p>
                                    </li>
                                    <li class="mb-1">
                                        <i class="far fa-envelope text-color-primary"></i>
                                        <p class="m-0">
                                            <a
                                                href="mailto:{{ trans('text.footer-email') }}">{{ trans('text.footer-email') }}</a>
                                        </p>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul class="list list-icons list-icons-sm">
                                    <li>
                                        <i class="fas fa-angle-left"></i>
                                        <a href="{{ route('faq') }}" class="link-hover-style-1 ml-1">
                                            {{ trans('text.faq') }}
                                        </a>
                                    </li>
                                    <li>
                                        <i class="fas fa-angle-left"></i>
                                        <a href="{{ route('sitemap') }}" class="link-hover-style-1 ml-1">
                                            {{ trans('text.sitemap') }}
                                        </a>
                                    </li>
                                    <li>
                                        <i class="fas fa-angle-left"></i>
                                        <a href="{{ route('contact') }}" class="link-hover-style-1 ml-1">
                                            {{ trans('text.contact-us') }}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-copyright footer-copyright-style-2">
                <div class="container py-2">
                    <div class="row py-4">
                        <div class="col d-flex align-items-center justify-content-center">
                            <p>{{ trans('text.footer-copyright', ['year' => '2020']) }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <!-- Vendor -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.appear/jquery.appear.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.cookie/jquery.cookie.min.js') }}"></script>
    <script src="{{ asset('vendor/popper/umd/popper.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('vendor/common/common.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.gmap/jquery.gmap.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.lazyload/jquery.lazyload.min.js') }}"></script>
    <script src="{{ asset('vendor/isotope/jquery.isotope.min.js') }}"></script>
    <script src="{{ asset('vendor/owl.carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('vendor/vide/jquery.vide.min.js') }}"></script>
    <script src="{{ asset('vendor/vivus/vivus.min.js') }}"></script>

    <!-- Theme Base, Components and Settings -->
    <script src="{{ asset('js/theme.js') }}"></script>

    <!-- Current Page Vendor and Views -->


    <!-- Theme Custom -->
    <script src="{{ asset('js/custom.js') }}"></script>


    <!-- Theme Initialization Files -->
    <script src="{{ asset('js/theme.init.js') }}"></script>

    <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-12345678-1', 'auto');
			ga('send', 'pageview');
		</script>
		 -->

</body>

</html>