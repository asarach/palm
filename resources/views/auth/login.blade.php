@extends('layouts.app')
@section('content')
<section
    class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-dark overlay-show overlay-op-7"
    style="background-image: url({{ asset('img/page-header-background.jpg') }});">
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12 align-self-center p-static order-2 text-center">
                <h1 class="text-9 font-weight-bold">{{trans('text.login-page-title')}}</h1>
            </div>
        </div>
    </div>
</section>
<div class="container py-4">

    <div class="row justify-content-center">
        <div class="col-md-6 col-lg-5 mb-5 mb-lg-0">
            <h2 class="font-weight-bold text-5 mb-0">{{trans('text.login')}}</h2>
            <form action="{{ route('login') }}" id="frmSignIn" method="post" class="needs-validation">
                @csrf
                <div class="form-row">
                    <div class="form-group col">
                        <label class="text-color-dark text-3">{{trans('text.email')}}<span class="text-color-danger">*</span></label>
                        <input type="email" name="email" :value="old('email')" required autofocus class="form-control form-control-lg text-4">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label class="text-color-dark text-3">{{trans('text.password')}}<span class="text-color-danger">*</span></label>
                        <input type="password" name="password" required autocomplete="current-password" class="form-control form-control-lg text-4" >
                    </div>
                </div>
                <div class="form-row justify-content-between">
                    <div class="form-group col-md-auto">
                        <div class="custom-control ">
                            <input type="checkbox" class="" name="remember">
                            <label class="cur-pointer text-2" for="rememberme">{{trans('text.rememberme')}}</label>
                        </div>
                    </div>
                    <div class="form-group col-md-auto">
                        <a class="text-decoration-none text-color-dark text-color-hover-primary font-weight-semibold text-2" href="{{ route('password.request') }}">{{trans('text.forgot-password')}}</a>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <button type="submit" class="btn btn-dark btn-modern btn-block text-uppercase rounded-0 font-weight-bold text-3 py-3" data-loading-text="Loading...">{{trans('text.login')}}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection