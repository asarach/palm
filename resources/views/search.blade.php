@extends('layouts.app')
@section('meta')
<title>{{trans('text.search')}} - {{ settings('app_name', 'News') }}</title>
<meta name="keywords" content="{{ settings('main_keywords', 'News') }}">
<meta name="description" content="{{ settings('main_description', 'News') }}">
@endsection
@section('content')
<section
  class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-dark overlay-show overlay-op-7"
  style="background-image: url({{ asset('img/page-header-background.jpg') }});">
  <div class="container">
    <div class="row mt-5">
      <div class="col-md-12 align-self-center p-static order-2 text-center">
        <h1 class="text-9 font-weight-bold">{{trans('text.search')}}</h1>
        <span class="sub-title">{{trans('text.search-result-for', ['number' => $posts->total()  ])}} <strong>{{ request('q') }}</strong></span>
      </div>
    </div>
  </div>
</section>
<div class="container py-5 mt-3">
  <div class="row">
    <div class="col">
      <ul class="simple-post-list m-0">
        @foreach ($posts as $post)
        <li>
          <div class="post-info">
            <a href="{{ $post->getLink() }}">{{ $post->title }}</a>
            <div class="post-meta">
              <span class="text-dark text-uppercase font-weight-semibold">{{ $post->getCategoryName() }}</span> | {{ formatDateB($post->created_at ) }}
            </div>
          </div>
        </li>
        @endforeach
      </ul>
      {{ $posts->appends(['q' => request('q')])->links() }}
    </div>
  </div>
</div>
@endsection