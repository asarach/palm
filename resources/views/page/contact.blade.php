@extends('layouts.app')
@section('meta')
<title>{{$page->title}} - {{ settings('app_name', 'News') }}</title>
<meta name="keywords" content="{{$page->keywords}}">
<meta name="description" content="{{$page->description}}">
@endsection
@section('content')
<div id="googlemaps" class="google-map mt-0" style="height: 500px;"></div>
<div class="container">
    <div class="row py-4">
        <div class="col-lg-6">
            <div class="overflow-hidden mb-1">
                <h2 class="font-weight-normal text-7 mt-2 mb-0 appear-animation" data-appear-animation="maskUp"
                    data-appear-animation-delay="200">
                    <strong class="font-weight-extra-bold">{{trans('text.contact-us')}}</strong>
                </h2>
            </div>
            <div class="overflow-hidden mb-4 pb-3">
                <p class="mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="400">
                    {{trans('text.contact-page-text-1')}}
                </p>
            </div>
            <form class="contact-form" role="form" method="POST" action="{{ route('store.contact') }}">
                @csrf
                @honeypot

                @if ($errors->any())
                <div class="contact-form-error alert alert-danger mt-4">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <div class="form-row">
                    <div class="form-group col-lg-6">
                        <label class="required font-weight-bold text-dark text-2">{{trans('text.name')}}</label>
                        <input type="text" value="{{ old('name') }}"
                            data-msg-required="{{trans('text.please-enter-your-name')}}" maxlength="100"
                            class="form-control @error('name') is-invalid @enderror"" name=" name" required>
                    </div>

                    <div class="form-group col-lg-6">
                        <label class="required font-weight-bold text-dark text-2">{{trans('text.email')}}</label>
                        <input type="email" value="{{ old('email') }}"
                            data-msg-required="{{trans('text.please-enter-your-email-address')}}"
                            data-msg-email="{{trans('text.please-enter-your-email-address')}}" maxlength="100"
                            class="form-control @error('email') is-invalid @enderror"" name=" email" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label class="font-weight-bold text-dark text-2">{{trans('text.subject')}}</label>
                        <input type="text" value="{{ old('subject') }}"
                            data-msg-required="{{trans('text.please-enter-the-subject')}}" maxlength="100"
                            class="form-control @error('subject') is-invalid @enderror" name="subject" required>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col">
                        <label class="required font-weight-bold text-dark text-2">{{trans('text.message')}}</label>
                        <textarea maxlength="5000" name="body"
                            data-msg-required="{{trans('text.please-enter-the-message')}}" rows="8"
                            class="form-control  @error('body') is-invalid @enderror"
                            required>{{ old('body') }}</textarea>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <input type="submit" value="{{trans('text.send-message')}}" class="btn btn-primary btn-modern"
                            data-loading-text="{{trans('text.loading')}}">
                    </div>
                </div>
            </form>

        </div>
        <div class="col-lg-6">

            <div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="800">
                <h4 class="mt-2 mb-1">{{trans('text.our-office')}}</h4>
                <ul class="list list-icons list-icons-style-2 mt-2">
                    <li>
                        <i class="fas fa-map-marker-alt top-6"></i>
                        <strong class="text-dark">{{trans('text.address')}}:</strong>
                        {{ trans('text.footer-adress') }}
                    </li>
                    <li>
                        <i class="fas fa-phone top-6"></i>
                        <strong class="text-dark">{{trans('text.phone')}}:</strong>
                        {{ trans('text.footer-phone') }}</li>
                    <li>
                        <i class="fas fa-envelope top-6"></i>
                        <strong class="text-dark">{{trans('text.email')}}:</strong>
                        <a href="mailto:{{ trans('text.footer-email') }}">{{ trans('text.footer-email') }}</a></li>
                </ul>
            </div>

            <div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="950">
                <h4 class="pt-5">{{trans('text.contact-page-business-hours')}}</h4>
                <ul class="list list-icons list-dark mt-2">
                    <li><i class="far fa-clock top-6"></i> {{trans('text.contact-page-business-hours-1')}}</li>
                    <li><i class="far fa-clock top-6"></i> {{trans('text.contact-page-business-hours-2')}}</li>
                    <li><i class="far fa-clock top-6"></i> {{trans('text.contact-page-business-hours-3')}}</li>
                </ul>
            </div>

            <h4 class="pt-5">{{$page->title}}</h4>
            <p class="lead mb-0 text-4">
                {!!$page->content!!}
            </p>

        </div>

    </div>

</div>

@endsection