@extends('layouts.app')
@section('meta')
<title>{{trans('text.sitemap-page-title')}} - {{ settings('app_name', 'News') }}</title>
<meta name="keywords" content="{{ settings('main_keywords', 'News') }}">
<meta name="description" content="{{ settings('main_description', 'News') }}">
@endsection
@section('content')
<section
    class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-dark overlay-show overlay-op-7"
    style="background-image: url({{ asset('img/page-header-background.jpg') }});">
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12 align-self-center p-static order-2 text-center">
                <h1 class="text-9 font-weight-bold">{{trans('text.sitemap-page-title')}}</h1>
            </div>
        </div>
    </div>
</section>
<div class="container py-4">
    <div class="row">
        <div class="col-sm-3">
            <h5 class="mb-2"><a class="nav-link p-0" href="{{ route('index') }}">{{ trans('text.home') }}</a></h5>
            <h5 class="mb-2"><a class="nav-link p-0" href="{{ route('blog') }}">{{ trans('text.blog') }}</a></h5>
            <h5 class="mb-2"><a class="nav-link p-0" href="{{ route('portfolio') }}">{{ trans('text.portfolio') }}</a></h5>
            <h5 class="mb-2"><a class="nav-link p-0" href="{{ route('about') }}">{{ trans('text.about') }}</a></h5>
            <h5 class="mb-2"><a class="nav-link p-0" href="{{ route('faq') }}">{{ trans('text.faq') }}</a></h5>
            <h5 class="mb-2"><a class="nav-link p-0" href="{{ route('services') }}">{{ trans('text.services') }}</a></h5>
            <h5 class="mb-2"><a class="nav-link p-0" href="{{ route('sitemap') }}">{{ trans('text.sitemap') }}</a></h5>
            <h5 class="mb-4"><a class="nav-link p-0" href="{{ route('contact') }}">{{ trans('text.contact') }}</a></h5>
            <ul class="list list-icons list-icons-sm mb-4">
                @foreach ($posts as $post)
                <li>
                    <a href="{!! $post->getLink() !!}">
                        <i class="far fa-file"></i>
                        {{ $post->title}}
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@endsection