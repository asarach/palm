@extends('layouts.app')
@section('meta')
<title>{{trans('text.services-page-title')}} - {{ settings('app_name', 'News') }}</title>
<meta name="keywords" content="{{ settings('main_keywords', 'News') }}">
<meta name="description" content="{{ settings('main_description', 'News') }}">
@endsection
@section('content')
<section class="page-header  mb-0 page-header-modern page-header-background page-header-background-md overlay overlay-color-dark overlay-show overlay-op-7" style="background-image: url({{ asset('img/page-header-background.jpg') }});">
  <div class="container">
    <div class="row mt-5">
      <div class="col-md-12 align-self-center p-static order-2 text-center">
        <h1 class="text-10">{{trans('text.services-page-title')}}</h1>
        <span class="sub-title">{{trans('text.services-page-subtitle')}}</span>
      </div>
    </div>
  </div>
</section>


<div class="container-fluid services-page">
  <div class="row align-items-center bg-color-grey">
    <div class="col-lg-6 p-0">
      <section class="parallax section section-parallax custom-parallax-bg-pos-left custom-sec-left h-100 m-0" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'horizontalPosition': '100%'}" data-image-src="img/services-1.jpg" style="min-height: 315px;">
      </section>
    </div>
    <div class="col-lg-6 p-0">
      <section class="section service-section section-no-border h-100 m-0">
        <div class="row m-0">
          <div class="col-half-section col-half-section-left">
            <div class="overflow-hidden left-border">
              <h4 class="mb-0 appear-animation" data-appear-animation="maskUp">{{trans('text.services-page-services-title-1')}}</h4>
            </div>
            <div class="overflow-hidden">
              <p class="mb-0 lead text-4 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="200">{{trans('text.services-page-services-text-1')}}</p>
              <p class="text-2 mb-0 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">{{trans('text.services-page-services-textb-1')}}</p>

            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
  <div class="row align-items-center bg-color-grey">
    <div class="col-lg-6 order-2 order-lg-1 p-0">
      <section class="section service-section section-no-border h-100 m-0">
        <div class="row justify-content-end m-0">
          <div class="col-half-section col-half-section-right custom-text-align-right">
            <div class="overflow-hidden left-border">
              <h4 class="mb-0appear-animation" data-appear-animation="maskUp">{{trans('text.services-page-services-title-2')}}</h4>
            </div>
            <div class="overflow-hidden">
              <p class="mb-0 lead text-4 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="200">{{trans('text.services-page-services-text-2')}}</p>
              <p class="text-2 mb-0 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">{{trans('text.services-page-services-textb-2')}}</p>
            </div>
          </div>
        </div>
      </section>
    </div>
    <div class="col-lg-6 order-1 order-lg-2 p-0">
      <section class="parallax section section-parallax h-100 m-0" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'horizontalPosition': '100%'}" data-image-src="img/services-2.jpg" style="min-height: 315px;">
      </section>
    </div>
  </div>
  <div class="row align-items-center bg-color-grey">
    <div class="col-lg-6 p-0">
      <section class="parallax section section-parallax custom-parallax-bg-pos-left custom-sec-left h-100 m-0" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'horizontalPosition': '100%'}" data-image-src="img/services-3.jpg" style="min-height: 315px;">
      </section>
    </div>
    <div class="col-lg-6 p-0">
      <section class="section service-section section-no-border h-100 m-0">
        <div class="row m-0">
          <div class="col-half-section col-half-section-left">
            <div class="overflow-hidden left-border">
              <h4 class="mb-0 appear-animation" data-appear-animation="maskUp">{{trans('text.services-page-services-title-3')}}</h4>
            </div>
            <div class="overflow-hidden">
              <p class="mb-0 lead text-4 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="200">{{trans('text.services-page-services-text-3')}}</p>
              <p class="text-2 mb-0 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">{{trans('text.services-page-services-textb-3')}}</p>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
  <div class="row align-items-center bg-color-grey">
    <div class="col-lg-6 order-2 order-lg-1 p-0">
      <section class="section service-section section-no-border h-100 m-0">
        <div class="row justify-content-end m-0">
          <div class="col-half-section col-half-section-right custom-text-align-right">
            <div class="overflow-hidden left-border">
              <h4 class="mb-0appear-animation" data-appear-animation="maskUp">{{trans('text.services-page-services-title-4')}}</h4>
            </div>
            <div class="overflow-hidden">
              <p class="mb-0 lead text-4 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="200">{{trans('text.services-page-services-text-4')}}</p>
              <p class="text-2 mb-0 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">{{trans('text.services-page-services-textb-4')}}</p>
            </div>
          </div>
        </div>
      </section>
    </div>
    <div class="col-lg-6 order-1 order-lg-2 p-0">
      <section class="parallax section section-parallax h-100 m-0" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'horizontalPosition': '100%'}" data-image-src="img/services-4.jpg" style="min-height: 315px;">
      </section>
    </div>
  </div>
  <div class="row align-items-center bg-color-grey">
    <div class="col-lg-6 p-0">
      <section class="parallax section section-parallax custom-parallax-bg-pos-left custom-sec-left h-100 m-0" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'horizontalPosition': '100%'}" data-image-src="img/services-5.jpg" style="min-height: 315px;">
      </section>
    </div>
    <div class="col-lg-6 p-0">
      <section class="section service-section section-no-border h-100 m-0">
        <div class="row m-0">
          <div class="col-half-section col-half-section-left">
            <div class="overflow-hidden left-border">
              <h4 class="mb-0 appear-animation" data-appear-animation="maskUp">{{trans('text.services-page-services-title-5')}}</h4>
            </div>
            <div class="overflow-hidden">
              <p class="mb-0 lead text-4 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="200">{{trans('text.services-page-services-text-5')}}</p>
              <p class="text-2 mb-0 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">{{trans('text.services-page-services-textb-5')}}</p>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
  <div class="row align-items-center bg-color-grey">
    <div class="col-lg-6 order-2 order-lg-1 p-0">
      <section class="section service-section section-no-border h-100 m-0">
        <div class="row justify-content-end m-0">
          <div class="col-half-section col-half-section-right custom-text-align-right">
            <div class="overflow-hidden left-border">
              <h4 class="mb-0appear-animation" data-appear-animation="maskUp">{{trans('text.services-page-services-title-6')}}</h4>
            </div>
            <div class="overflow-hidden">
              <p class="mb-0 lead text-4 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="200">{{trans('text.services-page-services-text-6')}}</p>
              <p class="text-2 mb-0 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">{{trans('text.services-page-services-textb-6')}}</p>
            </div>
          </div>
        </div>
      </section>
    </div>
    <div class="col-lg-6 order-1 order-lg-2 p-0">
      <section class="parallax section section-parallax h-100 m-0" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'horizontalPosition': '100%'}" data-image-src="img/services-6.jpg" style="min-height: 315px;">
      </section>
    </div>
  </div>
  <div class="row align-items-center bg-color-grey">
    <div class="col-lg-6 p-0">
      <section class="parallax section section-parallax custom-parallax-bg-pos-left custom-sec-left h-100 m-0" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'horizontalPosition': '100%'}" data-image-src="img/services-7.jpg" style="min-height: 315px;">
      </section>
    </div>
    <div class="col-lg-6 p-0">
      <section class="section service-section section-no-border h-100 m-0">
        <div class="row m-0">
          <div class="col-half-section col-half-section-left">
            <div class="overflow-hidden left-border">
              <h4 class="mb-0 appear-animation" data-appear-animation="maskUp">{{trans('text.services-page-services-title-7')}}</h4>
            </div>
            <div class="overflow-hidden">
              <p class="mb-0 lead text-4 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="200">{{trans('text.services-page-services-text-7')}}</p>
              <p class="text-2 mb-0 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">{{trans('text.services-page-services-textb-7')}}</p>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
  <div class="row align-items-center bg-color-grey">
    <div class="col-lg-6 order-2 order-lg-1 p-0">
      <section class="section service-section section-no-border h-100 m-0">
        <div class="row justify-content-end m-0">
          <div class="col-half-section col-half-section-right custom-text-align-right">
            <div class="overflow-hidden left-border">
              <h4 class="mb-0appear-animation" data-appear-animation="maskUp">{{trans('text.services-page-services-title-8')}}</h4>
            </div>
            <div class="overflow-hidden">
              <p class="mb-0 lead text-4 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="200">{{trans('text.services-page-services-text-8')}}</p>
              <p class="text-2 mb-0 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">{{trans('text.services-page-services-textb-8')}}</p>
            </div>
          </div>
        </div>
      </section>
    </div>
    <div class="col-lg-6 order-1 order-lg-2 p-0">
      <section class="parallax section section-parallax h-100 m-0" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'horizontalPosition': '100%'}" data-image-src="img/services-8.jpg" style="min-height: 315px;">
      </section>
    </div>
  </div>
</div>


@endsection