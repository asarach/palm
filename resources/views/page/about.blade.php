@extends('layouts.app')
@section('meta')
<title>{{trans('text.about-us')}} - {{ settings('app_name', 'News') }}</title>
<meta name="keywords" content="{{ settings('main_keywords', 'News') }}">
<meta name="description" content="{{ settings('main_description', 'News') }}">
@endsection
@section('content')
<section class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-dark overlay-show overlay-op-7" style="background-image: url({{ asset('img/page-header-about-us.jpg') }});">
  <div class="container">
    <div class="row mt-5">
      <div class="col-md-12 align-self-center p-static order-2 text-center">
        <h1 class="text-9 font-weight-bold">{{trans('text.about-us')}}</h1>
        <span class="sub-title">{{trans('text.about-us-subtitle')}}</span>
      </div>
    </div>
  </div>
</section>
<div class="container">
  <div class="row pt-5">
    <div class="col">
      <div class="row text-center pb-5">
        <div class="col-md-9 mx-md-auto">
          <div class="overflow-hidden mb-3">
            <h1 class="word-rotator slide font-weight-bold text-8 mb-0 appear-animation" data-appear-animation="maskUp">
              <span>{{trans('text.about-page-line-1-part-1')}}</span>
              <span class="word-rotator-words bg-primary">
                <b class="is-visible">{{trans('text.about-page-line-1-word-1')}}</b>
                <b>{{trans('text.about-page-line-1-word-2')}}</b>
                <b>{{trans('text.about-page-line-1-word-3')}}</b>
              </span>
              <span> {{trans('text.about-page-line-1-part-3')}}</span>
            </h1>
          </div>
          <div class="overflow-hidden mb-3">
            <p class="lead mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="200">
              {{trans('text.about-page-text-1')}}
            </p>
          </div>
        </div>
      </div>
      <div class="row mt-3 mb-5">
        <div class="col-md-4 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="800">
          <h3 class="font-weight-bold text-4 mb-2">{{trans('text.about-page-mission-title')}}</h3>
          <p>{{trans('text.about-page-mission-text')}}</p>
        </div>
        <div class="col-md-4 appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="600">
          <h3 class="font-weight-bold text-4 mb-2">{{trans('text.about-page-vision-title')}}</h3>
          <p>{{trans('text.about-page-vision-text')}}</p>
        </div>
        <div class="col-md-4 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="800">
          <h3 class="font-weight-bold text-4 mb-2">{{trans('text.about-page-why-title')}}</h3>
          <p>{{trans('text.about-page-why-text')}}</p>
        </div>
      </div>
    </div>
  </div>
</div>
<section class="section section-height-3 bg-color-grey-scale-1 m-0 border-0">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-lg-6 pb-sm-4 pb-lg-0 pr-lg-5 mb-sm-5 mb-lg-0">
        <h2 class="text-color-dark font-weight-normal text-6 mb-2">{{trans('text.about-page-who')}} <strong class="font-weight-extra-bold">{{trans('text.about-page-we-are')}}</strong></h2>
        <p class="lead">{{trans('text.about-page-who-text-1')}}</p>
        <p class="pl-5 ml-5">{{trans('text.about-page-who-text-2')}}</p>
      </div>
      <div class="col-sm-8 col-md-6 col-lg-4 offset-sm-4 offset-md-4 offset-lg-2 mt-sm-5" style="top: 1.7rem;">
        <img src="{{asset('img/about-1.jpg')}}" class="img-fluid position-absolute d-none d-sm-block appear-animation" data-appear-animation="expandIn" data-appear-animation-delay="300" style="top: 10%; left: -50%;" alt="" />
        <img src="{{asset('img/about-2.jpg')}}" class="img-fluid position-absolute d-none d-sm-block appear-animation" data-appear-animation="expandIn" style="top: -33%; left: -29%;" alt="" />
        <img src="{{asset('img/about-3.jpg')}}" class="img-fluid position-relative appear-animation mb-2" data-appear-animation="expandIn" data-appear-animation-delay="600" alt="" />
      </div>
    </div>
  </div>
</section>

@endsection