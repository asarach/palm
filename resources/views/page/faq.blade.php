@extends('layouts.app')
@section('meta')
<title>{{trans('text.faq-page-title')}} - {{ settings('app_name', 'News') }}</title>
<meta name="keywords" content="{{ settings('main_keywords', 'News') }}">
<meta name="description" content="{{ settings('main_description', 'News') }}">
@endsection
@section('content')
<section
    class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-dark overlay-show overlay-op-7"
    style="background-image: url({{ asset('img/page-header-background.jpg') }});">
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12 align-self-center p-static order-2 text-center">
                <h1 class="text-9 font-weight-bold">{{trans('text.faq-page-title')}}</h1>
                <span class="sub-title">{{trans('text.faq-page-subtitle')}}</span>
            </div>
        </div>
    </div>
</section>

<div class="container py-4">
    <div class="row">
        <div class="col">
            <div class="toggle toggle-primary" data-plugin-toggle>
                <section class="toggle active">
                    <a class="toggle-title">{{trans('text.faq-page-title-1')}}</a>
                    <div class="toggle-content">
                        <p>{{trans('text.faq-page-text-1')}}</p>
                    </div>
                </section>

                <section class="toggle">
                    <a class="toggle-title">{{trans('text.faq-page-title-2')}}</a>
                    <div class="toggle-content">
                        <p>{{trans('text.faq-page-text-2')}}</p>
                    </div>
                </section>

                <section class="toggle">
                    <a class="toggle-title">{{trans('text.faq-page-title-3')}}</a>
                    <div class="toggle-content">
                        <p>{{trans('text.faq-page-text-3')}}</p>
                    </div>
                </section>

                <section class="toggle">
                    <a class="toggle-title">{{trans('text.faq-page-title-4')}}</a>
                    <div class="toggle-content">
                        <p>{{trans('text.faq-page-text-4')}}</p>
                    </div>
                </section>

                <section class="toggle">
                    <a class="toggle-title">{{trans('text.faq-page-title-5')}}</a>
                    <div class="toggle-content">
                        <p>{{trans('text.faq-page-text-5')}}</p>
                    </div>
                </section>

                <section class="toggle">
                    <a class="toggle-title">{{trans('text.faq-page-title-6')}}</a>
                    <div class="toggle-content">
                        <p>{{trans('text.faq-page-text-6')}}</p>
                    </div>
                </section>

                <section class="toggle">
                    <a class="toggle-title">{{trans('text.faq-page-title-7')}}</a>
                    <div class="toggle-content">
                        <p>{{trans('text.faq-page-text-7')}}</p>
                    </div>
                </section>

                <section class="toggle">
                    <a class="toggle-title">{{trans('text.faq-page-title-8')}}</a>
                    <div class="toggle-content">
                        <p>{{trans('text.faq-page-text-8')}}</p>
                    </div>
                </section>

                <section class="toggle">
                    <a class="toggle-title">{{trans('text.faq-page-title-9')}}</a>
                    <div class="toggle-content">
                        <p>{{trans('text.faq-page-text-9')}}</p>
                    </div>
                </section>
            </div>

        </div>

    </div>

</div>

@endsection