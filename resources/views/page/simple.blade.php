@extends('layouts.app')
@section('meta')
<title>{{$page->title}} - {{ settings('app_name', 'News') }}</title>
<meta name="keywords" content="{{$page->keywords}}">
<meta name="description" content="{{$page->description}}">
@endsection
@section('content')
<div class="page simple-page">
    <div class="container">
        <div class="row">
          <div class="col-lg-9 col-12">
                <main class="main-content">
                    <div class="card">
                        <div class="card-body">
                            <h1 class="page-title">{{$page->title}}</h1>
                            <div class="page-text">
                                {!!$page->content!!}
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div class="col-lg-3 order-first order-lg-last">
                @include('sidemenu', ['is_top' => false])
            </div>
        </div>
    </div>
</div>
@endsection
@section('sitemap')
<div class="page sitemap-page">
    <div class="container">
        <div class="row">
          <div class="col-12">
                <main class="main-content">
                    <div class="card">
                        <div class="card-body">
                            <h2><a class="nav-link" href="{{ route('index') }}">{{ trans('front.home') }}</a></h2>
                            <h2><a class="nav-link" href="{{ route('files') }}">{{ trans('front.files') }}</a></h2>
                            <h2><a class="nav-link" href="{{ route('appointments') }}">{{ trans('front.appointments') }}</a></h2>
                            <h2><a class="nav-link" href="{{ route('bmi') }}">{{ trans('front.bmi') }}</a></h2>
                            <h2><a class="nav-link" href="{{ route('photos') }}">{{ trans('front.images') }}</a></h2>
                            <h2><a class="nav-link" href="{{ route('contact') }}">{{ trans('front.contact-us') }}</a></h2>
                            <h2><a class="nav-link" href="{{ route('posts') }}">{{ trans('front.posts') }}</a></h2>
                            @foreach ($posts as $post)
                              <h3 class="card-title"><a href="{!! route('post', ['slug' => $post->slug]) !!}">{{ $post->title}}</a></h3>
                            @endforeach
                        </div>
                    </div>
                </main>
            </div>
        </div>
    </div>
</div>
@endsection

@section('post-index')
<div class="container articles-index">
    <div class="row">
        <div class="col-lg-8 col-12">
            <main class="main-content">

                <div class="items-index">
                    <div id="items_listing" class="items_listing list">
                        @include('post.loop', ['posts' => $posts, 'per_deck' => 2])
                    </div>
                    {{ $posts->links() }}
                </div>
            </main>
        </div>
        <div class="col-lg-4  order-last">
            @component('sidemenu')
            @slot('top')

            @if (!$stickies->isEmpty())
            <aside class="card widget posts d-none d-lg-block">
                <div class="card-body">
                    <div class="widget-title">
                        <h5>{{trans('front.sticky-posts-title')}}</h5>
                    </div>
                    <div class="widget-body">
                        @foreach ($stickies as $sticky)
                        <div class="card list-card article">
                            <div class="card-image">
                                <a href="{!! route('blog.show', ['slug' => $sticky->slug]) !!}">
                                    <img src="{{getThumbnail($sticky->images, 'post_images', 'xs')}}" class="card-img"
                                        title="{{ $sticky->title}}" alt="{{ $sticky->title}}">
                                </a>
                            </div>
                            <div class="card-body">
                                <a href="{!! route('blog.show', ['slug' => $sticky->slug]) !!}">
                                    <h5 class="card-title">{{ $sticky->title}}</h5>
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </aside>
            @endif

            @if (!$populars->isEmpty())
            <aside class="card widget posts d-none d-lg-block">
                <div class="card-body">
                    <div class="widget-title">
                        <h5>{{trans('front.populars-posts-title')}}</h5>
                    </div>
                    <div class="widget-body">
                        @foreach ($populars as $popular)
                        <div class="card list-card article">
                            <div class="card-image">
                                <a href="{!! route('blog.show', ['slug' => $popular->slug]) !!}">
                                    <img src="{{getThumbnail($popular->images, 'post_images', 'xs')}}" class="card-img"
                                        title="{{ $popular->title}}" alt="{{ $popular->title}}">
                                </a>
                            </div>
                            <div class="card-body">
                                <a href="{!! route('blog.show', ['slug' => $popular->slug]) !!}">
                                    <h5 class="card-title">{{ $popular->title}}</h5>
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </aside>
            @endif

            @endslot
            @slot('bottom')
            @endslot
            @endcomponent
        </div>
    </div>
</div>
@endsection

@section('post-show')
<div class="content-header">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">{{trans('front.home')}}</a></li>
                <li class="breadcrumb-item"><a href="{{ route('post.index') }}">{{trans('front.posts')}}</a></li>
            </ol>
        </nav>
        <h1 class="card-title">
            {{$post->title}}
        </h1>

    </div>
</div>
<div class="container articles-show">
    <div class="row">
        <div class="col-lg-8">
            <main class="main-content">
                <div class="card card-single">
                    <div class="card-header">

                        <div class="card-views">
                            {{ $post->total_views . ' ' . trans('front.views')}}
                        </div>

                    </div>
                    <div class="card-image">
                        <img src="{{getThumbnail($post->images, 'post_images', 'lg')}}" class="card-img"
                            title="{{ $post->title}}" alt="{{ $post->title}}">
                    </div>
                    <div class="card-meta">
                        @include('share', [
                        'url' => route('blog.show', $post->slug),
                        'title' => $post->title,
                        'description' => $post->summary,
                        'hashtags' => $post->keywords])
                    </div>
                    <div class="card-date">
                        {{ formatDateB($post->created_at)}}
                    </div>

                    <div class="card-body">
                        {!! html_entity_decode($post->body) !!}
                    </div>
                    @if (Auth::check() and Auth::user()->hasRole(['admin', 'super-admin']))
                    <div class="card-edit">
                        <a target="_blank" href="{!! route('admin::post.edit', ['id' => $post->id]) !!}">
                            <div class="btn btn-info">تعديل</div>
                        </a>

                    </div>
                    @endif
                </div>

                @if ($post->comment_status)
                <comments post_type="post" post_id="{{ $post->id }}"></comments>
                @endif

                <div class="recommendations_listing">
                    <div class="recommendations-title">
                        <h3>{{trans('front.posts-recommendations')}}</h3>
                    </div>
                    <div id="items_listing" class="items_listing list">
                        @include('post.loop', ['posts' => $promotes, 'per_deck' => 2])
                    </div>
                </div>
            </main>
        </div>
        <div class="col-lg-4  order-last">
            @component('sidemenu')
            @slot('top')

            @if (!$stickies->isEmpty())
            <aside class="card widget posts d-none d-lg-block">
                <div class="card-body">
                    <div class="widget-title">
                        <h5>{{trans('front.sticky-posts-title')}}</h5>
                    </div>
                    <div class="widget-body">
                        @foreach ($stickies as $sticky)
                        <div class="card list-card article">
                            <div class="card-image">
                                <a href="{!! route('blog.show', ['slug' => $sticky->slug]) !!}">
                                    <img src="{{getThumbnail($sticky->images, 'post_images', 'xs')}}" class="card-img"
                                        title="{{ $sticky->title}}" alt="{{ $sticky->title}}">
                                </a>
                            </div>
                            <div class="card-body">
                                <a href="{!! route('blog.show', ['slug' => $sticky->slug]) !!}">
                                    <h5 class="card-title">{{ $sticky->title}}</h5>
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </aside>
            @endif

            @if (!$populars->isEmpty())
            <aside class="card widget posts d-none d-lg-block">
                <div class="card-body">
                    <div class="widget-title">
                        <h5>{{trans('front.populars-posts-title')}}</h5>
                    </div>
                    <div class="widget-body">
                        @foreach ($populars as $popular)
                        <div class="card list-card article">
                            <div class="card-image">
                                <a href="{!! route('blog.show', ['slug' => $popular->slug]) !!}">
                                    <img src="{{getThumbnail($popular->images, 'post_images', 'xs')}}" class="card-img"
                                        title="{{ $popular->title}}" alt="{{ $popular->title}}">
                                </a>
                            </div>
                            <div class="card-body">
                                <a href="{!! route('blog.show', ['slug' => $popular->slug]) !!}">
                                    <h5 class="card-title">{{ $popular->title}}</h5>
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </aside>
            @endif


            @endslot
            @slot('bottom')

            @endslot

            @endcomponent
        </div>
    </div>
</div>

@endsection