<article class="post post-medium border-0 pb-0 mb-5">
  <div class="post-image">
    <a href="{{ $post->getLink() }}">
      <img src="{{ getThumbnail($post->images, 'post_images', 'md') }}"
        class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0" alt="{{ $post->title }}" />
    </a>
  </div>
  <div class="post-content">
    <h2 class="font-weight-semibold text-5 line-height-6 mt-3 mb-2">
      <a href="{{  $post->getLink() }}">{{  $post->title }}</a>
    </h2>
    <p>{{ $post->excerpt }}</p>
    <div class="post-meta">
      <span><i class="far fa-user"></i> {{ trans('text.by') }} {{ $post->authorName() }}</span>
      <span><i class="far fa-folder"></i>{{ $post->getCategoryName() }}</span>
    </div>
  </div>
</article>