@extends('layouts.app')
@section('meta')
<title>{{trans('text.blog-index-title')}} - {{ settings('app_name', 'News') }}</title>
<meta name="keywords" content="{{ settings('main_keywords', 'News') }}">
<meta name="description" content="{{ settings('main_description', 'News') }}">
@endsection
@section('content')
<section
    class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-dark overlay-show overlay-op-7"
    style="background-image: url({{asset('img/page-header-blog.jpg')}});">
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12 align-self-center p-static order-2 text-center">
                <h1 class="text-9 font-weight-bold">{{trans('text.blog-index-title')}}</h1>
                <span class="sub-title">{{trans('text.blog-index-subtitle')}}</span>
            </div>
        </div>
    </div>
</section>
<div class="container py-4">
  <div class="row">
    <div class="col">
      <div class="blog-posts">
        <div class="row">
          @foreach ($posts as $post)
          <div class="col-md-4">
            @include('post.loop',['post' => $post])
          </div>
          @endforeach
        </div>
        <div class="row">
          <div class="col">
            {{ $posts->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection