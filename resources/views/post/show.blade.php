@extends('layouts.app')
@section('meta')
<title>{{ $post->title }} - {{ settings('app_name', 'News') }}</title>
<meta name="keywords" content="{{ $post->keywords }}">
<meta name="description" content="{{ $post->summary }}">
@endsection
@section('content')
<section
    class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-dark overlay-show overlay-op-7"
    style="background-image: url( {{ asset('img/page-header-blog.jpg') }});">
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12 align-self-center p-static order-2 text-center">
                <h1 class="text-9 font-weight-bold">{{ $post->title }}</h1>
            </div>
        </div>
    </div>
</section>
<div class="container py-4">
    <div class="row">
        <div class="col">
            <div class="blog-posts single-post">
                <article class="post post-large blog-single-post border-0 m-0 p-0">
                    <div class="post-image ml-0">
                            <img src="{{ getThumbnail($post->images, 'post_images', 'lg') }}"
                                class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0" alt="" />
                    </div>
                    <div class="post-date ml-0">
                        <span class="day">{{ $post->created_at->formatLocalized('%d') }}</span>
                        <span class="month">{{ $post->created_at->formatLocalized('%B') }}</span>
                    </div>
                    <div class="post-content ml-0">
                        <h2 class="font-weight-bold">{{ $post->title }}</h2>
                        <div class="post-meta">
                            <span><i class="far fa-user"></i> {{ trans('text.by') }} {{ $post->authorName() }} </span>
                            <span><i class="far fa-folder"></i> {{ $post->getCategoryName() }}</span>
                        </div>
                        {!! $post->body !!}
                        <div class="post-block mt-5 post-share">
                            <h4 class="mb-3">{{ trans('text.share-this') }}</h4>
                            <div class="addthis_toolbox addthis_default_style ">
                                <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                                <a class="addthis_button_tweet"></a>
                                <a class="addthis_button_pinterest_pinit"></a>
                                <a class="addthis_counter addthis_pill_style"></a>
                            </div>
                            <script 
                                type="text/javascript"
                                src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50faf75173aadc53">
                            </script>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</div>
@endsection