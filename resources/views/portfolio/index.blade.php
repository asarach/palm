@extends('layouts.app')
@section('meta')
<title>{{trans('text.portfolio-index-title')}} - {{ settings('app_name', 'News') }}</title>
<meta name="keywords" content="{{ settings('main_keywords', 'News') }}">
<meta name="description" content="{{ settings('main_description', 'News') }}">
@endsection
@section('content')
<section
  class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-dark overlay-show overlay-op-7"
  style="background-image: url({{asset('img/page-header-portfolio.jpg')}});">
  <div class="container">
    <div class="row mt-5">
      <div class="col-md-12 align-self-center p-static order-2 text-center">
        <h1 class="text-9 font-weight-bold">{{trans('text.portfolio-index-title')}}</h1>
        <span class="sub-title">{{trans('text.portfolio-index-subtitle')}}</span>
      </div>
    </div>
  </div>
</section>
<div class="container py-2">
  <ul class="nav nav-pills sort-source sort-source-style-3 justify-content-center" data-sort-id="portfolio"
    data-option-key="filter" data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*'}">
    <li class="nav-item active" data-option-value="*">
      <a class="nav-link text-1 text-uppercase active" href="#">
        {{trans('text.portfolio-show-all')}}
      </a>
    </li>
    @foreach ($cats as $cat)
    <li class="nav-item" data-option-value=".cat_{{ $cat->id }}">
      <a class="nav-link text-1 text-uppercase" href="#">
        {{ $cat->name }}
      </a>
    </li>
    @endforeach
  </ul>

  <div class="sort-destination-loader sort-destination-loader-showing mt-4 pt-2">
    <div class="row portfolio-list sort-destination" data-sort-id="portfolio">

      @foreach ($posts as $post)
      <div class="col-md-6 col-lg-4 isotope-item cat_{{ $post->category_id }}">
        @include('portfolio.loop',['post' => $post])
      </div>
      @endforeach
    </div>
  </div>
</div>
@endsection