@extends('layouts.app')
@section('meta')
<title>{{ $post->title }} - {{ settings('app_name', 'News') }}</title>
<meta name="keywords" content="{{ settings('main_keywords', 'News') }}">
<meta name="description" content="{{ settings('main_description', 'News') }}">
@endsection
@section('content')
<section
  class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-dark overlay-show overlay-op-7"
  style="background-image: url({{ asset('img/page-header-portfolio.jpg') }});">
  <div class="container">
    <div class="row mt-5">
      <div class="col">
        <div class="col-md-12 align-self-center p-static order-2 text-center">
          <h1 class="text-9 font-weight-bold">{{ $post->title }}</h1>
          <span class="sub-title">{{ $post->getCategoryName() }}</span>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="container pt-2 pb-4">
  <div class="row pb-4 mb-2">
    <div class="col-md-6 mb-4 mb-md-0 appear-animation" data-appear-animation="fadeInLeftShorter"
      data-appear-animation-delay="300">
      <div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-dark mt-3"
        data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}">
        @foreach ($post->images as $image)
        <div>
          <div class="img-thumbnail border-0 border-radius-0 p-0 d-block">
            <img src="{{ $image->getImage('pj') }}" class="img-fluid border-radius-0" alt="">
          </div>
        </div>
        @endforeach
      </div>
    </div>
    <div class="col-md-6">
      <div class="overflow-hidden">
        <h2 class="text-color-dark font-weight-normal text-4 mb-0 appear-animation" data-appear-animation="maskUp"
          data-appear-animation-delay="600">{{ trans('text.description') }}
          <strong class="font-weight-extra-bold">{{ trans('text.project') }}</strong>
        </h2>
      </div>
      <p class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="800">
        {!! $post->body !!}
      </p>
      <div class="overflow-hidden  mt-4">
        <h2 class="text-color-dark font-weight-normal text-4 mb-0 appear-animation" data-appear-animation="maskUp"
          data-appear-animation-delay="1000">{{ trans('text.details') }}
          <strong class="font-weight-extra-bold">{{ trans('text.project') }}</strong>
        </h2>
      </div>
      <ul class="list list-icons list-primary list-borders text-2 appear-animation"
        data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1200">
        <li>
          <i class="fas fa-caret-left right-10"></i>
          <strong class="text-color-primary">{{ trans('text.client') }}:</strong> {{ $post->client }}</li>
        <li>
          <i class="fas fa-caret-left right-10"></i>
          <strong class="text-color-primary">{{ trans('text.date') }}:</strong> {{ $post->date }}</li>
        <li>
          <i class="fas fa-caret-left right-10"></i>
          <strong class="text-color-primary">{{ trans('text.skills') }}:</strong>
          @foreach ($post->getSkills() as $skill)
          <span class="badge badge-dark badge-sm badge-pill px-2 py-1 ml-1">{{ $skill }}</span>
          @endforeach
        </li>
        <li>
          <i class="fas fa-caret-left right-10"></i>
          <strong class="text-color-primary">{{ trans('text.project-url') }}:</strong>
          {{ $post->url }}
        </li>
      </ul>
    </div>
  </div>
</div>
@endsection