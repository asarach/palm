<div class="portfolio-item">
  <a href="{{ $post->getLink() }}">
    <span class="thumb-info thumb-info-lighten border-radius-0">
      <span class="thumb-info-wrapper border-radius-0">
        <img src="{{ getThumbnail($post->images, 'post_images', 'pf') }}" class="img-fluid border-radius-0" alt="">
        <span class="thumb-info-title">
          <span class="thumb-info-inner">{{ $post->title }}</span>
          <span class="thumb-info-type">{{ $post->getCategoryName() }}</span>
        </span>
        <span class="thumb-info-action">
          <span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
        </span>
      </span>
    </span>
  </a>
</div>