var routes_base = window.App.urlBase + 'palmcp/';
//var routes_base = window.App.urlBase + ':lang/admin/';//multi-language
module.exports = [{
    path: routes_base ,
    component: require('./components/Index.vue').default
},


//posts
{
    path: routes_base + 'posts',
    component: require('./components/post/Index.vue').default
},
{
    path: routes_base + 'post/edit/:id',
    component: require('./components/post/Edit.vue').default
},

{
    path: routes_base + 'post/create',
    component: require('./components/post/Create.vue').default
},

//categories
{
    path: routes_base + 'categories',
    component: require('./components/category/Index.vue').default
},
{
    path: routes_base + 'category/:id',
    component: require('./components/category/Index.vue').default
},
{
    path: routes_base + 'category/edit/:id',
    component: require('./components/category/Edit.vue').default
},

//pubs
{
    path: routes_base + 'pubs',
    component: require('./components/pub/Index.vue').default
},
{
    path: routes_base + 'pub/edit/:id',
    component: require('./components/pub/Edit.vue').default
},

//comment
{
    path: routes_base + 'comments',
    component: require('./components/comment/Index.vue').default
},

//Language
{
    path: routes_base + 'languages',
    component: require('./components/language/Index.vue').default
},
{
    path: routes_base + 'language/edit/:id',
    component: require('./components/language/Edit.vue').default
},

//Menu
{
    path: routes_base + 'menus',
    component: require('./components/menu/Index.vue').default
},

//links
{
    path: routes_base + 'link/edit/:id',
    component: require('./components/menu/Edit.vue').default
},

//Medias
{
    path: routes_base + 'medias',
    component: require('./components/media/Index.vue').default
},

//Message
{
    path: routes_base + 'messages',
    component: require('./components/message/Index.vue').default
},
{
    path: routes_base + 'message/:id',
    component: require('./components/message/Show.vue').default
},

//Page
{
    path: routes_base + 'pages',
    component: require('./components/page/Index.vue').default
},
{
    path: routes_base + 'page/create',
    component: require('./components/page/Create.vue').default
},
{
    path: routes_base + 'page/edit/:id',
    component: require('./components/page/Edit.vue').default
},

//Poll
{
    path: routes_base + 'polls',
    component: require('./components/poll/Index.vue').default
},
{
    path: routes_base + 'poll/create',
    component: require('./components/poll/Create.vue').default
},
{
    path: routes_base + 'poll/:id',
    component: require('./components/poll/Show.vue').default
},
{
    path: routes_base + 'poll/edit/:id',
    component: require('./components/poll/Edit.vue').default
},

//Setting
{
    path: routes_base + 'settings',
    component: require('./components/setting/Index.vue').default
},

//Translation
{
    path: routes_base + 'translations',
    component: require('./components/translation/Index.vue').default
},

//User
{
    path: routes_base + 'users',
    component: require('./components/user/Index.vue').default
},
{
    path: routes_base + 'user/edit/:id',
    component: require('./components/user/Edit.vue').default
},
{
    path: routes_base + 'user/create',
    component: require('./components/user/Create.vue').default
},


];
