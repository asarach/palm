/**
* First we will load all of this project's JavaScript dependencies which
* includes Vue and other libraries. It is a great starting point when
* building robust, powerful web applications using Vue and Laravel.
*/

require('./bootstrap');
require('./main');
require('./icons');




window.Vue = require('vue');
window.Event = new Vue();
Vue.prototype.trans = (string, args) => {
 let value = _.get(window.trans, string, string);
 _.eachRight(args, (paramVal, paramKey) => {
   value = _.replace(value, `:${paramKey}`, paramVal);
 });
 return value;
};

import Vuex from 'vuex'
Vue.use(Vuex)

const store = new Vuex.Store({
 state: {
   current_language : 'ar',
   current_menu: {
     name: '',
     id: '',
     location: '',
   },
 }
})

Vue.prototype.Auth = window.App.user
Vue.prototype.appName = window.App.name
Vue.prototype.prefix = window.App.urlBase  +'palmcp/';
//Vue.prototype.prefix = window.App.urlBase +  store.state.current_language +'/palmcp/';//multi-language
Vue.prototype.ajax_prefix = window.App.urlBase +'palmcp/ajax/';
//Vue.prototype.ajax_prefix = window.App.urlBase +  store.state.current_language +'/palmcp/ajax/';//multi-language
Vue.prototype.urlBase = window.App.urlBase;
Vue.prototype.appUrl = window.App.appUrl;

var moment = require("moment");


Vue.filter("formatDate", function (value) {
  if (!value) return ""
  return moment(value, "YYYY-MM-DD").locale("fr").format("L");
})


/*** tirth party component ***/

import VueRouter from 'vue-router'
Vue.use(VueRouter)



import vueHeadful from 'vue-headful';
Vue.component('vue-headful', vueHeadful)

import Datetime from 'vue-datetime'
import 'vue-datetime/dist/vue-datetime.css'
Vue.use(Datetime)

import Multiselect from 'vue-multiselect'
import 'vue-multiselect/dist/vue-multiselect.min.css'
Vue.component('multiselect', Multiselect)

import Tooltip from 'vue-directive-tooltip';
import 'vue-directive-tooltip/dist/vueDirectiveTooltip.css';
Vue.use(Tooltip);

import VueClipboard from 'vue-clipboard2'
Vue.use(VueClipboard)

import { Settings } from 'luxon'
Settings.defaultLocale = 'ar'


/*** Local component ***/

Vue.component('loading-spinner', require('./components/partials/LoadingSpinner.vue').default);
Vue.component('loading-error', require('./components/partials/LoadingError.vue').default);
Vue.component('notifications', require('./components/partials/Notifications.vue').default);
Vue.component('confirmation', require('./components/partials/Confirmation.vue').default);
Vue.component('filtering', require('./components/partials/Filtering.vue').default);
Vue.component('pagination', require('./components/partials/Pagination.vue').default);
Vue.component('upload-image', require('./components/partials/UploadImage.vue').default);
Vue.component('upload-images', require('./components/partials/UploadImages.vue').default);
Vue.component('upload-file', require('./components/partials/UploadFile.vue').default);
Vue.component('upload-files', require('./components/partials/UploadFiles.vue').default);

Vue.component('post-form', require('./components/post/Form.vue').default);

Vue.component('category-form', require('./components/category/Form.vue').default);
Vue.component('pub-form', require('./components/pub/Form.vue').default);
Vue.component('language-form', require('./components/language/Form.vue').default);
Vue.component('link-form', require('./components/menu/Form.vue').default);
Vue.component('page-form', require('./components/page/Form.vue').default);
Vue.component('poll-form', require('./components/poll/Form.vue').default);
Vue.component('user-form', require('./components/user/Form.vue').default);
Vue.component('upload-avatar', require('./components/user/UploadAvatar.vue').default);


import routes from './routes'

/*** Local component ***/
const router = new VueRouter({
 mode: 'history',
 linkActiveClass: 'active',
 linkExactActiveClass: 'exact-active',
 routes: routes
})
router.afterEach((to, from) => {
 $('.tip').tooltip('hide');
})

/*** Vuex ***/


const app = new Vue({
 router,
 el: '#app',
 store: store,
});
