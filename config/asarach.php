<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Upload Settings
    |--------------------------------------------------------------------------
    */
    "upload_folders" => [
        
        "post_images" => [
            "full" => [
                "folder" => "post_images/full/",
                "size" => []
            ],
            "lg" => [
                "folder" => "post_images/lg/",
                "size" => [820, 615]
            ],
            "md" => [
                "folder" => "post_images/md/",
                "size" => [460, 345]
            ],
            "sm" => [
                "folder" => "post_images/sm/",
                "size" => [320, 240]
            ],
            "xs" => [
                "folder" => "post_images/xs/",
                "size" => [120, 90]
            ],
            "ts" => [
                "folder" => "post_images/ts/",
                "size" => [120, 120]
            ],
            "pj" => [
                "folder" => "post_images/pj/",
                "size" => [560, 315]
            ]
        ],

        "cat_thumb" => [
            "lg" => [
                "folder" => "cat_thumb/lg/",
                "size" => [820, 615]
            ],
            "md" => [
                "folder" => "cat_thumb/md/",
                "size" => [460, 345]
            ],
            "sm" => [
                "folder" => "cat_thumb/sm/",
                "size" => [320, 240]
            ],
            "xs" => [
                "folder" => "cat_thumb/xs/",
                "size" => [120, 90]
            ]
        ],

        "pub_image" => [
            "lg" => [
                "folder" => "pub_image/lg/",
                "size" => [820, 615]
            ],
            "md" => [
                "folder" => "pub_image/md/",
                "size" => [460, 345]
            ],
            "sm" => [
                "folder" => "pub_image/sm/",
                "size" => [320, 240]
            ],
            "xs" => [
                "folder" => "pub_image/xs/",
                "size" => [120, 90]
            ]
        ],

        "avatar" => [
            "lg" => [
                "folder" => "avatar/lg/",
                "size" => [820, 615]
            ],
            "md" => [
                "folder" => "avatar/md/",
                "size" => [460, 345]
            ],
            "sm" => [
                "folder" => "avatar/sm/",
                "size" => [320, 240]
            ],
            "xs" => [
                "folder" => "avatar/xs/",
                "size" => [120, 90]
            ]
        ],

        "images" => [
            "lg" => [
                "folder" => "images/lg/",
                "size" => [820, 615]
            ],
            "md" => [
                "folder" => "images/md/",
                "size" => [460, 345]
            ],
            "sm" => [
                "folder" => "images/sm/",
                "size" => [320, 240]
            ],
            "xs" => [
                "folder" => "images/xs/",
                "size" => [120, 90]
            ]
        ],

    ],

    "page_views" => [
        "simple" => "Simple",
        "about"  => "About"
    ],
    
    "menu_locations" => [
        "main" => "Main",
        "footer-links"  => "Footer Links",
        "partners-links"  => "Partners Links"
    ],

    "countries" => [
        "sau" => "saudi-arabia",
        "egy" => "egypt",
    ],

    "version" => "1.1.01",
    "version_lang" => "1.1",
    "testimonial_cat" => "2",
    "project_cat" => "1",
    "blog_cat" => "3",

];
