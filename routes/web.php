<?php

use Illuminate\Support\Facades\Route;
use Spatie\Honeypot\ProtectAgainstSpam;
/* index  */

Route::get("/", array("as" => "index", "uses" => "HomeController@index"));

/* search  */
Route::get("/search", array("as" => "search", "uses" => "SearchController@show"));


/*Post*/
Route::get("/blog", array("as" => "blog", "uses" => "PostController@index"));
Route::get("/post/{slug}", array("as" => "blog.show", "uses" => "PostController@show"));
/*Post*/
Route::get("/portfolio", array("as" => "portfolio", "uses" => "PostController@portfolioIndex"));
Route::get("/portfolio/{slug}", array("as" => "portfolio.show", "uses" => "PostController@portfolioShow"));


/* Page  */
Route::get("/about", array("as" => "about", "uses" => "PageController@about"));
Route::get("/faq", array("as" => "faq", "uses" => "PageController@faq"));
Route::get("/services", array("as" => "services", "uses" => "PageController@services"));
Route::get("/sitemap", array("as" => "sitemap", "uses" => "PageController@sitemap"));
Route::get("/contact", array("as" => "contact", "uses" => "PageController@contact"));
Route::post("/contact", array("as" => "store.contact", "uses" => "PageController@storeMessage"))->middleware(ProtectAgainstSpam::class);


Route::middleware(["auth:sanctum", "verified"])->get("/dashboard", function () {
    return view("dashboard");
})->name("dashboard");
