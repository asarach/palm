<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Message;
use Spatie\Permission\Models\Role;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminMessageTest extends TestCase
{
    use DatabaseTransactions;
    public function testAdminMessageIndex()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $message = Message::factory()->create();
        if (!isset($_SERVER["QUERY_STRING"])) {
            $_SERVER["QUERY_STRING"] = "";
        }

        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/messages");

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "messages" => [
                    "current_page",
                    "first_page_url",
                    "from",
                    "last_page",
                    "last_page_url",
                    "links",
                    "next_page_url",
                    "path",
                    "per_page",
                    "prev_page_url",
                    "to",
                    "total",
                    "links",
                    "data" => [
                        "*" => ["id", "name", "email", "subject", "viewed"],
                    ],
                ],
            ]);
    }

    public function testAdminMessageShow()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $message = Message::factory()->create();

        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/message/" . $message->id);

        //testDumpError($response);

        $response
            ->assertStatus(200)

            ->assertJsonStructure([
                "message" => [
                    "body",
                    "created_at",
                    "email",
                    "id",
                    "name",
                    "subject",
                    "updated_at",
                    "viewed"
                ],
            ]);
    }

    public function testAdminMessageDestroy()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $message = Message::factory()->create();


        $response = $this->actingAs($user)
            ->json("DELETE", "/admin/ajax/message/" . $message->id);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "result" => true
            ]);
    }

    public function testAdminMessageChangeViewed()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $message = Message::factory()->create();


        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/message/" . $message->id . "/viewed/0");

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "viewed" => "0"
            ]);
    }

    public function testAdminMessageActions()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $message = Message::factory()->create();
        $message2 = Message::factory()->create();

        //TEST ACTIVATE

        $data =  [
            "action" => "viewed",
            "items" => [$message->id  => true, $message2->id  => true]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/message/actions", $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    ["id" => $message->id, "viewed" => 1],
                    ["id" => $message2->id, "viewed" => 1]
                ]
            ]);

        //TEST DEACTIVATE

        $data =  [
            "action" => "unviewed",
            "items" => [$message->id  => true, $message2->id  => true]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/message/actions", $data);


        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    ["id" => $message->id, "viewed" => 0],
                    ["id" => $message2->id, "viewed" => 0]
                ]
            ]);

        //TEST DELETE

        $data =  [
            "action" => "delete",
            "items" => [$message->id  => true, $message2->id  => true]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/message/actions", $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    $message->id,
                    $message2->id
                ]
            ]);
    }
}
