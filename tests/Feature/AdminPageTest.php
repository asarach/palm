<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Page;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class AdminPageTest extends TestCase
{
    use DatabaseTransactions;
    public function testAdminPageIndex()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $page = Page::factory()->create();

        if (!isset($_SERVER["QUERY_STRING"])) {
            $_SERVER["QUERY_STRING"] = "";
        }

        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/pages");

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "languages" => [
                    "*" => ["id", "name"],
                ],
                "pages" => [
                    "current_page",
                    "first_page_url",
                    "from",
                    "last_page",
                    "last_page_url",
                    "links",
                    "next_page_url",
                    "path",
                    "per_page",
                    "prev_page_url",
                    "to",
                    "total",
                    "links",
                    "data" => [
                        "*" => ["id", "content", "created_at", "deleted_at", "description", "keywords", "name", "slug", "status", "title", "updated_at"],
                    ],
                ],
            ]);
    }

    public function testAdminPageCreate()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/page/create");

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "languages" => [
                    "*" => ["id", "name"],
                ],
            ]);
    }

    public function testAdminPageStore()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        //$language = Language::first();

        //craete page
        $data = [
            "name" => "Lorem",
            "slug" => "slug",
            "title" => "title",
            "content" => "content",
            "description" => "description",
            "keywords" => "keywords",
            "status" => 1,
        ];

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/page", $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "page" => [
                    "name",
                    "slug",
                    "title",
                    "status",
                    "content",
                    "description",
                    "keywords",
                    "id"
                ]
            ]);
    }

    public function testAdminPageEdit()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        //craete page
        $data = [
            "name" => "Lorem",
            "slug" => "slug",
            "title" => "title",
            "content" => "content",
            "description" => "description",
            "keywords" => "keywords",
            "status" => 1,
        ];

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/page", $data);
        $page = $response->decodeResponseJson()["page"];

        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/page/edit/" . $page["id"]);

        //testDumpError($response);
        
        $response
            ->assertStatus(200)
            ->assertJson([
                "page" => [
                    "id" => $page["id"],
                    "name" => $page["name"],
                    "slug" => $page["slug"],
                    "title" => $page["title"],
                    "status" => $page["status"],
                    "content" => $page["content"],
                    "description" => $page["description"] ,
                    "keywords" => $page["keywords"],
                ]
            ]);
            
    }

    public function testAdminPageUpdate()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $page = Page::factory()->create();

        $data = [
            "name" => $page->name . "2",
            "slug" => $page->slug . "2",
            "title" => $page->title . "2",
            "status" => 0,
            "content" => $page->content . "2",
            "description" => $page->description . "2",
            "keywords" => $page->keywords . "2",
        ];


        $response = $this->actingAs($user)
            ->json("PUT", "/admin/ajax/page/" . $page->id, $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "page" => [
                    "id" => $page->id,
                    "name" => $page->name . "2",
                    "slug" => $page->slug . "2",
                    "title" => $page->title . "2",
                    "status" => 0,
                    "description" => $page->description . "2",
                    "keywords" => $page->keywords . "2",
                    "content" => $page->content . "2",
                    "deleted_at" => null,
                ]
            ]);
    }

    public function testAdminPageDestroy()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $page = Page::factory()->create();


        $response = $this->actingAs($user)
            ->json("DELETE", "/admin/ajax/page/" . $page->id);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "result" => true
            ]);
    }

    public function testAdminPageRestore()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $page = Page::factory()->create();

        $page->delete();

        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/page/restore/" . $page->id);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "result" => true
            ]);
    }

    public function testAdminPageChangeStatus()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $page = Page::factory()->create();


        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/page/".$page->id."/status/0" );

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "status" => "0"
            ]);
    }

    public function testAdminPageActions()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $page = Page::factory()->create();
        $page2 = Page::factory()->create();

        //TEST ACTIVATE

        $data =  [
            "action" => "activate",
            "items" => [$page->id  => true, $page2->id  => true ]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/page/actions", $data );

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    ["id" => $page->id, "status" => 1 ],
                    ["id" => $page2->id, "status" => 1 ]
                ]
            ]);

        //TEST DEACTIVATE

        $data =  [
            "action" => "deactivate",
            "items" => [$page->id  => true, $page2->id  => true ]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/page/actions", $data );


        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    ["id" => $page->id, "status" => 0],
                    ["id" => $page2->id, "status" => 0 ]
                ]
            ]);

        //TEST DELETE

        $data =  [
            "action" => "delete",
            "items" => [$page->id  => true, $page2->id  => true ]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/page/actions", $data );

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    $page->id,
                    $page2->id
                ]
            ]);
    }
}
