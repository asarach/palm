<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Post;
use App\Models\User;
use App\Models\Media;
use App\Models\Category;
use Spatie\Permission\Models\Role;
use Waavi\Translation\Models\Language;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminPostest extends TestCase
{
    use DatabaseTransactions;
    public function testAdminPostIndex()
{
    //create user
    $user = User::factory()->create();
    $user = User::find($user->id);
    $role = Role::where("name", "super-admin")->first();

    $user->assignRole($role);

    $post = $this->createPost();

    if (!isset($_SERVER["QUERY_STRING"])) {
        $_SERVER["QUERY_STRING"] = "";
    }

    $response = $this->actingAs($user)
        ->json("GET", "/admin/ajax/posts");

    //testDumpError($response);

    $response
        ->assertStatus(200)
        ->assertJsonStructure([
            "categories" => [
            "*" => ["id", "name"],
        ],
        
            "posts" => [
                "current_page",
                "first_page_url",
                "from",
                "last_page",
                "last_page_url",
                "links",
                "next_page_url",
                "path",
                "per_page",
                "prev_page_url",
                "to",
                "total",
                "links",
                "data" => [
                    "*" => [
                        "id", "title", "slug", "body", "excerpt", "keywords", "video_link", "long", "lati", "comment_count", "status", "promoted",  "sticky", "comment_status",  "end_date", "start_date",  "video",  "videos",  "language_id", "author_id", "deleted_at", "created_at", "updated_at",
                        "language" => ["name", "id"],
                        "author" => ["name", "id"],
                        "categories" => ["*" => ["id", "name"]],
                    ],
                ],
            ],
        ]);
}

public function testAdminPostCreate()
{
    //create user
    $user = User::factory()->create();
    $user = User::find($user->id);
    $role = Role::where("name", "super-admin")->first();

    $user->assignRole($role);

    $response = $this->actingAs($user)
        ->json("GET", "/admin/ajax/post/create");

    //testDumpError($response);

    $response
        ->assertStatus(200)
        ->assertJsonStructure([
            "categories" => [
                "*" => ["id", "name"],
            ],
            "languages" => [
                "*" => ["id", "name"],
            ],
        ]);
}

public function testAdminPostStore()
{
    //create user
    $user = User::factory()->create();
    $user = User::find($user->id);
    $role = Role::where("name", "super-admin")->first();

    $user->assignRole($role);

    $language = Language::first();
    $category = Category::first();
    $thumb = Media::inRandomOrder()->first();
    $images = Media::inRandomOrder()->first();
    $pdf = Media::inRandomOrder()->first();
    $documents = Media::inRandomOrder()->first();

    //craete post
        $data = [
            "title" => "title",
            "body" => "body",
            "excerpt" => "description",
            "keywords" => "description",
            "video_link" => "http://test.test",
            "long" => 123,
            "lati" => 123,
            "video" => "video",
            "end_date" => "2020-11-25T21:41:44.000000Z",
            "start_date" => "2020-11-25T21:41:44.000000Z",
            "status" => 1,
            "sticky" => 1,
            "promoted" => 1,
            "comment_status" => 1,
            "videos" => ["asa", "ww"],
            "language" => ["id" => $language->id],
            "categories" => [["id" => $category->id]],
            "thumb" => $thumb->id,
            "images_upload" => [["val" => $images->id]],
            "pdf" => $pdf->id,
            "documents_upload" => [["val" => $documents->id]],
        ];

    $response = $this->actingAs($user)
        ->json("POST", "/admin/ajax/post", $data);

    //testDumpError($response);

    $response
        ->assertStatus(200)
        ->assertJsonStructure([
            "post" => [
                "title",
                "slug",
                "body",
                "excerpt",
                "keywords",
                "lati",
                "long",
                "comment_status",
                "status",
                "sticky",
                "promoted",
                "start_date",
                "end_date",
                "author_id",
                "language_id",
                "created_at",
                "updated_at",
                "video",
                "video_link",
                "videos",
                "id"
            ]
        ]);
}

public function btestAdminPostShow()
{
    //create user
    $user = User::factory()->create();
    $user = User::find($user->id);
    $role = Role::where("name", "super-admin")->first();

    $user->assignRole($role);

    $post = $this->createPost();

    $response = $this->actingAs($user)
        ->json("GET", "/admin/ajax/post/" . $post->id);

    //testDumpError($response);

    $response
        ->assertStatus(200)

        ->assertJsonStructure([
            "post" => [
                "title",
                "slug",
                "content",
                "excerpt",
                "status",
                "sticky",
                "end_date",
                "user_id",
                "updated_at",
                "created_at",
                "id"
            ],
        ]);
}

public function testAdminPostEdit()
{
    //create user
    $user = User::factory()->create();
    $user = User::find($user->id);
    $role = Role::where("name", "super-admin")->first();

    $user->assignRole($role);

    $language = Language::first();
    $category = Category::first();
    $thumb = Media::inRandomOrder()->first();
    $images = Media::inRandomOrder()->first();
    $pdf = Media::inRandomOrder()->first();
    $documents = Media::inRandomOrder()->first();

    //craete post
    $data = [
        "title" => "title",
        "body" => "body",
        "excerpt" => "description",
        "keywords" => "description",
        "video_link" => "http://test.test",
        "long" => 123,
        "lati" => 123,
        "video" => "video",
        "end_date" => "2020-11-25T21:41:44.000000Z",
        "start_date" => "2020-11-25T21:41:44.000000Z",
        "status" => 1,
        "sticky" => 1,
        "promoted" => 1,
        "comment_status" => 1,
        "videos" => ["asa", "ww"],
        "language" => ["id" => $language->id],
        "categories" => [["id" => $category->id]],
        "thumb" => $thumb->id,
        "images_upload" => [["val" => $images->id]],
        "pdf" => $pdf->id,
        "documents_upload" => [["val" => $documents->id]],
    ];

    $response = $this->actingAs($user)
        ->json("POST", "/admin/ajax/post", $data);
    $post = $response->decodeResponseJson()["post"];

    $response = $this->actingAs($user)
        ->json("GET", "/admin/ajax/post/edit/" . $post["id"]);

    //testDumpError($response);

    $response
        ->assertStatus(200)
        ->assertJsonStructure([
            "categories" => [
                "*" => ["id", "name"],
            ],
            "languages" => [
                "*" => ["id", "name"],
            ],
        ])
        ->assertJson([
            "post" => [
                // "id" => $card["id"],
                "title" => $card["title"],
                "slug" => $card["slug"],
                "body" => $card["body"],
                "excerpt" => $card["excerpt"],
                "keywords" => $card["keywords"],
                "video_link" => $card["video_link"],
                "long" => $card["long"],
                "lati" => $card["lati"],
                "status" => $card["status"],
                "promoted" => $card["promoted"],
                "sticky" => $card["sticky"],
                "comment_status" => $card["comment_status"],
                //"end_date" => $card["end_date"],
                //"start_date" => $card["start_date"],
                "video" => $card["video"],
                "videos" => ["asa", "ww"],
                //"comment_count" => $card["comment_count"],
                "created_at" => $card["created_at"],
                //"deleted_at" => $card["deleted_at"],
                "updated_at" => $card["updated_at"],

                "language" => ["id" => $language->id, "name" => $language->name],
                "language_id" => $language->id,

                "author" => ["id" => $user->id, "name" => $user->name],
                "author_id" => $user->id,

                "categories" => [["id" => $category->id, "name" =>  $category->name]],
            ]
        ]);
}

public function testAdminPostUpdate()
{
    $user = User::factory()->create();
    $user = User::find($user->id);
    $role = Role::where("name", "super-admin")->first();

    $user->assignRole($role);

    $language = Language::first();
    $category = Category::first();
    $thumb = Media::inRandomOrder()->first();
    $images = Media::inRandomOrder()->first();
    $pdf = Media::inRandomOrder()->first();
    $documents = Media::inRandomOrder()->first();

    $post = $this->createPost();
    

    $data = [
        "title" => $card->title . "2",
        "slug" => $card->titlsluge . "2",
        "body" => $card->body . "2",
        "excerpt" => $card->excerpt . "2",
        "keywords" => $card->keywords . "2",
        "video_link" => $card->video_link . "2",
        "long" => 1232,
        "lati" => 1232,
        "video" => $card->video . "2",
        "status" => 0,
        "sticky" => 0,
        "promoted" => 0,
        "end_date" => "2020-11-25T21:41:44.000000Z",
        "start_date" => "2020-11-25T21:41:44.000000Z",
        "comment_status" => 0,
        "videos" => ["asa2", "ww2"],
        "language" => ["id" => $language->id],
        "categories" => [["id" => $category->id]],
        "thumb" => $thumb->id,
        "images_upload" => [["val" => $images->id]],
        "pdf" => $pdf->id,
        "documents_upload" => [["val" => $documents->id]],
    ];


    $response = $this->actingAs($user)
        ->json("PUT", "/admin/ajax/post/" . $post->id, $data);

    //testDumpError($response);

    $response
        ->assertStatus(200)
        ->assertJson([
            "post" => [
                "id" => $card->id,
                "title" => $card->title . "2",
                "keywords" => $card->keywords . "2",
                "lati" => 1232,
                "long" => 1232,
                "body" => $card->body . "2",
                "comment_status" => 0,
                "status" => 0,
                "sticky" => 0,
                "promoted" => 0,
                "language_id" => $language->id,
                "video" => $card->video . "2",
                "video_link" => $card->video_link . "2",
            ]
        ]);
}

public function testAdminPostDestroy()
{
    $user = User::factory()->create();
    $user = User::find($user->id);
    $role = Role::where("name", "super-admin")->first();

    $user->assignRole($role);

    $post = $this->createPost();


    $response = $this->actingAs($user)
        ->json("DELETE", "/admin/ajax/post/" . $post->id);

    //testDumpError($response);

    $response
        ->assertStatus(200)
        ->assertJson([
            "result" => true
        ]);
}

public function testAdminPostRestore()
{
    $user = User::factory()->create();
    $user = User::find($user->id);
    $role = Role::where("name", "super-admin")->first();

    $user->assignRole($role);

    $post = $this->createPost();

    $post->delete();

    $response = $this->actingAs($user)
        ->json("GET", "/admin/ajax/post/restore/" . $post->id);

    //testDumpError($response);

    $response
        ->assertStatus(200)
        ->assertJson([
            "result" => true
        ]);
}

public function testAdminPostChangeStatus()
{
    $user = User::factory()->create();
    $user = User::find($user->id);
    $role = Role::where("name", "super-admin")->first();

    $user->assignRole($role);

    $post = $this->createPost();


    $response = $this->actingAs($user)
        ->json("GET", "/admin/ajax/post/" . $post->id . "/status/0");

    //testDumpError($response);

    $response
        ->assertStatus(200)
        ->assertJson([
            "status" => "0"
        ]);
}

public function testAdminPostChangeSticky()
{
    $user = User::factory()->create();
    $user = User::find($user->id);
    $role = Role::where("name", "super-admin")->first();

    $user->assignRole($role);

    $post = $this->createPost();


    $response = $this->actingAs($user)
        ->json("GET", "/admin/ajax/post/" . $post->id . "/sticky/0");

    //testDumpError($response);

    $response
        ->assertStatus(200)
        ->assertJson([
            "sticky" => "0"
        ]);
}

public function testAdminPostChangePromoted()
{
    $user = User::factory()->create();
    $user = User::find($user->id);
    $role = Role::where("name", "super-admin")->first();

    $user->assignRole($role);

    $post = $this->createPost();


    $response = $this->actingAs($user)
        ->json("GET", "/admin/ajax/post/" . $post->id . "/promoted/0");

    //testDumpError($response);

    $response
        ->assertStatus(200)
        ->assertJson([
            "promoted" => "0"
        ]);
}

public function btestAdminPostDuplicate()
{
    $user = User::factory()->create();
    $user = User::find($user->id);
    $role = Role::where("name", "super-admin")->first();

    $user->assignRole($role);

    $post = $this->createPost();


    $response = $this->actingAs($user)
        ->json("GET", "/admin/ajax/post/duplicate/" . $post->id);

    //testDumpError($response);

    $response
        ->assertStatus(200)
        ->assertJson([
            "post" => [
                "title" => $post["title"],
                "content" => $post["content"],
                "excerpt" => $post["excerpt"],
                "sticky" => $post["sticky"],
            ]
        ]);
}

public function testAdminPostActions()
{
    $user = User::factory()->create();
    $user = User::find($user->id);
    $role = Role::where("name", "super-admin")->first();

    $user->assignRole($role);

    $post = $this->createPost();
    $post2 = $this->createPost();

    //TEST ACTIVATE

    $data =  [
        "action" => "status-active",
        "items" => [$post->id  => true, $post2->id  => true]
    ];

    $response = $this->actingAs($user)
        ->json("post", "/admin/ajax/post/actions", $data);

    //testDumpError($response);

    $response
        ->assertStatus(200)
        ->assertJson([
            "results" => [
                ["id" => $post->id, "status" => 1],
                ["id" => $post2->id, "status" => 1]
            ]
        ]);

    //TEST DEACTIVATE

    $data =  [
        "action" => "status-deactive",
        "items" => [$post->id  => true, $post2->id  => true]
    ];

    $response = $this->actingAs($user)
        ->json("post", "/admin/ajax/post/actions", $data);


    //testDumpError($response);

    $response
        ->assertStatus(200)
        ->assertJson([
            "results" => [
                ["id" => $post->id, "status" => 0],
                ["id" => $post2->id, "status" => 0]
            ]
        ]);

        //TEST sticky-yes

        $data =  [
            "action" => "sticky-yes",
            "items" => [$card->id  => true, $card2->id  => true]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/card/actions", $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    ["id" => $card->id, "sticky" => 1],
                    ["id" => $card2->id, "sticky" => 1]
                ]
            ]);

        //TEST sticky-no

        $data =  [
            "action" => "sticky-no",
            "items" => [$card->id  => true, $card2->id  => true]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/card/actions", $data);


        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    ["id" => $card->id, "sticky" => 0],
                    ["id" => $card2->id, "sticky" => 0]
                ]
            ]);

        //TEST sticky-yes

        $data =  [
            "action" => "promoted-yes",
            "items" => [$card->id  => true, $card2->id  => true]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/card/actions", $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    ["id" => $card->id, "promoted" => 1],
                    ["id" => $card2->id, "promoted" => 1]
                ]
            ]);

        //TEST sticky-no

        $data =  [
            "action" => "promoted-no",
            "items" => [$card->id  => true, $card2->id  => true]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/card/actions", $data);


        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    ["id" => $card->id, "promoted" => 0],
                    ["id" => $card2->id, "promoted" => 0]
                ]
            ]);


    //TEST DELETE

    $data =  [
        "action" => "delete",
        "items" => [$post->id  => true, $post2->id  => true]
    ];

    $response = $this->actingAs($user)
        ->json("post", "/admin/ajax/post/actions", $data);

    //testDumpError($response);

    $response
        ->assertStatus(200)
        ->assertJson([
            "results" => [
                $post->id,
                $post2->id
            ]
        ]);
}


    public function createCard()
    {
        $author = User::all()->random();
        $language = Language::all()->random();

        $card = Card::factory()->create([
            "language_id" =>  $language->id,
            "author_id" =>  $author->id,
        ]);

        return $card;
    }
}
