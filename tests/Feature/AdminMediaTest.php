<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Media;
use Illuminate\Http\UploadedFile;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminMediaTest extends TestCase
{
    use DatabaseTransactions;
    public function testAdminMediaIndex()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        Storage::fake("uploads");

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/medias/upload?type=cat_thumb", [
                "file" => UploadedFile::fake()->image("avatar.jpg")
            ]);

        Storage::disk("uploads")->assertExists("cat_thumb/lg/avatar.jpg");
        $media = $response->decodeResponseJson();


        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/medias");

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "folders",
                "medias",
            ]);


        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/medias?folder=cat_thumb");


        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "folders",
                "medias" => [
                    "current_page",
                    "first_page_url",
                    "from",
                    "last_page",
                    "last_page_url",
                    "links",
                    "next_page_url",
                    "path",
                    "per_page",
                    "prev_page_url",
                    "to",
                    "total",
                    "links",
                    "data" => [
                        "*" => ["id", "created_at", "src", "status", "title", "type", "updated_at", "uri"],
                    ],
                ],
            ]);
    }

    public function testAdminMediaUpdate()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $media = $this->createMedia();

        $data = [
            "id" => $media->id,
            "key" => 0,
            "title" => $media->title . "2",
        ];


        $response = $this->actingAs($user)
            ->json("PUT", "/admin/ajax/media/" . $media->id, $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "media" => [
                    "id" => $media->id,
                    "src" => $media->getImage(),
                    "uri" => $media->uri,
                    "status" => $media->status,
                    "title" => $media->title . "2",
                    "type" => $media->type
                ]
            ]);
    }

    public function testAdminMediaDestroy()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $media = $this->createMedia();

        $response = $this->actingAs($user)
            ->json("DELETE", "/admin/ajax/media/" . $media->id);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "cleared_count",
                "cleared_size"
            ]);
    }

    public function testAdminMediaClearFolder()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);


        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/medias/clear-medias/cat_thumb");

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "cleared_size",
                "cleared_count"
            ]);
    }
    public function testAdminMediaClearMedias()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);


        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/medias/clear-folder/cat_thumb");

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "cleared_size",
                "cleared_count"
            ]);
    }

    public function testAdminMediaUpload()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        //upload image
        Storage::fake("uploads");

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/medias/upload?type=cat_thumb", [
                "file" => UploadedFile::fake()->image("avatar.jpg")
            ]);

        $response_json = $response->decodeResponseJson();
        $media = Media::find($response_json["image_id"]);

        Storage::disk("uploads")->assertExists($media->type . "/lg/" . $media->uri);
        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "image_id",
                "success",
            ]);
    }


    public function testAdminMediaUploadFile()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        //upload image
        Storage::fake("uploads");

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/medias/upload-file?type=cat_thumb", [
                "file" => UploadedFile::fake()->image("avatar.jpg")
            ]);
        $media = $response->decodeResponseJson();
        $path = explode("uploads/", $media["file"]["file_path"]);
        Storage::disk("uploads")->assertExists($path[1]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "success",
                "file" => [
                    "file_id",
                    "file_path"
                ]

            ]);
    }


    public function testAdminMediaCkuploadImages()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        //upload image
        Storage::fake("uploads");

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/images/ckupload?type=image", [
                "upload" => UploadedFile::fake()->image("avatar.jpg")
            ]);
        //testDumpError($response);

        $media = $response->decodeResponseJson();
        Storage::disk("uploads")->assertExists(str_replace("/uploads/", "", $media["url"]));
        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "fileName",
                "uploaded",
                "url"
            ]);
    }

    public function createMedia(){

        $media = Media::factory()->create(["uri" => "default.pdf", "type" => "cat_thumb"]);

        return $media;

    }
}
