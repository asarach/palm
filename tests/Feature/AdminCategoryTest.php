<?php

namespace Tests\Feature;


use Tests\TestCase;
use App\Models\User;
use App\Models\Category;
use Illuminate\Http\UploadedFile;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class AdminCategoryTest extends TestCase
{
    use DatabaseTransactions;
    //use RefreshDatabase;
    public function testAdminCategoryIndex()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $category = Category::factory()->create();

        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/categories");

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                [
                    "id" => $category->id, "name" => $category->name,
                ]
            ])
            ->assertJsonFragment([
                [
                    "url" => "", "name" => "front.categories",
                ]
            ])
            ->assertJsonStructure([
                "all_categories" => [
                    "*" => ["id", "name"],
                ],
                "breadcrumbs" => [
                    "*" => ["url", "name"],
                ],
                "categories" => [
                    "current_page",
                    "first_page_url",
                    "from",
                    "last_page",
                    "last_page_url",
                    "links",
                    "next_page_url",
                    "path",
                    "per_page",
                    "prev_page_url",
                    "to",
                    "total",
                    "links",
                    "data" => [
                        "*" => ["id", "description", "keywords", "name", "order", "parent_id", "slug", "status"],
                    ],
                ],
            ]);
    }

    public function testAdminCategoryShow()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $category = Category::factory()->create();

        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/category/" . $category->id);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                [
                    "id" => $category->id, "name" => $category->name,
                ]
            ])
            ->assertJsonFragment([
                [
                    "url" => "categories", "name" => "front.categories",
                    "url" => "", "name" => $category->name,
                ]
            ])
            ->assertJsonStructure([
                "all_categories" => [
                    "*" => ["id", "name"],
                ],
                "breadcrumbs" => [
                    "*" => ["url", "name"],
                ],
                "categories" => [
                    "current_page",
                    "first_page_url",
                    "from",
                    "last_page",
                    "last_page_url",
                    "links",
                    "next_page_url",
                    "path",
                    "per_page",
                    "prev_page_url",
                    "to",
                    "total",
                    "links",
                    "data" => [
                        "*" => ["id", "description", "keywords", "name", "order", "parent_id", "slug", "status"],
                    ],
                ],
            ]);
    }

    public function testAdminCategoryStore()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        //upload image
        Storage::fake("uploads");

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/medias/upload?type=cat_thumb", [
                "file" => UploadedFile::fake()->image("avatar.jpg")
            ]);

        Storage::disk("uploads")->assertExists("cat_thumb/lg/avatar.jpg");
        $thumb = $response->decodeResponseJson();

        $parent = Category::factory()->create();

        //craete category
        $data = [
            "name" => "Lorem",
            "status" => 1,
            "order" => 1,
            "description" => "description",
            "keywords" => "keywords",
            "parent_id" => $parent->id,
            "thumb" =>  $thumb["image_id"],
        ];

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/category", $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "category" => [
                    "name",
                    "slug",
                    "order",
                    "status",
                    "description",
                    "keywords",
                    "id"
                ]
            ]);
    }

    public function testAdminCategoryEdit()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        //upload image
        Storage::fake("uploads");

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/medias/upload?type=cat_thumb", [
                "file" => UploadedFile::fake()->image("avatar.jpg")
            ]);

        Storage::disk("uploads")->assertExists("cat_thumb/lg/avatar.jpg");
        $thumb = $response->decodeResponseJson();

        $parent = Category::factory()->create();

        //craete category
        $data = [
            "name" => "Lorem",
            "status" => 1,
            "order" => 1,
            "description" => "description",
            "keywords" => "keywords",
            "parent_id" => $parent->id,
            "thumb" =>  $thumb["image_id"],
        ];

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/category", $data);
        $category = $response->decodeResponseJson()["category"];

        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/category/edit/" . $category["id"]);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "category" => [
                    "id" => $category["id"],
                    "name" => $category["name"],
                    "slug" => $category["slug"],
                    "status" => $category["status"],
                    "order" => $category["order"],
                    "description" => $category["description"],
                    "keywords" => $category["keywords"],
                    "old_thumb" => [
                        "file_name" => null,
                        "file_path" => "http://unigen8.de/uploads/cat_thumb/lg/avatar.jpg",
                    ],
                    "parent_id" => $parent->id,
                ]
            ]);
    }

    public function testAdminCategoryUpdate()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        //upload image
        Storage::fake("uploads");

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/medias/upload?type=cat_thumb", [
                "file" => UploadedFile::fake()->image("avatar.jpg")
            ]);

        Storage::disk("uploads")->assertExists("cat_thumb/lg/avatar.jpg");
        $thumb = $response->decodeResponseJson();

        $category = Category::factory()->create();
        $parent = Category::factory()->create();

        $data = [
            "name" => $category->name . "2",
            "slug" => $category->slug . "2",
            "status" => 0,
            "order" => 0,
            "description" => $category->description . "2",
            "keywords" => $category->keywords . "2",
            "parent_id" => $parent->id,
            "thumb" => $thumb["image_id"],
        ];


        $response = $this->actingAs($user)
            ->json("PUT", "/admin/ajax/category/" . $category->id, $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "category" => [
                    "id" => $category->id,
                    "name" => $category->name . "2",
                    "slug" => $category->slug . "2",
                    "status" => 0,
                    "order" => 0,
                    "description" => $category->description . "2",
                    "keywords" => $category->keywords . "2",
                    "parent_id" => $parent->id,
                ]
            ]);
    }

    public function testAdminCategoryDestroy()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $category = Category::factory()->create();


        $response = $this->actingAs($user)
            ->json("DELETE", "/admin/ajax/category/" . $category->id);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "result" => true
            ]);
    }

    public function testAdminCategoryChangeStatus()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $category = Category::factory()->create();


        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/category/" . $category->id . "/status/0");

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "status" => "0"
            ]);
    }

    public function testAdminCategoryActions()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $category = Category::factory()->create();
        $category2 = Category::factory()->create();

        //TEST ACTIVATE

        $data =  [
            "action" => "activate",
            "items" => [$category->id  => true, $category2->id  => true]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/category/actions", $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    ["id" => $category->id, "status" => 1],
                    ["id" => $category2->id, "status" => 1]
                ]
            ]);

        //TEST DEACTIVATE

        $data =  [
            "action" => "deactivate",
            "items" => [$category->id  => true, $category2->id  => true]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/category/actions", $data);


        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    ["id" => $category->id, "status" => 0],
                    ["id" => $category2->id, "status" => 0]
                ]
            ]);

        //TEST DELETE

        $data =  [
            "action" => "delete",
            "items" => [$category->id  => true, $category2->id  => true]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/category/actions", $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    $category->id,
                    $category2->id
                ]
            ]);
    }
}
