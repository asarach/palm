<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Card;
use App\Models\Link;
use App\Models\Menu;
use App\Models\User;
use App\Models\Category;
use Spatie\Permission\Models\Role;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminMenuTest extends TestCase
{
    use DatabaseTransactions;
    public function testAdminMenuIndex()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $menu = Menu::factory()->create();
        if (!isset($_SERVER["QUERY_STRING"])) {
            $_SERVER["QUERY_STRING"] = "";
        }
        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/menus");

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "links" => [
                    "*" => ["id", "name", "linkable_type", "url", "linkable_id", "order", "parent_id", "menu_id", "created_at", "updated_at", "linkable_item", "level", "parente", "menu" => ["id", "name"]],
                ],
                "menus" => [
                    "*" => ["id", "name", "location", "created_at", "updated_at"],
                ],
                "linkable_types" => [],
                "menu_locations" => [],
            ]);
    }

    public function testAdminMenuGetItems()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        Menu::factory()->create();

        $Category = Category::factory()->create();

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/link/get-items", [
                "type" => "App\Models\Category",
                "query" => $Category->name
            ]);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                [
                    "id" => $Category->id, "name" => $Category->name,
                ]
            ])
            ->assertJsonStructure([
                "items" => [
                    "*" => ["id", "name"],
                ],
            ]);
    }

    public function testAdminMenuShow()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $menu = Menu::factory()->create();
        $link = $this->createLink($menu->id);

        if (!isset($_SERVER["QUERY_STRING"])) {
            $_SERVER["QUERY_STRING"] = "";
        }
        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/menus?manu=" . $menu->id);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "links" => [
                    "*" => ["id", "name", "linkable_type", "url", "linkable_id", "order", "parent_id", "menu_id", "created_at", "updated_at", "linkable_item", "level", "parente", "menu" => ["id", "name"]],
                ],
                "menus" => [
                    "*" => ["id", "name", "location", "created_at", "updated_at"],
                ],
                "linkable_types" => [],
                "menu_locations" => [],
            ]);
    }

    public function testAdminMenuStore()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $parent = Menu::factory()->create();

        //craete menu
        $data = [
            "name" => "Lorem",
            "location" => "main",
        ];

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/menu", $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "menu" => [
                    "name",
                    "location"
                ]
            ]);
    }

    public function testAdminMenuStoreLink()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);


        $menu = Menu::factory()->create();
        $parent = $this->createLink();

        //craete menu
        $data = [
            "name" => "Lorem",
            "order" => 1,
            "url" => "#",
            "linkable_type" => "App\Models\Page",
            "linkable_id" => "1",
            "menu_id" => $menu->id,
            "parent_id" =>  $parent->id,
        ];

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/link", $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "link" => [
                    "linkable_type",
                    "linkable_id",
                    "order",
                    "name",
                    "parent_id",
                    "menu_id",
                    "updated_at",
                    "created_at",
                    "id"
                ]
            ]);
    }

    public function testAdminMenuEdit()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        //craete menu
        $data = [
            "name" => "Lorem",
            "location" => "main",
        ];

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/menu", $data);

        $menu = $response->decodeResponseJson()["menu"];

        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/menu/edit/" . $menu["id"]);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "menu" => [
                    "id" => $menu["id"],
                    "name" => $menu["name"]
                ]
            ]);
    }

    public function testAdminMenuEditLink()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $menu = Menu::factory()->create();
        $parent = $this->createLink();

        //craete menu
        $data = [
            "name" => "Lorem",
            "order" => "1",
            "url" => "#",
            "linkable_type" => "App\Models\Page",
            "linkable_id" => "1",
            "menu_id" => $menu->id,
            "parent_id" =>  $parent->id,
        ];

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/link", $data);
        $link = $response->decodeResponseJson()["link"];

        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/link/edit/" . $link["id"]);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "link" => [
                    "linkable_type" => $link["linkable_type"],
                    "linkable_id" => $link["linkable_id"],
                    "order" => $link["order"],
                    "name" => $link["name"],
                    "parent_id" => $link["parent_id"],
                    "menu_id" => $link["menu_id"],
                    "updated_at" => $link["updated_at"],
                    "created_at" => $link["created_at"],
                    "id" => $link["id"],
                ]

            ]);
    }

    public function testAdminMenuUpdate()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $menu = Menu::factory()->create();

        $data = [
            "name" => $menu->name . "2",
            "location" => $menu->location . "2",
        ];


        $response = $this->actingAs($user)
            ->json("PUT", "/admin/ajax/menu/" . $menu->id, $data);
        $menus = $response->decodeResponseJson()["menus"];
        $menu_r = end($menus);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                [
                    "id" => $menu->id,
                    "name" => $menu->name . "2",
                    "location" => $menu->location . "2",
                    "created_at" => $menu->created_at,
                    "updated_at" => $menu_r["updated_at"],
                ]
            ]);
    }

    public function testAdminMenuUpdateLink()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $link = $this->createLink();
        $menu = Menu::factory()->create();
        $parent = $this->createLink();

        $data = [
            "name" => "Lorem" . "2",
            "order" => 1,
            "url" => "#" . "2",
            "linkable_type" => "App\Models\Page",
            "linkable_id" => "1",
            "menu_id" => $menu->id,
            "parent_id" =>  $parent->id,
        ];



        $response = $this->actingAs($user)
            ->json("PUT", "/admin/ajax/link/" . $link->id, $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "link" => true
            ]);
    }

    public function testAdminMenuDestroy()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $menu = Menu::factory()->create();

        $response = $this->actingAs($user)
            ->json("DELETE", "/admin/ajax/menu/" . $menu->id);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "menus" => [
                    "*" => ["id", "name", "location", "created_at", "updated_at"],
                ],
            ]);
    }

    public function testAdminMenuDestroyLink()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $link = $this->createLink();

        if (!isset($_SERVER["QUERY_STRING"])) {
            $_SERVER["QUERY_STRING"] = "";
        }

        $response = $this->actingAs($user)
            ->json("DELETE", "/admin/ajax/link/" . $link->id);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "links" => [
                    "*" => ["id", "name", "linkable_type", "url", "linkable_id", "order", "parent_id", "menu_id", "created_at", "updated_at"],
                ],
            ]);
    }

    public function createLink($menu_id = null)
    {
        if ($menu_id == null) {
            $menu_id = Menu::inRandomOrder()->first()->id;
        }
        $linkable = Card::inRandomOrder()->first();

        $link =  Link::factory()->create([
            "linkable_id" =>   $linkable->id,
            "linkable_type" => get_class($linkable),
            "menu_id" =>  $menu_id,
            "parent_id" =>  Null,
        ]);

        return $link;
    }
}
