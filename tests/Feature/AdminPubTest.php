<?php

namespace Tests\Feature;

use Carbon\Carbon;
use App\Models\Pub;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminPubTest extends TestCase
{
    use DatabaseTransactions;
    public function testAdminPubIndex()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $pub = Pub::factory()->create();

        if (!isset($_SERVER["QUERY_STRING"])) {
            $_SERVER["QUERY_STRING"] = "";
        }

        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/pubs");

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "pubs" => [
                    "current_page",
                    "first_page_url",
                    "from",
                    "last_page",
                    "last_page_url",
                    "links",
                    "next_page_url",
                    "path",
                    "per_page",
                    "prev_page_url",
                    "to",
                    "total",
                    "links",
                    "data" => [
                        "*" => ["id", "name", "url", "order_num", "start_date", "end_date"],
                    ],
                ],
            ]);
    }


    public function testAdminPubStore()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        //upload image
        Storage::fake("uploads");

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/medias/upload?type=cat_thumb", [
                "file" => UploadedFile::fake()->image("avatar.jpg")
            ]);

        Storage::disk("uploads")->assertExists("cat_thumb/lg/avatar.jpg");
        $thumb = $response->decodeResponseJson();

        $parent = Pub::factory()->create();

        //craete pub
        $data = [
            "name" => "Lorem",
            "order_num" => 1,
            "status" => 1,
            "url" => "http://mp3quran.de/ar/admin/videos",
            "start_date" => "2020-11-06T00:00:00.000+01:00",
            "end_date" => "2020-11-06T00:00:00.000+01:00",
            "image" =>  $thumb["image_id"],
        ];

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/pub", $data);

        testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "pub" => [
                    "name",
                    "order_num",
                    "status",
                    "status",
                    "start_date",
                    "end_date",
                    "id"
                ]
            ]);
    }

    public function testAdminPubEdit()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        //upload image
        Storage::fake("uploads");

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/medias/upload?type=cat_thumb", [
                "file" => UploadedFile::fake()->image("avatar.jpg")
            ]);

        Storage::disk("uploads")->assertExists("cat_thumb/lg/avatar.jpg");
        $thumb = $response->decodeResponseJson();

        $parent = Pub::factory()->create();

        //craete pub
        $data = [
            "name" => "Lorem",
            "order_num" => "1",
            "status" => 1,
            "url" => "http://mp3quran.de/ar/admin/videos",
            "start_date" => "2020-11-06T10:00:00.000+01:00",
            "end_date" => "2020-11-06T10:00:00.000+01:00",
            "image" =>  $thumb["image_id"],
        ];

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/pub", $data);

        $pub = $response->decodeResponseJson()["pub"];

        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/pub/edit/" . $pub["id"]);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "pub" => [
                    "id" => $pub["id"],
                    "name" => $pub["name"],
                    "order_num" => $pub["order_num"],
                    "status" => $pub["status"],
                    "url" => $pub["url"],
                    "start_date" => str_replace("T09:00:00.000000Z", "", $pub["start_date"]),
                    "end_date" => str_replace("T09:00:00.000000Z", "", $pub["end_date"]),
                ]
            ]);
    }

    public function testAdminPubUpdate()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        //upload image
        Storage::fake("uploads");

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/medias/upload?type=cat_thumb", [
                "file" => UploadedFile::fake()->image("avatar.jpg")
            ]);

        Storage::disk("uploads")->assertExists("cat_thumb/lg/avatar.jpg");
        $thumb = $response->decodeResponseJson();

        $pub = Pub::factory()->create();
        $parent = Pub::factory()->create();

        $sd = new Carbon($pub->start_date);
        $ed = new Carbon($pub->start_date);

        $data = [
            "name" => $pub->name . "2",
            "status" => 0,
            "order_num" => 0,
            "url" => $pub->url . "2",
            "start_date" => $sd->addMonth()->toISOString(),
            "end_date" => $ed->addMonth()->toISOString(),
            "image" => $thumb["image_id"],
        ];


        $response = $this->actingAs($user)
            ->json("PUT", "/admin/ajax/pub/" . $pub->id, $data);

        testDumpError($response);

        $pub_sd = new Carbon($pub->start_date);
        $pub_ed = new Carbon($pub->start_date);

        $response
            ->assertStatus(200)
            ->assertJson([
                "pub" => [
                    "id" => $pub->id,
                    "name" => $pub->name . "2",
                    "status" => 0,
                    "order_num" => 0,
                    "url" => $pub->url . "2",
                    "start_date" =>  $pub_sd->addMonth()->toISOString(),
                    "end_date" =>  $pub_ed->addMonth()->toISOString(),
                ]
            ]);
    }

    public function testAdminPubDestroy()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $pub = Pub::factory()->create();


        $response = $this->actingAs($user)
            ->json("DELETE", "/admin/ajax/pub/" . $pub->id);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "result" => true
            ]);
    }

    public function testAdminPubRestore()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $pub = Pub::factory()->create();

        $pub->delete();

        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/pub/restore/" . $pub->id);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "result" => true
            ]);
    }

    public function testAdminPubChangeStatus()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $pub = Pub::factory()->create();


        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/pub/" . $pub->id . "/status/0");

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "status" => "0"
            ]);
    }

    public function testAdminPubActions()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $pub = Pub::factory()->create();
        $pub2 = Pub::factory()->create();

        //TEST ACTIVATE

        $data =  [
            "action" => "activate",
            "items" => [$pub->id  => true, $pub2->id  => true]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/pub/actions", $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    ["id" => $pub->id, "status" => 1],
                    ["id" => $pub2->id, "status" => 1]
                ]
            ]);

        //TEST DEACTIVATE

        $data =  [
            "action" => "deactivate",
            "items" => [$pub->id  => true, $pub2->id  => true]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/pub/actions", $data);


        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    ["id" => $pub->id, "status" => 0],
                    ["id" => $pub2->id, "status" => 0]
                ]
            ]);

        //TEST DELETE

        $data =  [
            "action" => "delete",
            "items" => [$pub->id  => true, $pub2->id  => true]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/pub/actions", $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    $pub->id,
                    $pub2->id
                ]
            ]);
    }
}
