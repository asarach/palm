<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminUserTest extends TestCase
{
    use DatabaseTransactions;
    public function testAdminUserIndex()
    {
        //create user
        $auth_user = User::factory()->create();
        $auth_user = User::find($auth_user->id);
        $role = Role::where("name", "super-admin")->first();

        $auth_user->assignRole($role);

        $response = $this->actingAs($auth_user)
            ->json("GET", "/admin/ajax/users");

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "roles" => [
                    "*" => ["created_at", "guard_name", "name", "updated_at", "id"],
                ],
                "users" => [
                    "current_page",
                    "first_page_url",
                    "from",
                    "last_page",
                    "last_page_url",
                    "links",
                    "next_page_url",
                    "path",
                    "per_page",
                    "prev_page_url",
                    "to",
                    "total",
                    "links",
                    "data" => [
                        "*" => ["id", "created_at", "current_team_id", "email", "email_verified_at", "name", "profile_photo_path", "profile_photo_url", "role", "updated_at"],
                    ],
                ],
            ]);
    }

    public function testAdminUserCreate()
    {
        //create user
        $auth_user = User::factory()->create();
        $auth_user = User::find($auth_user->id);
        $role = Role::where("name", "super-admin")->first();

        $auth_user->assignRole($role);

        $response = $this->actingAs($auth_user)
            ->json("GET", "/admin/ajax/user/create");

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "roles" => [
                    "*" => ["created_at", "guard_name", "name", "updated_at", "id"],
                ],
            ]);
    }

    public function testAdminUserStore()
    {
        //create user
        $auth_user = User::factory()->create();
        $auth_user = User::find($auth_user->id);
        $role = Role::where("name", "super-admin")->first();

        $auth_user->assignRole($role);

        //upload image
        Storage::fake("uploads");

        $response = $this->actingAs($auth_user)
            ->json("POST", "/admin/ajax/user/upload-avatar", [
                "file" => UploadedFile::fake()->image("avatar.jpg")
            ]);

        Storage::disk("uploads")->assertExists("avatar/lg/avatar.jpg");
        $avatar = $response->decodeResponseJson();

        //craete page
        $data = [
            "name" => "Lorem",
            "email" => "test@test.test",
            "method" => "admin",
            "password" => "password",
            "password_confirmation" => "password",
            "avatar" => $avatar["avatar"],
        ];

        $response = $this->actingAs($auth_user)
            ->json("POST", "/admin/ajax/user", $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "user" => [
                    "created_at",
                    "email",
                    "name",
                    "profile_photo_path",
                    "profile_photo_url",
                    "role",
                    "roles",
                    "updated_at",
                    "id"
                ]
            ]);
    }

    public function testAdminUserEdit()
    {
        //create user
        $auth_user = User::factory()->create();
        $auth_user = User::find($auth_user->id);
        $role = Role::where("name", "super-admin")->first();

        $auth_user->assignRole($role);

        //upload image
        Storage::fake("uploads");

        $response = $this->actingAs($auth_user)
            ->json("POST", "/admin/ajax/user/upload-avatar", [
                "file" => UploadedFile::fake()->image("avatar.jpg")
            ]);

        Storage::disk("uploads")->assertExists("avatar/lg/avatar.jpg");
        $avatar = $response->decodeResponseJson();

        //craete user
        $data = [
            "name" => "Lorem",
            "email" => "test@test.test",
            "method" => "admin",
            "password" => "password",
            "password_confirmation" => "password",
            "avatar" => $avatar["avatar"],
        ];

        $response = $this->actingAs($auth_user)
            ->json("POST", "/admin/ajax/user", $data);

        $user = $response->decodeResponseJson()["user"];

        $response = $this->actingAs($auth_user)
            ->json("GET", "/admin/ajax/user/edit/" . $user["id"]);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "user" => [
                    "id" => $user["id"],
                    "name" => $user["name"],
                    "profile_photo_url" => $user["profile_photo_url"],
                    "profile_photo_path" => $user["profile_photo_path"],
                    "email" => $user["email"],
                    "old_avatar" => [
                        "file_name" => "avatar.jpg",
                        "file_path" => "http://unigen8.de/uploads/avatar/md/avatar.jpg",
                    ],
                ]
            ]);
    }

    public function testAdminUserUpdate()
    {
        $auth_user = User::factory()->create();
        $auth_user = User::find($auth_user->id);
        $role = Role::where("name", "super-admin")->first();

        $auth_user->assignRole($role);

        //upload image
        Storage::fake("uploads");

        $response = $this->actingAs($auth_user)
            ->json("POST", "/admin/ajax/user/upload-avatar", [
                "file" => UploadedFile::fake()->image("avatar.jpg")
            ]);

        Storage::disk("uploads")->assertExists("avatar/lg/avatar.jpg");
        $avatar = $response->decodeResponseJson();


        $user = User::factory()->create();

        $data = [
            "name" => $user->name . "2",
            "email" => "asa" . $user->email,
            "method" => "admin",
            "password" =>  "password2",
            "password_confirmation" =>  "password2",
            "avatar" => $avatar["avatar"],
        ];


        $response = $this->actingAs($auth_user)
            ->json("PUT", "/admin/ajax/user/" . $user->id, $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "user" => [
                    "id" => $user->id,
                    "name" => $user->name . "2",
                    "email" => "asa" . $user->email,
                ]
            ]);
    }

    public function testAdminUserDestroy()
    {
        $auth_user = User::factory()->create();
        $auth_user = User::find($auth_user->id);
        $role = Role::where("name", "super-admin")->first();

        $auth_user->assignRole($role);

        $user = User::factory()->create();


        $response = $this->actingAs($auth_user)
            ->json("DELETE", "/admin/ajax/user/" . $user->id);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "result" => true
            ]);
    }

    public function testAdminUserChangeRole()
    {
        $auth_user = User::factory()->create();
        $auth_user = User::find($auth_user->id);
        $role = Role::where("name", "super-admin")->first();

        $auth_user->assignRole($role);

        $user = User::factory()->create();

        $response = $this->actingAs($auth_user)
            ->json("GET", "/admin/ajax/user/" . $user->id . "/role/admin");

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "role" => "admin"
            ]);
    }

    public function testAdminUserUploadAvatar()
    {
        $auth_user = User::factory()->create();
        $auth_user = User::find($auth_user->id);
        $role = Role::where("name", "super-admin")->first();

        $auth_user->assignRole($role);

        //upload image
        Storage::fake("uploads");

        $response = $this->actingAs($auth_user)
            ->json("POST", "/admin/ajax/user/upload-avatar", [
                "file" => UploadedFile::fake()->image("avatar.jpg")
            ]);

        Storage::disk("uploads")->assertExists("avatar/lg/avatar.jpg");

        $response
            ->assertStatus(200)
            ->assertJson([
                "avatar" => "avatar.jpg",
                "success" => true,
            ]);
    }
}
