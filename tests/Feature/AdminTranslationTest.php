<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Waavi\Translation\Models\Translation;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminTranslationTest extends TestCase
{
    use DatabaseTransactions;
    public function testAdminTranslationIndex()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        //$translation = Translation::factory()->create();

        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/translations");

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "groups" => [],
                "language" => [
                    "created_at",
                    "deleted_at",
                    "direction",
                    "display",
                    "id",
                    "locale",
                    "name",
                    "name_english",
                    "native",
                    "regional",
                    "script",
                    "status",
                    "updated_at",
                ],
                "languages" => [
                    "*" => [
                        "created_at",
                        "deleted_at",
                        "direction",
                        "display",
                        "id",
                        "locale",
                        "name",
                        "name_english",
                        "native",
                        "regional",
                        "script",
                        "status",
                        "updated_at",
                    ]
                ],
                "translations" => [
                    "current_page",
                    "first_page_url",
                    "from",
                    "last_page",
                    "last_page_url",
                    "links",
                    "next_page_url",
                    "path",
                    "per_page",
                    "prev_page_url",
                    "to",
                    "total",
                    "links",
                    "data" => [
                        "*" => ["created_at", "group", "id", "item", "locale", "locked", "main_text", "namespace", "text", "unstable", "updated_at"],
                    ],
                ],
            ]);
    }


    public function testAdminTranslationStore()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        //craete translation
        $data = [
            "group" => "Lorem",
            "item" => "description",
            "locale" => config("app.locale"),
            "text" => "Description",
        ];

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/translation", $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "success" => true
            ]);
    }

    public function testAdminTranslationUpdate()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $data = [
            "group" => "Lorem",
            "item" => "description",
            "locale" => config("app.locale"),
            "text" => "Description",
        ];

        $translation = Translation::create($data);

        $data2 = [
            "group" => "mera",
            "item" => "kaka",
            "locale" => config("app.locale"),
            "text" => "bena",
        ];

        $translation2 = Translation::create($data2);

        $data = [
            $translation->id => $translation->text . "2",
            $translation2->id => $translation2->text . "2",
        ];


        $response = $this->actingAs($user)
            ->json("PUT", "/admin/ajax/translations", $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "success" => true
            ]);
    }

    public function testAdminTranslationDestroy()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $data = [
            "group" => "Lorem",
            "item" => "description",
            "locale" => config("app.locale"),
            "text" => "Description",
        ];

        $translation = Translation::create($data);


        $response = $this->actingAs($user)
            ->json("DELETE", "/admin/ajax/translation/" . $translation->id);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "success" => true
            ]);
    }

    public function testAdminTranslationFixTranslations()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/translations/translator-fix");

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "reslut" => true,
                "success" => true
            ]);
    }
}
