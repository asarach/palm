<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Poll;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Waavi\Translation\Models\Language;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminPollest extends TestCase
{
    use DatabaseTransactions;
    public function testAdminPollIndex()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $poll = $this->createPoll();

        if (!isset($_SERVER["QUERY_STRING"])) {
            $_SERVER["QUERY_STRING"] = "";
        }

        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/polls");

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "languages" => [
                    "*" => ["id", "name"],
                ],
                "polls" => [
                    "current_page",
                    "first_page_url",
                    "from",
                    "last_page",
                    "last_page_url",
                    "links",
                    "next_page_url",
                    "path",
                    "per_page",
                    "prev_page_url",
                    "to",
                    "total",
                    "links",
                    "data" => [
                        "*" => ["id", "title", "slug", "content", "excerpt", "status", "sticky", "end_date", "language_id", "user_id", "deleted_at", "created_at", "updated_at", "language", "user"],
                    ],
                ],
            ]);
    }

    public function testAdminPollCreate()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/poll/create");

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "languages" => [
                    "*" => ["id", "name"],
                ],
            ]);
    }

    public function testAdminPollStore()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $language = Language::first();

        //craete poll
        $data = [
            "title" => "title",
            "content" => "content",
            "excerpt" => "description",
            "end_date" => "2020-11-25T21:41:44.000000Z",
            "status" => 1,
            "sticky" => 1,
            "options" => [],
            "language_id" => $language->id,
        ];

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/poll", $data);

        // testDumpError($response);
        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "poll" => [
                    "title",
                    "slug",
                    "content",
                    "excerpt",
                    "status",
                    "sticky",
                    "end_date",
                    "user_id",
                    "updated_at",
                    "created_at",
                    "options",
                    "id"
                ]
            ]);
    }

    public function testAdminPollShow()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $poll = $this->createPoll();

        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/poll/" . $poll->id);

        //testDumpError($response);

        $response
            ->assertStatus(200)

            ->assertJsonStructure([
                "poll" => [
                    "title",
                    "slug",
                    "content",
                    "excerpt",
                    "status",
                    "sticky",
                    "end_date",
                    "user_id",
                    "updated_at",
                    "created_at",
                    "id"
                ],
            ]);
    }

    public function testAdminPollEdit()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $language = Language::first();

        //craete poll
        $data = [
            "title" => "title",
            "content" => "content",
            "excerpt" => "description",
            "end_date" => "2020-11-25T21:41:44.000000Z",
            "status" => 1,
            "sticky" => 1,
            "options" => [],
            "language_id" => $language->id,
        ];

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/poll", $data);
        $poll = $response->decodeResponseJson()["poll"];

        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/poll/edit/" . $poll["id"]);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "poll" => [
                    "id" => $poll["id"],
                    "title" => $poll["title"],
                    "slug" => $poll["slug"],
                    "content" => $poll["content"],
                    "excerpt" => $poll["excerpt"],
                    "status" => $poll["status"],
                    "sticky" => $poll["sticky"],
                ]
            ]);
    }

    public function testAdminPollUpdate()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $poll = $this->createPoll();

        $data = [
            "title" => $poll->title . "2",
            "slug" => $poll->slug . "2é",
            "content" => $poll->content . "2",
            "excerpt" => $poll->excerpt . "2",
            "status" => 0,
            "sticky" => 0,
            "end_date" => "2020-11-25T21:41:44.000000Z",
            "options" => [],
        ];


        $response = $this->actingAs($user)
            ->json("PUT", "/admin/ajax/poll/" . $poll->id, $data);

        //testDumpError($response);

        testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "poll" => [
                    "id" => $poll->id,
                    "title" => $poll->title . "2",
                    //"slug" => $poll->slug . "2",
                    "content" => $poll->content . "2",
                    "status" => 0,
                    "sticky" => 0,
                    "excerpt" => $poll->excerpt . "2",
                    "end_date" => "2020-11-25T21:41:44.000000Z",
                    "options" => [],
                ]
            ]);
    }

    public function testAdminPollDestroy()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $poll = $this->createPoll();


        $response = $this->actingAs($user)
            ->json("DELETE", "/admin/ajax/poll/" . $poll->id);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "result" => true
            ]);
    }

    public function testAdminPollRestore()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $poll = $this->createPoll();

        $poll->delete();

        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/poll/restore/" . $poll->id);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "result" => true
            ]);
    }

    public function testAdminPollChangeStatus()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $poll = $this->createPoll();


        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/poll/" . $poll->id . "/status/0");

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "status" => "0"
            ]);
    }

    public function testAdminPollChangeSticky()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $poll = $this->createPoll();


        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/poll/" . $poll->id . "/sticky/0");

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "sticky" => "0"
            ]);
    }
    
    public function testAdminPollActions()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $poll = $this->createPoll();
        $poll2 = $this->createPoll();

        //TEST ACTIVATE

        $data =  [
            "action" => "activate",
            "items" => [$poll->id  => true, $poll2->id  => true]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/poll/actions", $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    ["id" => $poll->id, "status" => 1],
                    ["id" => $poll2->id, "status" => 1]
                ]
            ]);

        //TEST DEACTIVATE

        $data =  [
            "action" => "deactivate",
            "items" => [$poll->id  => true, $poll2->id  => true]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/poll/actions", $data);


        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    ["id" => $poll->id, "status" => 0],
                    ["id" => $poll2->id, "status" => 0]
                ]
            ]);

        //TEST DELETE

        $data =  [
            "action" => "delete",
            "items" => [$poll->id  => true, $poll2->id  => true]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/poll/actions", $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    $poll->id,
                    $poll2->id
                ]
            ]);
    }

    public function createPoll()
    {
        $user = User::all()->random();
        $language = Language::all()->random();

        $comment = Poll::factory()->create([
            "language_id" =>  $language->id,
            "user_id" =>  $user->id,
        ]);

        return $comment;
    }
}
