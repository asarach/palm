<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Card;
use App\Models\User;
use App\Models\Comment;
use Spatie\Permission\Models\Role;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminCommentest extends TestCase
{
    use DatabaseTransactions;
    public function testAdminCommentIndex()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $this->createComment();

        if (!isset($_SERVER["QUERY_STRING"])) {
            $_SERVER["QUERY_STRING"] = "";
        }
        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/comments");

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([

                "comments" => [
                    "current_page",
                    "first_page_url",
                    "from",
                    "last_page",
                    "last_page_url",
                    "links",
                    "next_page_url",
                    "path",
                    "per_page",
                    "prev_page_url",
                    "to",
                    "total",
                    "links",
                    "data" => [
                        "*" => [
                            "id", "author", "comment", "commentable", "commentable_id", "commentable_type", "created_at", "date", "deleted_at", "dislike", "email", "like", "name", "parent_id", "post_link", "post_title", "signal", "status", "updated_at", "user_id",
                            "user" => ["created_at", "current_team_id", "email", "email_verified_at", "id", "name", "profile_photo_path", "profile_photo_url", "role", "updated_at"]
                        ],
                    ],
                ],
            ]);
    }

    public function testAdminCommentDestroy()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $comment = $this->createComment();


        $response = $this->actingAs($user)
            ->json("DELETE", "/admin/ajax/comment/" . $comment->id);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "result" => true
            ]);
    }

    public function testAdminCommentRestore()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $comment = $this->createComment();

        $comment->delete();

        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/comment/restore/" . $comment->id);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "result" => true
            ]);
    }

    public function testAdminCommentChangeStatus()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $comment = $this->createComment();


        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/comment/" . $comment->id . "/status/0");

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "status" => "0"
            ]);
    }

    public function testAdminCommentActions()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $comment = $this->createComment();
        $comment2 = $this->createComment();

        //TEST ACTIVATE

        $data =  [
            "action" => "activate",
            "items" => [$comment->id  => true, $comment2->id  => true]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/comment/actions", $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    ["id" => $comment->id, "status" => 1],
                    ["id" => $comment2->id, "status" => 1]
                ]
            ]);

        //TEST DEACTIVATE

        $data =  [
            "action" => "deactivate",
            "items" => [$comment->id  => true, $comment2->id  => true]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/comment/actions", $data);


        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    ["id" => $comment->id, "status" => 0],
                    ["id" => $comment2->id, "status" => 0]
                ]
            ]);

        //TEST DELETE

        $data =  [
            "action" => "delete",
            "items" => [$comment->id  => true, $comment2->id  => true]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/comment/actions", $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    $comment->id,
                    $comment2->id
                ]
            ]);


        //TEST RESTORE
        $comment->delete();
        $comment2->delete();

        $data =  [
            "action" => "restor",
            "items" => [$comment->id  => true, $comment2->id  => true]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/comment/actions", $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    $comment->id,
                    $comment2->id
                ]
            ]);
    }

    public function createComment(){
        $user = User::inRandomOrder()->first();
        $commentable = $this->getCommentable();

        $comment = Comment::factory()->create([
            "commentable_id" =>   $commentable->id,
            "commentable_type" => get_class($commentable),
            "user_id" =>  $user->id,
            "parent_id" =>  null,
        ]);

        return $comment;
    }
    
    public function getCommentable()
    {
        $commentable = Card::inRandomOrder()->first();
        return $commentable;
    }
}
