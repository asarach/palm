<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Setting;
use Spatie\Permission\Models\Role;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminSettingTest extends TestCase
{
    use DatabaseTransactions;
    public function testAdminSettingIndex()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $setting = Setting::factory()->create();

        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/settings");

       //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                $setting->key => $setting->value,
            ])
            ->assertJsonStructure([
                "settings",
            ]);
    }

    public function testAdminSettingUpdate()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $setting = Setting::factory()->create();
        $setting2 = Setting::factory()->create();

        $data = [
            $setting->key => $setting->value . "3",
            $setting2->key => $setting2->value . "2",
        ];


        $response = $this->actingAs($user)
            ->json("PUT", "/admin/ajax/settings", $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                $setting->key => $setting->value . "3",
            ])
            ->assertJsonFragment([
                $setting2->key => $setting2->value . "2",
            ])
            ->assertJsonStructure([
                "settings",
            ]);
    }
}
