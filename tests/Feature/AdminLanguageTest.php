<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminLanguageTest extends TestCase
{

    use DatabaseTransactions;
    public function btesestAdminLanguageIndex()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        DB::table("translator_languages")->insert([
            "locale" => "bt",
            "name" => "Btrabic",
            "script" => "Arab",
            "native" => "العربية",
            "regional" => "bt_AE",
        ]);


        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/languages");

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "avalaible_locales" => [
                    "*" => ["locale", "name", "native", "script", "regional"],
                ],
                "languages" => [
                    "current_page",
                    "first_page_url",
                    "from",
                    "last_page",
                    "last_page_url",
                    "links",
                    "next_page_url",
                    "path",
                    "per_page",
                    "prev_page_url",
                    "to",
                    "total",
                    "links",
                    "data" => [
                        "*" => ["id", "created_at", "deleted_at", "direction", "display", "locale", "name", "name_english", "native", "regional", "script", "status", "updated_at"],
                    ],
                ],
            ]);
    }

    public function btesestAdminLanguageStore()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);


        //craete language
        $data = [
            "name" => "Lorem",
            "direction" => "ltr",
            "display" => "Loremo",
            "locale" => "ef",
            "name" => "efrançais",
            "name_english" => "english",
            "native" => "efrançais",
            "regional" => "ef_FR",
            "script" => "Latn",
            "status" => 0,
        ];

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/language", $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "language" => [
                    "created_at",
                    "id",
                    "locale",
                    "name",
                    "updated_at"
                ]
            ]);
    }

    public function btesestAdminLanguageEdit()
    {
        //create user
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);



        //craete language
        $data = [
            "name" => "Lorem",
            "direction" => "ltr",
            "display" => "Loremo",
            "locale" => "ef",
            "name_english" => "english",
            "native" => "efrançais",
            "regional" => "ef_FR",
            "script" => "Latn",
            "status" => 0,
        ];

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/language", $data);
        $language = $response->decodeResponseJson()["language"];

        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/language/edit/" . $language["id"]);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "language" => [
                    "id" => $language["id"],
                    "name" => $language["name"],
                    "direction" => $language["direction"],
                    "display" => $language["display"],
                    "locale" => $language["locale"],
                    "name" => $language["name"],
                    "name_english" => $language["name_english"],
                    "native" => $language["native"],
                    "regional" => $language["regional"],
                    "script" => $language["script"],
                    "status" => $language["status"],
                    "created_at" => $language["created_at"],
                    "status" => $language["status"],
                    "updated_at" => $language["updated_at"]

                ]
            ]);
    }

    public function btesestAdminLanguageUpdate()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        //craete language
        $data = [
            "name" => "Lorem",
            "direction" => "ltr",
            "display" => "Loremo",
            "locale" => "ef",
            "name_english" => "english",
            "native" => "efrançais",
            "regional" => "ef_FR",
            "script" => "Latn",
            "status" => 0,
        ];

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/language", $data);
        $language = $response->decodeResponseJson()["language"];

        $data = [
            "name" => $language["name"] . "2",
            "direction" => $language["direction"] . "2",
            "display" => $language["display"] . "2",
            "locale" => $language["locale"] . "2",
            "name_english" => $language["name_english"] . "2",
            "native" => $language["native"] . "2",
            "regional" => $language["regional"] . "2",
            "script" => $language["script"] . "2",
            "status" => 1,
        ];


        $response = $this->actingAs($user)
            ->json("PUT", "/admin/ajax/language/" . $language["id"], $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "language" => [
                    "name" => $language["name"] . "2",
                    "direction" => $language["direction"] . "2",
                    "display" => $language["display"] . "2",
                    "locale" => $language["locale"] . "2",
                    "name_english" => $language["name_english"] . "2",
                    "native" => $language["native"] . "2",
                    "regional" => $language["regional"] . "2",
                    "script" => $language["script"] . "2",
                    "status" => 1,
                ]
            ]);
    }

    public function btesestAdminLanguageDestroy()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $data = [
            "name" => "Lorem",
            "direction" => "ltr",
            "display" => "Loremo",
            "locale" => "ef",
            "name_english" => "english",
            "native" => "efrançais",
            "regional" => "ef_FR",
            "script" => "Latn",
            "status" => 0,
        ];

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/language", $data);
        $language = $response->decodeResponseJson()["language"];


        $response = $this->actingAs($user)
            ->json("DELETE", "/admin/ajax/language/" . $language["id"]);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "result" => true
            ]);
    }

    public function btesestAdminLanguageChangeStatus()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $data = [
            "name" => "Lorem",
            "direction" => "ltr",
            "display" => "Loremo",
            "locale" => "ef",
            "name_english" => "english",
            "native" => "efrançais",
            "regional" => "ef_FR",
            "script" => "Latn",
            "status" => 0,
        ];

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/language", $data);
        $language = $response->decodeResponseJson()["language"];


        $response = $this->actingAs($user)
            ->json("GET", "/admin/ajax/language/" . $language["id"] . "/status/0");

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "status" => "0"
            ]);
    }

    public function btesestAdminLanguageActions()
    {
        $user = User::factory()->create();
        $user = User::find($user->id);
        $role = Role::where("name", "super-admin")->first();

        $user->assignRole($role);

        $data = [
            "name" => "Lorem",
            "direction" => "ltr",
            "display" => "Loremo",
            "locale" => "ef",
            "name_english" => "english",
            "native" => "efrançais",
            "regional" => "ef_FR",
            "script" => "Latn",
            "status" => 0,
        ];

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/language", $data);
        $language = $response->decodeResponseJson()["language"];

        $data = [
            "name" => "Lerem",
            "direction" => "ltr",
            "display" => "Loremo",
            "locale" => "tf",
            "name_english" => "tnglish",
            "native" => "tfrançais",
            "regional" => "tf_FR",
            "script" => "Latn",
            "status" => 0,
        ];

        $response = $this->actingAs($user)
            ->json("POST", "/admin/ajax/language", $data);

        $language2 = $response->decodeResponseJson()["language"];

        //TEST ACTIVATE

        $data =  [
            "action" => "activate",
            "items" => [$language["id"]  => true, $language2["id"]  => true]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/language/actions", $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    ["id" => $language["id"], "status" => 1],
                    ["id" => $language2["id"], "status" => 1]
                ]
            ]);

        //TEST DEACTIVATE

        $data =  [
            "action" => "deactivate",
            "items" => [$language["id"]  => true, $language2["id"]  => true]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/language/actions", $data);


        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    ["id" => $language["id"], "status" => 0],
                    ["id" => $language2["id"], "status" => 0]
                ]
            ]);

        //TEST DELETE

        $data =  [
            "action" => "delete",
            "items" => [$language["id"]  => true, $language2["id"]  => true]
        ];

        $response = $this->actingAs($user)
            ->json("post", "/admin/ajax/language/actions", $data);

        //testDumpError($response);

        $response
            ->assertStatus(200)
            ->assertJson([
                "results" => [
                    $language["id"],
                    $language2["id"]
                ]
            ]);
    }
}
