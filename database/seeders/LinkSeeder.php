<?php

namespace Database\Seeders;

use App\Models\Post;
use App\Models\Link;
use App\Models\Menu;
use Illuminate\Database\Seeder;

class LinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, 5) as $i) {
            $menu = Menu::inRandomOrder()->first();
            $linkable = $this->getLinkable();

            Link::factory()->create(
                [
                    "linkable_id" =>   $linkable->id,
                    "linkable_type" => get_class($linkable),
                    "menu_id" =>  $menu->id,
                    "parent_id" =>  Null,
                ]
            );
        }
        foreach (range(1, 10) as $i) {
            $menu = Menu::inRandomOrder()->first();
            $linkable = $this->getLinkable();
            $parent = Link::inRandomOrder()->first();

            Link::factory()->create(
                [
                    "linkable_id" =>   $linkable->id,
                    "linkable_type" => get_class($linkable),
                    "menu_id" =>  $menu->id,
                    "parent_id" =>  $parent->id,
                ]
            );
        }
    }

    public function getLinkable()
    {
        $linkable = Post::inRandomOrder()->first();
        return $linkable;
    }
}
