<?php

namespace Database\Seeders;

use App\Models\Pub;
use App\Models\Post;
use App\Models\Media;
use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Post Medias
        $Posts = Post::all();
        foreach ($Posts as $Post) {
            $medias = [];

            $medias[] = Media::factory()->count(1)->create(["uri" => "default.jpg", "type" => "Post_thumb"]);
            $medias[] = Media::factory()->count(1)->create(["uri" => "default.pdf", "type" => "Post_pdf"]);

            foreach (range(1, 3) as $i) {
                $medias[] = Media::factory()->count(1)->create(["uri" => "default.jpg", "type" => "Post_images"]);
            }

            foreach (range(1, 3) as $i) {
                $medias[] = Media::factory()->count(1)->create(["uri" => "default.pdf", "type" => "Post_documents"]);
            }

            foreach ($medias as  $media) {
                DB::table("mediable")->insert(
                    [
                        "media_id" => $media[0]->id,
                        "mediable_id" => $Post->id,
                        "mediable_type" => get_class($Post)
                    ]
                );
            }
        }

        //Category Medias
        $categories = Category::all();
        foreach ($categories as $category) {
            $medias = [];

            $medias[] = Media::factory()->count(1)->create(["uri" => "default.jpg", "type" => "cat_thumb"]);

            foreach ($medias as  $media) {
                DB::table("mediable")->insert(
                    [
                        "media_id" => $media[0]->id,
                        "mediable_id" => $category->id,
                        "mediable_type" => get_class($category)
                    ]
                );
            }
        }

        //Pub Medias
        $pubs = Pub::all();
        foreach ($pubs as $pub) {
            $medias = [];

            $medias[] = Media::factory()->count(1)->create(["uri" => "default.jpg", "type" => "pub_image"]);

            foreach ($medias as  $media) {
                DB::table("mediable")->insert(
                    [
                        "media_id" => $media[0]->id,
                        "mediable_id" => $pub->id,
                        "mediable_type" => get_class($pub)
                    ]
                );
            }
        }
    }
}
