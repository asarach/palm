<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("translator_languages")->insert(
            [
                "locale" => "ar",
                "name" => "Arabic",
                "script" => "Arab",
                "native" => "العربية",
                "regional" => "ar_AE",
            ]
        );
        DB::table("translator_languages")->insert(
            [
                "locale" => "en",
                "name" => "English",
                "script" => "Latn",
                "native" => "English",
                "regional" => "en_GB",
            ]
        );
    }
}
