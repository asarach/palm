<?php

namespace Database\Seeders;

use App\Models\Page;
use Illuminate\Database\Seeder;
use Waavi\Translation\Models\Language;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, 10) as $i) {
            $language = Language::all()->random();
            Page::factory()->create([
                "language_id" =>  $language->id,
            ]);
        }
    }
}
