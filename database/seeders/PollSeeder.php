<?php

namespace Database\Seeders;

use App\Models\Poll;
use App\Models\User;
use App\Models\PollAnswer;
use App\Models\PollOption;
use Illuminate\Database\Seeder;
use Waavi\Translation\Models\Language;

class PollSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, 3) as $i) {
            $user = User::all()->random();
            $language = Language::all()->random();

            Poll::factory()->create([
                "language_id" =>  $language->id,
                "user_id" =>  $user->id,
            ]);
        }

        $polls = Poll::all();

        foreach ($polls as $poll) {
            $options = [];
            foreach (range(1, 3) as $i) {
                $options[] = PollOption::factory()->create([
                    "poll_id" =>  $poll->id,
                ]);
            }

            foreach ($options as $option) {
                foreach (range(1, rand(4, 14)) as $i) {
                    PollAnswer::factory()->create([
                        "option_id" =>  $option->id,
                        "poll_id" =>  $poll->id,
                    ]);
                }
            }
        }
    }
}
