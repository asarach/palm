<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(["name" => "edit articles"]);
        Permission::create(["name" => "delete articles"]);
        Permission::create(["name" => "publish articles"]);
        Permission::create(["name" => "unpublish articles"]);

        // create roles and assign existing permissions
        $role1 = Role::create(["name" => "writer"]);
        $role1->givePermissionTo("edit articles");
        $role1->givePermissionTo("delete articles");

        $role2 = Role::create(["name" => "admin"]);
        $role2->givePermissionTo("publish articles");
        $role2->givePermissionTo("unpublish articles");

        $role3 = Role::create(["name" => "super-admin"]);
        // gets all permissions via Gate::before rule; see AuthServiceProvider

        // create demo users
        $user = User::create([
            "name" => "User",
            "email" => "test@example.com",
            "password" => bcrypt("password"),
        ]);

        $user->assignRole($role1);

        $user = User::create([
            "name" => "Admin",
            "email" => "admin@example.com",
            "password" => bcrypt("password"),
        ]);
        $user->assignRole($role2);

        $user = User::create([
            "name" => "Super-Admin",
            "email" => "asaraach@gmail.com",
            "password" => bcrypt("password"),
        ]);
        $user->assignRole($role3);
    }
}
