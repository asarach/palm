<?php

namespace Database\Seeders;

use App\Models\Pub;
use Illuminate\Database\Seeder;

class PubSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pub::factory()->count(3)->create();
    }
}
