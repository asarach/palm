<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Post;
use App\Models\Category;
        use App\Models\User;
        
use Illuminate\Support\Facades\DB;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, 10) as $i) {
            $category = Category::all()->random();
        $author = User::all()->random();
        

            $post = Post::factory()->create([
                "category_id" =>  $category->id,
        "author_id" =>  $author->id,
        
            ]);
            
        }

    }
}    
        