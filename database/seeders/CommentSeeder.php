<?php

namespace Database\Seeders;

use App\Models\Post;
use App\Models\User;
use App\Models\Comment;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CommentSeeder extends Seeder
{
    use HasFactory;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, 20) as $i) {
            $user = User::inRandomOrder()->first();
            $commentable = $this->getCommentable();

            Comment::factory()->create([
                "commentable_id" =>   $commentable->id,
                "commentable_type" => get_class($commentable),
                "user_id" =>  $user->id,
                "parent_id" =>  null,
            ]);
        }
        foreach (range(1, 20) as $i) {
            $user = User::inRandomOrder()->first();
            $commentable = $this->getCommentable();
            $parent = Comment::inRandomOrder()->first();

            Comment::factory()->create([
                "commentable_id" =>   $commentable->id,
                "commentable_type" => get_class($commentable),
                "user_id" =>  $user->id,
                "parent_id" =>  $parent->id,
            ]);
        }
    }

    public function getCommentable()
    {
        $commentable = Post::inRandomOrder()->first();
        return $commentable;
    }
}
