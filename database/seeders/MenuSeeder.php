<?php

namespace Database\Seeders;

use App\Models\Menu;
use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu_locations = config("asarach.menu_locations");
        foreach ($menu_locations as $key => $menu_location) {
            Menu::factory()->count(1)->create(["location" => $key]);
        }
    }
}
