<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists("comments");
        Schema::create("comments", function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements("id");
            $table->text("comment")->nullable();
            $table->tinyInteger("status")->default(0);

            $table->string("email")->nullable();
            $table->string("name")->nullable();
            $table->integer("signal")->default(0);
            $table->integer("like")->default(0);
            $table->integer("dislike")->default(0);

            $table->integer("commentable_id")->nullable();
            $table->string("commentable_type")->nullable();

            $table->unsignedBigInteger("user_id")->nullable();
            $table->foreign("user_id")
                ->references("id")
                ->on("users")
                ->onDelete("cascade");

            $table->unsignedBigInteger("parent_id")->nullable();
            $table->foreign("parent_id")
                ->references("id")
                ->on("comments")
                ->onDelete("cascade");

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("comments");
    }
}
