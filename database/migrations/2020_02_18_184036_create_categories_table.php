
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("categories", function (Blueprint $table) {
            $table->engine = "InnoDB";
            
            $table->increments("id");
            $table->string("name", 190);
            $table->string("slug", 190)->nullable()->unique();
            $table->integer("order")->nullable()->default(1);
            $table->boolean("status")->nullable()->default(0);
            $table->text("description")->nullable();
            $table->text("keywords")->nullable();

            $table->unsignedInteger("parent_id")->nullable();
            $table->foreign("parent_id")
                ->references("id")
                ->on("categories")
                ->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("categories");
    }
}
