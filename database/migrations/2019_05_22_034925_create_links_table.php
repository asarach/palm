<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("links", function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->string("name");
            $table->string("linkable_type");
            $table->string("url")->nullable();
            $table->integer("linkable_id")->nullable();
            $table->integer("order")->default(10);

            $table->unsignedBigInteger("parent_id")->nullable();
            $table->foreign("parent_id")
                ->references("id")->on("links")
                ->onDelete("cascade");

            $table->unsignedBigInteger("menu_id");
            $table->foreign("menu_id")
                ->references("id")->on("menus")
                ->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("links");
    }
}
