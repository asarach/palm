<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("polls", function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements("id");
            $table->string("title", 190);
            $table->string("slug", 190)->nullable()->unique();
            $table->longText("content")->nullable();
            $table->text("excerpt")->nullable();
            $table->boolean("status")->nullable();
            $table->integer("sticky")->nullable();
            $table->string("end_date", 190)->nullable();

            $table->unsignedInteger("language_id")->nullable();
            $table->foreign("language_id")
                ->references("id")
                ->on("translator_languages")
                ->onDelete("cascade");

            $table->unsignedBigInteger("user_id")->nullable();
            $table->foreign("user_id")
                ->references("id")
                ->on("users")
                ->onDelete("cascade");

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create("poll_options", function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements("id");
            $table->string("title", 190);

            $table->unsignedBigInteger("poll_id")->nullable();
            $table->foreign("poll_id")
                ->references("id")
                ->on("polls")
                ->onDelete("cascade");

            $table->timestamps();
        });

        Schema::create("poll_answers", function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements("id");
            $table->string("ip", 190);

            $table->unsignedBigInteger("option_id")->nullable();
            $table->foreign("option_id")
                ->references("id")
                ->on("poll_options")
                ->onDelete("cascade");

            $table->unsignedBigInteger("poll_id")->nullable();
            $table->foreign("poll_id")
                ->references("id")
                ->on("polls")
                ->onDelete("cascade");

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("polls");
        Schema::dropIfExists("poll_options");
        Schema::dropIfExists("poll_answers");
    }
}
