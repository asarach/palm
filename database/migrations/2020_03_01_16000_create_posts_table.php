<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("posts", function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements("id");

            $table->string("title", "190");
            $table->string("slug", "190")->nullable()->unique();
            $table->longText("body")->nullable();
            $table->text("excerpt")->nullable();
            $table->text("keywords")->nullable();
            $table->integer("comment_count")->nullable()->default(0);
            $table->boolean("status")->nullable()->default(0);
            $table->boolean("comment_status")->nullable()->default(0);

            $table->unsignedInteger("category_id")->nullable();
            $table->foreign("category_id")
                ->references("id")
                ->on("categories")
                ->onDelete("cascade");
                
            $table->unsignedBigInteger("author_id")->nullable();
            $table->foreign("author_id")
                ->references("id")
                ->on("users")
                ->onDelete("cascade");

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("posts");
    }
}
