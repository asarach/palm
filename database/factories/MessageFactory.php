<?php

namespace Database\Factories;

use App\Models\Message;
use Illuminate\Database\Eloquent\Factories\Factory;

class MessageFactory extends Factory
{
    /**
     * The name of the factory"s corresponding model.
     *
     * @var string
     */
    protected $model = Message::class;

    /**
     * Define the model"s default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "body" => $this->faker->paragraph,
            "viewed" => $this->faker->numberBetween(0,1),
            "email" => $this->faker->unique()->safeEmail,
            "subject" => $this->faker->name,
            "name" => $this->faker->name,
            "created_at" =>  $this->faker->dateTime,
            "updated_at" =>  $this->faker->dateTime,
        ];
    }
}
