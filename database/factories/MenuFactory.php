<?php

namespace Database\Factories;

use App\Models\Menu;
use Illuminate\Database\Eloquent\Factories\Factory;

class MenuFactory extends Factory
{
    /**
     * The name of the factory"s corresponding model.
     *
     * @var string
     */
    protected $model = Menu::class;

    /**
     * Define the model"s default state.
     *
     * @return array
     */
    public function definition()
    {
        //$localtion = config("asarach.menu_locations")[0];
        return [
            "name" => $this->faker->name,
            "location" => "main",
            "created_at" =>  $this->faker->dateTime,
            "updated_at" =>  $this->faker->dateTime,
        ];
    }
}
