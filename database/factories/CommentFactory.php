<?php

namespace Database\Factories;

use App\Models\Comment;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    /**
     * The name of the factory"s corresponding model.
     *
     * @var string
     */
    protected $model = Comment::class;

    /**
     * Define the model"s default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "comment" => $this->faker->paragraph,
            "status" => $this->faker->numberBetween(0,1),
            "email" => $this->faker->unique()->safeEmail,
            "name" => $this->faker->name,
            "commentable_id" => "",
            "commentable_type" => "",
            "user_id" => 1,
            "parent_id" => Null,
            "created_at" =>  $this->faker->dateTime,
            "updated_at" =>  $this->faker->dateTime,
        ];
    }
}
