<?php

        namespace Database\Factories;
        
        use App\Models\Post;
        use Illuminate\Database\Eloquent\Factories\Factory;
        
        class PostFactory extends Factory
        {
            /**
             * The name of the factory's corresponding model.
             *
             * @var string
             */
            protected $model = Post::class;
        
            /**
             * Define the model's default state.
             *
             * @return array
             */
            public function definition()
            {
                return [
                    "title" => $this->faker->name,
                    "slug" => $this->faker->name,
                    "body" => $this->faker->text,
                    "excerpt" => $this->faker->paragraph,
                    "keywords" => $this->faker->paragraph,
                    "comment_count" => $this->faker->randomDigit,
                    "status" => $this->faker->numberBetween(0, 1),
                    "comment_status" => $this->faker->numberBetween(0, 1),
                    "category_id" =>  null,
                    "author_id" =>  null,
                    "created_at" =>  $this->faker->dateTime,
                    "updated_at" =>  $this->faker->dateTime,
                    
            
                ];
            }
        }
        