<?php

namespace Database\Factories;

use App\Models\Media;
use Illuminate\Database\Eloquent\Factories\Factory;

class MediaFactory extends Factory
{
    /**
     * The name of the factory"s corresponding model.
     *
     * @var string
     */
    protected $model = Media::class;

    /**
     * Define the model"s default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "title" => $this->faker->name,
            "uri" => $this->faker->name . ".jpg",
            "status" => $this->faker->numberBetween(0,1),
            "type" => "none",
            "created_at" =>  $this->faker->dateTime,
            "updated_at" =>  $this->faker->dateTime,
        ];
    }
}
