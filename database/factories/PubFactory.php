<?php

namespace Database\Factories;

use App\Models\Pub;
use Illuminate\Database\Eloquent\Factories\Factory;

class PubFactory extends Factory
{
    /**
     * The name of the factory"s corresponding model.
     *
     * @var string
     */
    protected $model = Pub::class;

    /**
     * Define the model"s default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "name" => $this->faker->name,
            "order_num" => $this->faker->numberBetween(1,10),
            "url" => $this->faker->url,
            "start_date" => $this->faker->dateTime,
            "end_date" => $this->faker->dateTime,
            "status" => $this->faker->numberBetween(0,1),
            "created_at" =>  $this->faker->dateTime,
            "updated_at" =>  $this->faker->dateTime,
        ];
    }
}
